///* ============================================================
// * bootstrap-rowlink.js j1
// * http://jasny.github.com/bootstrap/javascript.html#rowlink
// * ============================================================
// * Copyright 2012 Jasny BV, Netherlands.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// * http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// * ============================================================ */
//
//!function ($) {
//  
//  "use strict"; // jshint ;_;
//
//  var Rowlink = function (element, options) {
//    options = $.extend({}, $.fn.rowlink.defaults, options)
//    var tr = element.nodeName.toLowerCase() == 'tr' ? $(element) : $(element).find('tr:has(td)')
//    
//    tr.each(function() {
//      var link = $(this).find(options.target).first()
//      if (!link.length) return
//      
//      var href = link.attr('href')
//      console.log("href",href)
//      $(this).find('td').not('.nolink').click(function() {
//        window.location = href;
//      })
//
//      $(this).addClass('rowlink')
//      link.replaceWith(link.html())
//    })
//  }
//
//  
// /* ROWLINK PLUGIN DEFINITION
//  * =========================== */
//
//  $.fn.rowlink = function (options) {
//    return this.each(function () {
//      var $this = $(this)
//      , data = $this.data('rowlink')
//      if (!data) $this.data('rowlink', (data = new Rowlink(this, options)))
//    })
//  }
//
//  $.fn.rowlink.defaults = {
//    target: "a"
//  }
//
//  $.fn.rowlink.Constructor = Rowlink
//
//
// /* ROWLINK DATA-API
//  * ================== */
//
//  $(function () {
//    $('[data-provide="rowlink"],[data-provides="rowlink"]').each(function () {
//      $(this).rowlink($(this).data())
//    })
//  })
//  
//}(window.jQuery);

$(function () {

    $(document).on('click', '[data-redirect="vehicleDetail"]', function (e) {
        var $this = $(this);
        var href = $this.find("a").attr("href");
        if(href) {
//        	console.log("clicked",href)
        	window.location = href;
        }
    });
});


$(document).ready(function() {
	var $this = $('#sortdatatable');
//	var $this =  document.getElementById("sortdatatable");

	$this.load(function(){
		console.log("$this", "rendered");  
	});
	
//	console.log("$this", $this)
	$($this).bind("render", function() {
		console.log("$this", "rendered");
	});
	$this.render();
});

(function($) {
    $.fn.render = function() {
        var selector = this;
//        console.log("selector to bind: ", selector);
        selector.tablesorter( {sortList: [[1,0], [2,0], [3,0], [4,0]]} ); ;
        selector.trigger("render");
    };
})(jQuery);

