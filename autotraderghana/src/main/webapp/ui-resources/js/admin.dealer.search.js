
function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}


Handlebars.registerHelper('phone', function() {
	var phone1 = this.phone1,
		phone2 = this.phone2,
		str_inner = '',
		result = '';
	
	if (!isEmpty(phone1))
		str_inner += phone1;
	if (!isEmpty(phone2)) 
		str_inner += '&nbsp;/&nbsp;' + phone2;

	if (!isEmpty(str_inner))
		result = '<img src="/img/phone.gif" />&nbsp;'	+ str_inner ;
		
	return new Handlebars.SafeString(result);
});

$(function () {
	$(document).ajaxStart(function() {
		$("#rc_notify").text("Loading, please wait....");
		$("#rc_notify").show();
	});
	
	$.triggerDealerSearch();
//	
//	$('#sortPattern').on('change', function () {
//		$.triggerVehicleSearch();
//    }).trigger('change');
	
	
	
});


$.triggerDealerSearch = function (){
	var prefix = "/data";  
//		pageIndex = $("#pageIndex").val();


	function getSearchConditions() {
        var searchConditions = {};

        searchConditions.pageIndex = 0;

        return searchConditions;
    }

    function findDealerCount(callback) {
        if (request) {
            request.abort(); // abort any pending request
        }

        var request = $.ajax({
            url: prefix + "/dealer/count",
            type: "get",
            dataType: 'json',
//            data: JSON.stringify(pageIndex),
        });
        request.done(function (data) {
            //console.log("Data: " + data);
            $("#dealer-count,#search-result-label").html(data);
            callback(parseInt(data, 10));
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });

    };

    var handlePaginationClick = function (new_page_index, pagination_container) {
    	

//        $("#pageIndex").val(new_page_index);
        
        var resultHolder = $("#dealer-list-holder"),
        	source = render('admin_dealer_list');

        var	template = Handlebars.compile(source);

        if (request) {
            request.abort(); // abort any pending request
        }

        // fire off the request to OrderController
        var request = $.ajax({
            url: prefix + "/dealers",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(searchConditions.pageIndex ),
        });

        request.done(function (data) {
        	console.log("data",data);

            resultHolder.html(template({
                dealers: data
            }));
            $("#rc_notify").hide();
            //Removes the pagination elements if no results is found
            if (data.length === 0) {
                $(".pagination-holder").addClass("hidden");
            } else {
                $(".pagination-holder").removeClass("hidden");
            }
            
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });


    };

    var initPaginator = function (dealerCount) {
        $(".pagination-holder").pagination(dealerCount, {
//            current_page:  $("#pageIndex").val() /*searchConditions.page*/,

            current_page: searchConditions.page,
            items_per_page: 10,
            load_first_page: true,
            callback: handlePaginationClick,
            prev_show_always: true,
            next_show_always: true,
        });
    };

    var searchConditions = getSearchConditions();
    findDealerCount(initPaginator);
}
