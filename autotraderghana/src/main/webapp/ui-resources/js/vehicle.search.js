$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}
function JsonToJSDate(jsonDate) {
    var reg = /-?\d+/;
    var m = reg.exec(jsonDate);
    return new Date(parseInt(m[0]));
}
Handlebars.registerHelper('customer', function() {
	var customer = this.customer,
		sellerType = this.sellerType,
		city = this.city,
		str_inner = '',
		result = '';
	
	if (!isEmpty(sellerType) && sellerType !== 'Private Seller')
		str_inner += '<p style="margin:0 0 5px 0;"> <span class="label">' + sellerType +'</span></p>';
	if (!isEmpty(customer)) 
		str_inner += '<p style="margin:0">' + customer +'</p>';
	if (!isEmpty(city)) 
		str_inner += '<p style="font-weight:normal;">' + city +'</p>';
	
	
	if (!isEmpty(str_inner))
		result = str_inner;
		
	return new Handlebars.SafeString(result);
});
Handlebars.registerHelper('other-detail', function() {
	var bodyType = this.bodyType,
		fuelType = this.fueltype,
		gearBoxType = this.gearBoxType,
		engineSize = this.engineSize,
		timeDifference = this.timeDifference,
		str_time ='',
		str_inner = '',
		result = '';
	
	if (!isEmpty(bodyType))
		str_inner += '<li>' + bodyType +'</li>';
	if (!isEmpty(gearBoxType)) 
		str_inner += '<li>' + gearBoxType +'</li>';
	if (!isEmpty(fuelType)) 
		str_inner += '<li>' + fuelType +'</li>';
	if (!isEmpty(engineSize)) 
		str_inner += '<li>' + engineSize +'</li>';
	if (!isEmpty(timeDifference)) 
		str_time += '<p style="margin:0;"> <span class="label label-info">' + timeDifference +'</span></p>';
	
	if (!isEmpty(str_inner))
		result = '<ul class="list-other-details" style="margin-bottom:5px;">'	+ str_inner +'</ul>' + str_time;
		
	return new Handlebars.SafeString(result);
});

$(function () {
	$(document).ajaxStart(function() {
		$("#rc_notify").text("Loading, please wait....");
		$("#rc_notify").show();
	});
	
	$.triggerVehicleSearch();
	
	
	$('#sortPattern').on('change', function () {
		$.triggerVehicleSearch();
    }).trigger('change');
	
//	$('#sortPattern').on('change', function () {
//		$.triggerVehicleSearch();
//    }).trigger('change');
	
	$('ul.my-search-tab a[data-toggle="tab"]').on('shown', function (e) {
        // e.target // activated tab
        // e.relatedTarget // previous tab
    	
    	console.log("$.triggerVehicleSearch(): ", e.target)
    	$.triggerVehicleSearch();
        

    });
	
});


$.triggerVehicleSearch = function (){
	var prefix = "/data";


    function getSearchConditions() {
        var searchConditions = {};
        return searchConditions;
    }

    function findVehicleCount(callback) {
        if (request) {
            request.abort(); // abort any pending request
        }
        var serializedData = $('#searchFilter-form').serialize();

        var request = $.ajax({
            url: prefix + "/vehicle/count",
            type: "post",
            data: serializedData
        });
        request.done(function (data) {
            //console.log("Data: " + data);
            $("#vehicle-count,#search-result-label").html(data);
            callback(parseInt(data, 10));
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });

    };

    var handlePaginationClick = function (new_page_index, pagination_container) {
    	
//    	var target = document.getElementById('spinner');
//		var spinner = new Spinner(getSpinOpts()).spin(target);
		$('#sortPattern').val($('#vehicleSortPattern').val());
        $("#pageIndex").val(new_page_index);
        var serializedData = $('#searchFilter-form').serialize();
        
        

//        console.log('source', require_template('vehicle_list'))
        var resultHolder = $("#vehicle-list-holder"),
        	source = render('vehicle_list');

        var	template = Handlebars.compile(source),
        	vehicleSearchUrl = prefix + "/vehicle/search/page";
        
       

        if (request) {
            request.abort(); // abort any pending request
        }

        // fire off the request to OrderController
        var request = $.ajax({
            url: prefix + "/vehicle/search/page",
            type: "post",
            data: serializedData,
        });

        request.done(function (data) {
//        	console.log("data",data);
        	
        	getSortPattern();
//        	resultHolder.empty();
            resultHolder.html(template({
                vehicles: data
            }));
//        	spinner.stop();
            $("#rc_notify").hide();
            //Removes the pagination elements if no results is found
            if (data.length === 0) {
                $(".pagination-holder").addClass("hidden");
            } else {
                $(".pagination-holder").removeClass("hidden");
            }
            
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });


    };

    var initPaginator = function (vehicleCount) {
        $(".pagination-holder").pagination(vehicleCount, {
//            current_page:  $("#pageIndex").val() /*searchConditions.page*/,

            current_page: searchConditions.page,
            items_per_page: 10,
            load_first_page: true,
            callback: handlePaginationClick,
            prev_show_always: true,
            next_show_always: true,
        });
    };

    var searchConditions = getSearchConditions();
    findVehicleCount(initPaginator);
}


//SortPattern

function getSortPattern() {
    var url = "/data/sortPattern";

    $("#sortPattern").select2({
        placeholder: "Filter By",
        allowClear: false,
        query: function (query) {

            if (request) {
                request.abort(); // abort any pending request
            }

            var request = $.getJSON(url, function (data) {
            }).done(function (data) {
                
                var results = [];

				_.each(data.sortPatterns, function(sortPattern) {
					results.push({
                        id: sortPattern.value,
                        text: sortPattern.name
                    });
					
				});

                query.callback({
                    results: results,
                    more: false
                });
            });

        },
        initSelection: function (element, callback) {
            var id = $(element).val();
            if (id !== "") {
                var data = {
                    id: id,
                    text: getSortPatternValue(id)
                };
                callback(data);
            }
        },
        formatResult: format,
        formatSelection: format
    }).on('change', function () {
        var changeId = $('#sortPattern').select2('val');
        console.log("changeId ", changeId)
        if (!isEmpty(changeId)) {
        	$('#vehicleSortPattern').val(changeId);
        } else {
            $('#vehicleSortPattern').val("year_desc");
        }
        
        

    })/*.trigger('change')*/;

}

function getSortPatternValue(sortPattern) {
	switch (sortPattern) {
	case 'mileage_asc':
		return "Mileage Ascending";
	case 'mileage_desc':
		return "Mileage Descending";
	case 'price_asc':
		return "Price Ascending";
	case 'price_desc':
		return "Price Descending";
	case 'year_asc':
		return "Year Descending";
	case 'year_desc':
		return "Year Descending";

	}
}