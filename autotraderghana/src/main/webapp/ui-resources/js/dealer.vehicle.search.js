
function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}

Handlebars.registerHelper('website', function() {
	var website = this.dealer.website,
		str_inner = '',
		result = '';
	if (!isEmpty(website)) 
		str_inner += '<a href="http://'+website+'" target="_blank">' + website +'</a>';
	
	
	if (!isEmpty(str_inner))
		result = str_inner;
	else {
		result = "Not Specified"
	}
		
	return new Handlebars.SafeString(result);
});
Handlebars.registerHelper('phone', function() {
	var phone1 = this.dealer.phone1,
		phone2 = this.dealer.phone2,
		str_inner = '',
		result = '';
	
	if (!isEmpty(phone1))
		str_inner += phone1;
	if (!isEmpty(phone2)) 
		str_inner += '&nbsp;/&nbsp;' + phone2;

	if (!isEmpty(str_inner))
		result = '<img src="/img/phone.gif" />&nbsp;'	+ str_inner ;
		
	return new Handlebars.SafeString(result);
});

$(function () {
	$(document).ajaxStart(function() {
		$("#rc_notify").text("Loading, please wait....");
		$("#rc_notify").show();
	});
	
	$.triggerDealerSearch();
	$.triggerDealerVehicleSearch();

	
	
	
});


$.triggerDealerVehicleSearch = function (){
	var prefix = "/data";  

	function getSearchConditions() {
        var searchConditions = {};

        searchConditions.pageIndex = 0;
        searchConditions.dealerId = $("#dealerId").text();

        return searchConditions;
    }

    function findDealerVehicleCount(callback) {
        if (request) {
            request.abort(); // abort any pending request
        }
        console.log("JSON.stringify(searchConditions)",JSON.stringify(searchConditions))
        var request = $.ajax({
            url: prefix + "/dealer/vehicle/count",
            type: "post",
            data: JSON.stringify(searchConditions),
            contentType: "application/json"
        });
        request.done(function (data) {
            //console.log("Data: " + data);
            $("#dealer-vehicle-count").html(data);
            callback(parseInt(data, 10));
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });

    };

    var handlePaginationClick = function (new_page_index, pagination_container) {
    	

    	searchConditions.pageIndex = new_page_index;
        
        var resultHolder = $("#dealer-vehicle-list-holder"),
        	source = render('vehicle_list');

        var	template = Handlebars.compile(source);

        if (request) {
            request.abort(); // abort any pending request
        }

        // fire off the request to OrderController
        var request = $.ajax({
            url: prefix + "/dealer/vehicles",
            type: "post",
            data: JSON.stringify(searchConditions),
            contentType: "application/json"
        });

        request.done(function (data) {
        	console.log("data",data);

            resultHolder.html(template({
                vehicles: data
            }));
            $("#rc_notify").hide();
            //Removes the pagination elements if no results is found
            if (data.length === 0) {
                $(".pagination-holder").addClass("hidden");
            } else {
                $(".pagination-holder").removeClass("hidden");
            }
            
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });


    };

    var initPaginator = function (dealerCount) {
        $(".pagination-holder").pagination(dealerCount, {
            current_page: searchConditions.page,
            items_per_page: 10,
            load_first_page: true,
            callback: handlePaginationClick,
            prev_show_always: true,
            next_show_always: true,
        });
    };

    var searchConditions = getSearchConditions();
    findDealerVehicleCount(initPaginator);
}

$.triggerDealerSearch = function (){
	var prefix = "/data";  

	function getSearchConditions() {
        var searchConditions = {};

        searchConditions.dealerId = $("#dealerId").text();

        return searchConditions;
    }

    function findDealer(){
        if (request) {
            request.abort(); // abort any pending request
        }
        
        var resultHolder = $("#dealer-info-holder"),
    	source = render('dealer_info');

        var	template = Handlebars.compile(source);

        var request = $.ajax({
            url: prefix + "/dealerInfo",
            type: "post",
            contentType: "application/json",
            data: JSON.stringify(searchConditions.dealerId),
        });
        request.done(function (data) {
            //console.log("Data: " + data);
        	console.log("data",data);
        	

            resultHolder.html(template({
                dealer: data
            }));
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.log("The following error occured: " + textStatus, errorThrown);
        });

    };

    

    var searchConditions = getSearchConditions();
    findDealer();
}
