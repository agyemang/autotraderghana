function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '')
        return true;
    if (typeof obj == 'number' && isNaN(obj))
        return true;
    if (obj instanceof Date && isNaN(Number(obj)))
        return true;
    return false;
}


function getSpinOpts() {
    var opts = {
        lines: 10, // The number of lines to draw
        length: 8, // The length of each line
        width: 3, // The line thickness
        radius: 7, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb
        speed: 1.5, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
    };
    return opts;
}




function render(tmpl_name, tmpl_data) {
    if (!render.tmpl_cache) {
        render.tmpl_cache = {};
    }

    if (!render.tmpl_cache[tmpl_name]) {
        var tmpl_dir = '/templates';
        var tmpl_url = tmpl_dir + '/' + tmpl_name + '.tmpl';

        var tmpl_string;
        $.ajax({
            url: tmpl_url,
            method: 'GET',
            async: false,
            dataType: 'html',
            success: function (data) {
                tmpl_string = data;
            }
        });

        render.tmpl_cache[tmpl_name] = _.template(tmpl_string);
    }

    return render.tmpl_cache[tmpl_name](tmpl_data);
}

$(function () {
	
	

    var vehicleCondition = $('#vehicleCondition').val(),
        $thisnew = $('#new-search'),
        $thisused = $("#used-search"),

        $thismoresearch = $("#iSMoreSearch1");

    if (!isEmpty(vehicleCondition)) {
        if (vehicleCondition === 'NEW') {
            $($thisused).removeClass('active');
            $($thisnew).addClass("active");
        }
    } else {
        $('#vehicleCondition').val('USED')
    }
    
    
    $('ul.vehicle-condition-tab a[data-toggle="tab"]').on('shown', function (e) {   	
    	console.log(e.target)

        var $this = $(this),
            searchtype = $this.attr('data-searchtype');

    	if (searchtype === 'used')
            $('#vehicleCondition').val('USED')
        else
            $('#vehicleCondition').val('NEW')

    });


    $('ul.my-search-tab a[data-toggle="tab"]').on('shown', function (e) {
        // e.target // activated tab
        // e.relatedTarget // previous tab
    	
    	console.log(e.target)

        var $this = $(this),
            searchtype = $this.attr('data-searchtype');

        if (searchtype === 'dealer') {
            $('#customerType').val("CAR_DEALER");
            $('#isNew24').val("false");
            $('#isUpdated24').val("false");
        } else if (searchtype === 'private') {
            $('#customerType').val("PRIVATE_SELLER");
            $('#isNew24').val("false");
            $('#isUpdated24').val("false");
        } else if (searchtype === 'new24') {
            $('#isNew24').val("true");
            $('#customerType').val("");
            $('#isUpdated24').val("false");
        } else if (searchtype === 'update24') {
        	$('#isUpdated24').val("true");
            $('#isNew24').val("false");
            $('#customerType').val("");
        } else {
            $('#customerType').val("");
            $('#isNew24').val("false");
            $('#isUpdated24').val("false");
        }

    });


    $($thismoresearch).change(function () {
        $('#more-search-criteria-content').toggle(this.checked);
    }).change(); //ensure visible state matches initially


    $('.select2').select2({
        allowClear: true,
        containerCssClass: 'span12',
    });

    $(".fix-radio > label").addClass("radio inline");
    $(".fix-check > label").addClass("checkbox block");
    $("select#advertisingDetails.country").addClass("span2 select2");




    var vehicleFindId = $('#vehicleFindId'),
        searchForm = $("#search-by-id-form"),
        appendedInputButtons = $("#appendedInputButtons")
            .attr("data-toggle", "popover")
            .attr("data-placement", "top")
            .attr("data-content", "Please enter Vehicle ID!");

    $(appendedInputButtons).focusout(function () {
        var $this = $("#appendedInputButtons"),
            inputText = $this.val();
        if (!isEmpty(inputText))
            vehicleFindId.val(inputText);
    });

    $(searchForm).submit(function () {
        var inputAvailable = vehicleFindId.val();
        if (isEmpty(inputAvailable)) {
            $(appendedInputButtons).popover('show');
            return false;
        }
        return true;
    });

    $(".search-filter-content").attr("data-toggle", "popover").attr("data-placement", "top").attr("data-content", "Please select a Vehicle Make!");


    $('#searchFilter-form').submit(function () {
        var checkId = $("#vehicleMake").val();
        console.log("checkId-vehicleMake", checkId)
        if (isEmpty(checkId)) {
            $(".search-filter-content").popover('show');
            return false;
        }
        return true;
    });

    $('body').on('click', function (e) {
        $('.popover-link').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});

$(function () {

    var vehicleModelSelect = $('#vehicleModelSelect'),
        yearToSelect = $('#yearToSelect'),
        priceToSelect = $('#priceToSelect'),
        engineSizeToSelect = $('#priceToSelect'),
        mileageToSelect = $('#mileageToSelect');

    $('#vehicleMake').select2({
        placeholder: 'Select Vehicle Make',
        allowClear: true
    }).on('change', function () {
        $.findVehicleModels();
    }).trigger('change');

    $('#priceFrom').select2({
        placeholder: 'Minimum',
        allowClear: true
    }).on('change', function () {
        $.findPriceTo();
    }).trigger('change');

    $('#yearFrom').select2({
        placeholder: 'Minimum',
        allowClear: true
    }).on('change', function () {
        $.findYearTo();
    }).trigger('change');

    $('#engineSizeFrom').select2({
        placeholder: 'Minimum',
        allowClear: true
    }).on('change', function () {
        $.findEngineSizeTo();
    }).trigger('change');

    $('#mileageFrom').select2({
        placeholder: 'Minimum',
        allowClear: true
    }).on('change', function () {
        $.findMileageTo();
    }).trigger('change');


});

$.findVehicleModels = function () {

    var prefix = "/data"
    var selectedMake = $("#vehicleMake").val();

    $(vehicleModelSelect).select2('data', null).select2({
        placeholder: 'Select Vehicle Model',
        allowClear: true,
        query: function (query) {
            var data;
            $.ajax({
                type: "POST",
                url: prefix + "/vehicleModels",
                data: JSON.stringify(selectedMake),
                contentType: "application/json",
                success: function (data) {
                    var results = [];
                    for (var i = 0; i < data.length; ++i) {
                        results.push({
                            id: data[i].id,
                            text: data[i].name
                        });
                    }

                    query.callback({
                        results: results,
                        more: false
                    });
                }
            });

        },
        initSelection: function (element, callback) {
            // the input tag has a value attribute preloaded that points to a preselected model's id
            // this function resolves that id attribute to an object that select2 can render
            // using its formatResult renderer - that way the model name is shown preselected
            var id = $(element).val();
            if (id !== "") {
                $.ajax({
                    type: "POST",
                    url: prefix + "/vehicleModel",
                    data: JSON.stringify(id),
                    contentType: "application/json",
                    success: function (data) {
                        var result = {
                            id: data.id,
                            text: data.name
                        };
                        callback(result);
                    }
                });

            }
        },
    }).on('change', function () {
        var checkId = $(vehicleModelSelect).select2('val');
        if (!isEmpty(checkId)) {
            $('#vehicleModel').val(checkId);
        } else {
            $('#vehicleModel').val("");
        }

    }).trigger('change');
}

$.findPriceTo = function () {

    var prefix = "/data"
    var selectedPrice = $("#priceFrom").val();

    $(priceToSelect).select2('data', null).select2({
        placeholder: 'Maximum',
        allowClear: true,
        query: function (query) {
            var data;
            $.ajax({
                type: "POST",
                url: prefix + "/priceTo",
                data: JSON.stringify(selectedPrice),
                contentType: "application/json",
                success: function (data) {
                    var results = [];
                    for (var i = 0; i < data.length; ++i) {
                        results.push({
                            id: data[i],
                            text: data[i]
                        });
                    }

                    query.callback({
                        results: results,
                        more: false
                    });
                }
            });

        },
        initSelection: function (element, callback) {
            var id = $(element).val();
            if (id !== "") {
                var data = {
                    id: id,
                    text: id
                };
                callback(data);
            }
        },
        formatResult: format,
        formatSelection: format
    }).on('change', function () {
        var checkId = $(priceToSelect).select2('val');
        if (!isEmpty(checkId)) {
            $('#priceTo').val(checkId);
        } else {
            $('#priceTo').val("");
        }

    }).trigger('change');
}
//YearTo
$.findYearTo = function () {

    var prefix = "/data"
    var selectedYear = $("#yearFrom").val();

    $(yearToSelect).select2('data', null).select2({
        placeholder: 'Maximum',
        allowClear: true,
        query: function (query) {
            var data;
            $.ajax({
                type: "POST",
                url: prefix + "/yearTo",
                data: JSON.stringify(selectedYear),
                contentType: "application/json",
                success: function (data) {
                    var results = [];
                    for (var i = 0; i < data.length; ++i) {
                        results.push({
                            id: data[i],
                            text: data[i]
                        });
                    }

                    query.callback({
                        results: results,
                        more: false
                    });
                }
            });

        },
        initSelection: function (element, callback) {
            var id = $(element).val();
            if (id !== "") {
                var data = {
                    id: id,
                    text: id
                };
                callback(data);
            }
        },
        formatResult: format,
        formatSelection: format
    }).on('change', function () {
        var checkId = $(yearToSelect).select2('val');
        if (!isEmpty(checkId)) {
            $('#yearTo').val(checkId);
        } else {
            $('#yearTo').val("");
        }

    }).trigger('change');
}

//EngineSizeTo
$.findEngineSizeTo = function () {

    var prefix = "/data"
    var selectedEngineSize = $("#engineSizeFrom").val();

    $(engineSizeToSelect).select2('data', null).select2({
        placeholder: 'Maximum',
        allowClear: true,
        query: function (query) {
            var data;
            $.ajax({
                type: "POST",
                url: prefix + "/engineSizeTo",
                data: JSON.stringify(selectedEngineSize),
                contentType: "application/json",
                success: function (data) {
                    var results = [];
                    for (var i = 0; i < data.length; ++i) {
                        results.push({
                            id: data[i],
                            text: data[i]
                        });
                    }

                    query.callback({
                        results: results,
                        more: false
                    });
                }
            });

        },
        initSelection: function (element, callback) {
            var id = $(element).val();
            if (id !== "") {
                var data = {
                    id: id,
                    text: id
                };
                callback(data);
            }
        },
        formatResult: format,
        formatSelection: format
    }).on('change', function () {
        var checkId = $(engineSizeToSelect).select2('val');
        if (!isEmpty(checkId)) {
            $('#enginSizeTo').val(checkId);
        } else {
            $('#enginSizeTo').val("");
        }

    }).trigger('change');
}

//MileageTo
$.findMileageTo = function () {

    var prefix = "/data"
    var selectedMileage = $("#mileageFrom").val();

    $(mileageToSelect).select2('data', null).select2({
        placeholder: 'Maximum',
        allowClear: true,
        query: function (query) {
            var data;
            $.ajax({
                type: "POST",
                url: prefix + "/mileageTo",
                data: JSON.stringify(selectedMileage),
                contentType: "application/json",
                success: function (data) {
                    var results = [];
                    for (var i = 0; i < data.length; ++i) {
                        results.push({
                            id: data[i],
                            text: data[i]
                        });
                    }

                    query.callback({
                        results: results,
                        more: false
                    });
                }
            });

        },
        initSelection: function (element, callback) {
            var id = $(element).val();
            if (id !== "") {
                var data = {
                    id: id,
                    text: id
                };
                callback(data);
            }
        },
        formatResult: format,
        formatSelection: format
    }).on('change', function () {
        var checkId = $(mileageToSelect).select2('val');
        if (!isEmpty(checkId)) {
            $('#mileageTo').val(checkId);
        } else {
            $('#mileageTo').val("");
        }

    }).trigger('change');
}

function format(item) {
    return item.text;
};

//function formatPrice(item) {
//	return item.text+" GHC";
//};