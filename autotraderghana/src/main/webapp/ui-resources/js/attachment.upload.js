$(function () {

    function isEmpty(obj) {
        if (typeof obj == 'undefined' || obj === null || obj === '') return true;
        if (typeof obj == 'number' && isNaN(obj)) return true;
        if (obj instanceof Date && isNaN(Number(obj))) return true;
        return false;
    }

    function JsonToJSDate(jsonDate) {
        var reg = /-?\d+/;
        var m = reg.exec(jsonDate);
        return new Date(parseInt(m[0]));
    }

    Handlebars.registerHelper('date', function () {
        var date = this.createDate;
        return new Handlebars.SafeString(
            $.datepicker.formatDate('dd-mm-yy', JsonToJSDate(date))
        );
    });

    Handlebars.registerHelper('link-delete-pic', function () {
        var vehicleId = this.vehicleId;
        var idToUse = null;

        if (!isEmpty(this.id))
            idToUse = this.id;
        else
            idToUse = this.transientId;
        var result = '<button class="delete-row btn btn-small btn-warning" type="button" data-delete="picture" data-idToUse="' + idToUse + '" data-vehicleId="' + vehicleId + '">Delete</button>';
        return new Handlebars.SafeString(result);
    });


    var prefix = '/data';

    function getSearchConditions() {
        var searchConditions = {};
        searchConditions.vehicleId = $("#vehicleId").val();
        return searchConditions;
    }

    $.findVehicleAttachments = function () {
        var searchConditions = getSearchConditions();

        var resultHolder = $("#vehicle-attachment-holder");
        var source = $("#template-vehicle-attachment").html();
        var template = Handlebars.compile(source);
        console.log("find vehicle attachment");
        $.ajax({
            type: "POST",
            url: prefix + "/vehicleattachments",
            data: JSON.stringify(searchConditions.vehicleId),
            contentType: "application/json",
            success: function (results) {
                resultHolder.html(template({
                    attachments: results
                }));
                console.log("find vehicle attachment results", results);
                if (results.length > 0) {
                    $(".no-attachment").hide();
                } else {
                    $(".no-attachment").show();
                }
            }
        });
    };



    var searchConditions = getSearchConditions();

    if (!isEmpty(searchConditions.vehicleId) && searchConditions.vehicleId !== "NEW-REG") {
        $.findVehicleAttachments();
    } else {
        $(".no-attachment").show();
    }


    $(document).on('click', '[data-delete="picture"]', function (e) {
        var $this = $(this),

            vehicleId = $this.attr('data-vehicleid'),
            IdtoUse = $this.attr('data-idtouse');

        e.preventDefault()
        // confirm dialog
        alertify.confirm("Do you really want to delete this picture?", function (e) {
            if (e) {

                var deleteConditions = {};
                deleteConditions.vehicleId = vehicleId;
                deleteConditions.idToUse = IdtoUse;

                $.ajax({
                    type: "POST",
                    url: prefix + "/vehicleattachments/delete",
                    data: JSON.stringify(deleteConditions),
                    contentType: "application/json",
                    success: function (results) {
                        console.log("DeletePicture::", results);
                        if (results == "DELETED") {
                            $.findVehicleAttachments();
                            alertify.success("Picture was successfully deleted.");
                        } else {
                            alertify.error("Picture was not deleted");
                        }
                    }
                });

            } else {
                alertify.error("Picture was not deleted");
            }
        });
    });



});


$(document).ready(function () {

    var sessionId = $("#sessionId").val();

    $("#file_upload1").uploadifive({
        'checkScript': false,
        'uploadScript': '/uploadAttachments/processUpload/;jsessionid=' + sessionId,
        'buttonClass': 'btn btn-info',
        'buttonText': 'Upload Vehicle Attachments',
        'removeCompleted': true,
        'multi': true,
        'auto': true,
        'method': 'post',
        'height': 35,
        'fileType': 'image',
        'fileObjName': 'filedata',
        'width': 200,
        'formData': {
            'vehicleId': $('#vehicleId').val(),
        },
        'onUploadComplete': function () {
            $.findVehicleAttachments();
        }
    });
});