<%@ include file="/common/taglibs.jsp"%>
<meta name="menu" content="Login" />
<%-- <head>
<title><fmt:message key="login.title" /></title>
<meta name="menu" content="Login" />
</head>
<body id="login"> --%>
	<div class="account-container btm10">
		<div class="form-content clearfix">
			<form method="post" id="loginForm"
				action="<c:url value='/j_security_check'/>"
				
				class="form-signin" autocomplete="off">
				<h3 class="modal-hidden">
					<i class="iconbig-lock"></i>
					<fmt:message key="login.heading" />
				</h3>
				<div class="login-fields">
					<!-- <p>Sign in using your registered account:</p> -->
					<c:if test="${not empty error}">
						<div class="alert alert-error fade in">
							<fmt:message key="errors.password.mismatch" />
						</div>
					</c:if>
					<div class="field">
						<label for="j_username" id="username-label"><fmt:message key="label.username" />:</label>
						<input type="text" name="j_username" id="j_username"
							class="login username-field"
							placeholder="<fmt:message key="label.username"/>" required
							tabindex="1">
					</div>
					<!-- /field -->
					<div class="field">
						<label for="j_password"><fmt:message key="label.password" />:</label>
						<input type="password" class="login password-field"
							name="j_password" id="j_password" tabindex="2"
							placeholder="<fmt:message key="label.password"/>" required>
					</div>
					<!-- /password -->
				</div>
				<!-- /login-fields -->
				<div class="login-actions">
					<c:if test="${appConfig['rememberMeEnabled']}">
						<span class="login-checkbox"> 
							<label class="checkbox"> 
								<input id="rememberMe" name="_spring_security_remember_me" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="3" />
								<label class="choice" for="rememberMe">Keep me signed in</label>
							</label>
						</span>
					</c:if>  
                
					<input type="submit" name="submit" id="login"
						value="<fmt:message key='button.login'/>"
						class="btn-signin btn btn-primary" tabindex="4" /> <a href="/"
						class="btn-signin btn">Cancel</a>
				</div>
				<div class="login-social marg10-btm">
					<p>
						<span><fmt:message key="login.passwordHint" /></span>
						<span>
						<fmt:message key="login.signup">
							<fmt:param>
								<c:url value="/signup" />
							</fmt:param>
						</fmt:message>
						</span>
					</p>
				</div>

			</form>
		</div>
		<!-- /form-content -->
	</div>
	<!-- /account-container -->
	<%-- <c:set var="scripts" scope="request">
		<%@ include file="/scripts/login.js"%>
	</c:set> --%>
<script type="text/javascript">
<c:if test="${param.ajax}">
    var loginFailed = function(data, status) {
        $(".error").remove();
        $('#username-label').before('<div class="error">Login failed, please try again.</div>');
    };

    $("#login").live('click', function(e) {
        e.preventDefault();
        $.ajax({url: getHost() + "${ctx}/j_security_check",
            type: "POST",
            beforeSend: function(xhr){
                xhr.withCredentials = true;
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            },
            data: $("#loginForm").serialize(),
            success: function(data, status, xhr) {
                //console.log(xhr.getResponseHeader("Set-Cookie"));
                if (data.loggedIn) {
                    // success
                    dialog.dialog('close');
                    location.href = getHost() + '${ctx}/users';
                } else {
                    loginFailed(data);
                }
            },
            error: loginFailed
        });
    });
</c:if>
    $('#username').focus();
</script>
<!-- </body> -->