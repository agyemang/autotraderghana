<%@ include file="/common/taglibs.jsp"%>
<div class="span12 custom-margin-30 search-panel">
	<div class="nav-headers">
		<div class="bs-docs-example form-horizontal" >
			<form:form cssClass="no-margin-bottom" action="/vehicle/findVehicleForm" id="search-by-id-form">
				<div class="input-append" style="margin-bottom: 20px;">
					<input class="span9" id="appendedInputButtons" type="text" placeholder="Enter Vehicle ID">
					<input type="hidden" id="vehicleFindId" value="" name="vehicleId"/>
					<button class="btn btn-info" type="submit" id="search-by-id">Find</button>
				</div>
			</form:form>
			<form:form action="/vehicle/searchVehicleForm" modelAttribute="searchFilter" id="searchFilter-form">
			
			<input id="vehicleCondition" name="vehicleCondition" value="${searchFilter.vehicleCondition}" type="hidden">
			<input id="pageIndex" name="pageIndex" value="${searchFilter.pageIndex}" type="hidden">
			<input id="vehicleSortPattern" name="vehicleSortPattern" value="${searchFilter.vehicleSortPattern}" type="hidden">
			
			<input id="customerType" name="customerType" value="${searchFilter.customerType}" type="hidden">
			<input id="isNew24" name="isNew24" value="${searchFilter.isNew24}" type="hidden">
			<input id="isUpdated24" name="isUpdated24" value="${searchFilter.isUpdated24}" type="hidden">
						
			
			<ul id="myTab" class="nav nav-tabs vehicle-condition-tab">
				<li class="active" id="used-search"><a href="" data-toggle="tab" data-searchtype="used">Used Cars</a></li>
				<li class="" id="new-search"><a href="" data-toggle="tab" data-searchtype="new">New Cars</a></li>
			</ul>
			
			
			
			<div class="search-filter-content margin-top-10"> 
			
				<form:select path="vehicleMake" id="vehicleMake" cssClass="span12">
					<option></option>
					<form:options items="${makes}" itemValue="id" itemLabel="make" />
				</form:select>
				
				<input id="vehicleModelSelect" type="hidden" class="span12 no-margin-left" value="${searchFilter.vehicleModel.id}"/>
				<input id="vehicleModel" name="vehicleModel" value="${searchFilter.vehicleModel.id}" type="hidden">
				
				<form:select path="bodyType" class="select2 span12" tabindex="" data-placeholder="Select Body Type">
					<option></option>
					<form:options items="${bodyTypes}" itemValue="id" itemLabel="name" />
				</form:select>
				
				<form:select path="fuelType" class="select2 span12" tabindex="" data-placeholder="Select Fuel Type">
					<option></option>
					<form:options items="${fuelTypes}" itemValue="id" itemLabel="name" />
				</form:select>
				
				<form:select path="gearBoxType" class="select2 span12" tabindex="" data-placeholder="Select GearBox Type">
					<option></option>
					<form:options items="${gearBoxTypes}" itemValue="id" itemLabel="name" />
				</form:select>
				
				<h4 class="no-margin-top">Price</h4>
				<div class="row-fluid">
					<div class="span6">
						<form:select path="priceFrom" id="priceFrom" cssClass="span12 select2" data-placeholder="Minimum">
							<option></option>
							<form:options items="${priceFroms}"/>
						</form:select>
					</div>
					<div class="span6">
						<input id="priceToSelect" type="hidden" class="span12 no-margin-left" value="${searchFilter.priceTo}"/>
						<input id="priceTo" name="priceTo" value="${searchFilter.priceTo}" type="hidden">
					</div>
				</div>
				
				<label class="checkbox more"><form:checkbox path="iSMoreSearch"/> More Search Criteria </label>

				<div id="more-search-criteria-content" style="display: none;">
				
					<h4>Year Model</h4>
					<div class="row-fluid">
						<div class="span6">
							<form:select path="yearFrom" class="select2 span12" tabindex="" data-placeholder="Minimum">
								<option></option>
								<form:options items="${years}" />							
							</form:select>
						</div>
						<div class="span6">
							<input id="yearToSelect" type="hidden" class="span12 no-margin-left" value="${searchFilter.yearTo}"/>
							<input id="yearTo" name="yearTo" value="${searchFilter.yearTo}" type="hidden">
						</div>
					</div>
					
					<h4 class="no-margin-top">Engine Size</h4>
					<div class="row-fluid">
						<div class="span6">
							<form:select path="engineSizeFrom" class="select2 span12" tabindex="" data-placeholder="Minimum">
								<option></option>
								<form:options items="${engineSizes}" />
							</form:select>
						</div>
						<div class="span6">
							<input id="engineSizeToSelect" type="hidden" class="span12 no-margin-left" value="${searchFilter.engineSizeTo}"/>
							<input id="engineSizeTo" name="engineSizeTo" value="${searchFilter.engineSizeTo}" type="hidden">
						</div>
					</div>
					
					<h4 class="no-margin-top">Mileage</h4>
					<div class="row-fluid">
						<div class="span6">
							<form:select path="mileageFrom" class="select2 span12" tabindex="" data-placeholder="Minimum">
								<option></option>
								<form:options items="${mileageFroms}" />
							</form:select>
						</div>
						<div class="span6">
							<input id="mileageToSelect" type="hidden" class="span12 no-margin-left" value="${searchFilter.mileageTo}"/>
							<input id="mileageTo" name="mileageTo" value="${searchFilter.mileageTo}" type="hidden">
						</div>
					</div>
<!-- 					<h4 class="no-margin-top">Other</h4>
					<div class="span12" style="padding-bottom: 10px;">
						<label class="checkbox"><input type="checkbox" id=""> Acessory Air Condition </label> <label class="checkbox">
						<input type="checkbox" id=""> Leather upholstery </label> 
						<label class="checkbox"><input type="checkbox" id=""> Traction control </label>
						<label class="checkbox"><input type="checkbox" id=""> Four Wheel </label>
					</div> -->
				</div>
				<div style="padding-top: 10px;">
					<button type="submit" class="btn btn-info span12"
						id="search-by-filter">Show Results (&nbsp;<span id="search-result-label"></span>&nbsp;)</button>
				</div>
			</div>
			<div class="clearfix"></div>
		</form:form>
		</div>
	</div>
	<ul class="nav nav-tabs nav-stacked no-margin-bottom sub-menu">
		<li class="active"><a href="<c:url value="/sellCar/vehicleForm"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Post Car Advert</a></li>
		<li class=""><a href="<c:url value="/dealers"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Find Car Dealers</a></li>
		<li class="active"><a href="<c:url value="/vehicle/usedcars"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Find Used Cars</a></li>
		<li class=""><a href="<c:url value="/vehicle/newcars"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Find New Cars</a></li>
		<li class="active"><a href="<c:url value="/spareParts"/>"><i class="icon-download"></i>&nbsp;&nbsp;Find Spare Parts</a></li>
		<c:if test="${empty pageContext.request.remoteUser}">
		<li class=""><a href="<c:url value="/signup"/>"><i class="icon-ok"></i>&nbsp;&nbsp;Not Member Yet? Sign Up</a></li>
		</c:if>
	</ul>
	<div class="nav-headers nav-headers-custom">
		<div class="image-galery">
			<a class="group" rel="group1" href="<c:url value="/img/3_b.jpg"/>"><img
				src="<c:url value="/img/3_b.jpg"/>" /></a>
		</div>
	</div>
</div>