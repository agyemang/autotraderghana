<%@ include file="/common/taglibs.jsp"%>

<div class="container footer">
	<hr class="alt"/>
	<div class="row">
		<div class="span6">
			 &copy;&nbsp;<fmt:message key="copyright.year"/>&nbsp;<fmt:message key="webapp.name"/>. All right reserved.
		</div>
		<div class="span6">
			<ul>
				<li><a href="#">Terms Of Service</a></li>
				<li><a href="#">Privacy</a></li>
				<li><a href="<fmt:message key="company.url"/>">Developed By: <fmt:message key="company.name"/></a></li>
				
			</ul>
		</div>
	</div>
</div>