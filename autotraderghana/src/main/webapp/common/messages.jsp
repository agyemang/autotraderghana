<%@ include file="/common/taglibs.jsp"%>
<%-- Error Messages --%>
<c:if test="${not empty errors}">
    <div class="alert alert-error fade in">
        <a href="#" data-dismiss="alert" class="close">&times;</a>
        <c:forEach var="error" items="${errors}">
            <c:out value="${error}"/><br />
        </c:forEach>
    </div>
    <c:remove var="errors"/>
</c:if>

<%-- Success Messages --%>
<c:if test="${not empty successMessages}">
    <div class="alert alert-success fade in">
        <a href="#" data-dismiss="alert" class="close">&times;</a>
        <c:forEach var="msg" items="${successMessages}">
            <c:out value="${msg}"/><br />
        </c:forEach>
    </div>
    <c:remove var="successMessages" scope="session"/>
</c:if>


<c:if test="${not empty flash}">
	<div class="alert alert-info fade in">
		<a class="close" data-dismiss="alert">&times;�</a>${flash}
	</div>
</c:if>
<c:if test="${successMessage != null}">
	<div class="alert alert-success fade in">
		<a class="close" data-dismiss="alert">&times;�</a>
		<c:out value="${successMessage}" />
	</div>
</c:if>
<c:if test="${feedbackMessage != null}">
	<div class="alert alert-info fade in">
		<a class="close" data-dismiss="alert">&times;</a>
		<c:out value="${feedbackMessage}" />
	</div>
</c:if>
<c:if test="${errorMessage != null}">
	<div class="alert alert-error fade in">
		<a class="close" data-dismiss="alert">&times;�</a>
		<c:out value="${errorMessage}" />
	</div>
</c:if>