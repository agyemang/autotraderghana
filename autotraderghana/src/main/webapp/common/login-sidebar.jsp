<%@ include file="/common/taglibs.jsp"%>
<div class="span12 custom-margin-30 search-panel">
	<div class="nav-headers">
		<form class="bs-docs-example form-horizontal">
							<div class="input-append" style="margin-bottom:20px;">
								<input class="span9" id="appendedInputButtons" type="text" placeholder="Enter Vehicle ID">
								<button class="btn btn-success" type="button" id="search-by-id">Find</button>
							</div>
							
							<ul id="myTab" class="nav nav-tabs">
							  <li class="active"><a href="" data-toggle="tab" data-searchtype="used">Used Cars</a></li>
							  <li class=""><a href="" data-toggle="tab" data-searchtype="new">New Cars</a></li>
							</ul>
							<div class="search-filter-content">
							
								<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Vehicle Make</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen1"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Vehicle Make">
									<option></option>
									
								</select>
								<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Vehicle Model</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen2"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Vehicle Model">
									<option></option>
									
								</select>
								<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Body Type</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen3"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Body Type">
									<option></option>
									
								</select>
								<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Fuel Type</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen4"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Fuel Type">
									<option></option>
									
								</select>
								<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Gearbox Type</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen5"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Gearbox Type">
									<option></option>
									
								</select>
								<h4>Price</h4>
								<div class="row-fluid">
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_autogen6"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Min</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen7"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input">   </div>   <ul class="select2-results">   </ul></div></div><select class="select2 span12 select2-offscreen" data-placeholder="Min" tabindex="-1">
											
											<option></option>
											
										</select>
									</div>
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Max</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen8"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Max">
											<option></option>
											
										</select>
									</div>
								</div>
								<label class="checkbox more"><input type="checkbox" id="more-search-criteria"> More Search Criteria </label>
								
								<div id="more-search-criteria-content" style="display: none;">
								
								<h4>Year Model</h4>
								<div class="row-fluid">
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Min</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen9"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Min">
											<option></option>
											
										</select>
									</div>
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Max</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen10"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Max">
											<option></option>
											
										</select>
									</div>
								</div>
								<h4>Engine Size</h4>
								<div class="row-fluid">
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Min</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen11"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Min">
											<option></option>
											
										</select>
									</div>
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Max</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen12"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Max">
											<option></option>
											
										</select>
									</div>
								</div>
								<h4>Mileage</h4>
								<div class="row-fluid">
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Min</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen13"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Min">
											<option></option>
											
										</select>
									</div>
									<div class="span6">
										<div class="select2-container select2 span12" id="s2id_single-append"><a href="javascript:void(0)" onclick="return false;" class="select2-choice select2-default" tabindex="-1">   <span>Max</span><abbr class="select2-search-choice-close" style="display: none;"></abbr>   <div><b></b></div></a><input class="select2-focusser select2-offscreen" type="text" id="s2id_autogen14"><div class="select2-drop select2-with-searchbox" style="display:none">   <div class="select2-search">       <input type="text" autocomplete="off" class="select2-input" tabindex="-1">   </div>   <ul class="select2-results">   </ul></div></div><select id="single-append" class="select2 span12 select2-offscreen" tabindex="-1" data-placeholder="Max">
											<option></option>
											
										</select>
									</div>
								</div>
								<!--<select id="single-append" class="select2 span12" tabindex="-1" data-placeholder="Drive Type">
									<option></option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
								</select>-->
								<h4>Other</h4>
								<div class="span12" style="padding-bottom:10px;">
									<label class="checkbox"><input type="checkbox" id=""> Acessory Air Condition </label>
									<label class="checkbox"><input type="checkbox" id=""> Leather upholstery </label>
									<label class="checkbox"><input type="checkbox" id=""> Traction control </label>
									<label class="checkbox"><input type="checkbox" id=""> Four Wheel </label>
								</div>
								
								
								</div>
								<div style="padding-top:10px;">
									<button type="button" class="btn btn-success span12" id="search-by-filter">Show Results</button>
								</div>
							</div>
							<div class="clearfix"></div>
						</form>
	</div>
	<ul class="nav nav-tabs nav-stacked no-margin-bottom sub-menu">
		<li class="active"><a href="<c:url value="/signup"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Post Car Advert</a></li>
		<li class=""><a href="<c:url value="/carDealers"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Find Car Dealers</a></li>
		<li class="active"><a href="<c:url value="/usedCars"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Find Used Cars</a></li>
		<li class=""><a href="<c:url value="/newCars"/>"><i class="icon-th-list"></i>&nbsp;&nbsp;Find New Cars</a></li>
		<li class="active"><a href="<c:url value="/spareParts"/>"><i class="icon-download"></i>&nbsp;&nbsp;Find Spare Parts</a></li>
		<c:if test="${empty pageContext.request.remoteUser}">
		<li class=""><a href="<c:url value="/signup"/>"><i class="icon-ok"></i>&nbsp;&nbsp;Not Member Yet? Sign Up</a></li>
		</c:if>
	</ul>
	<div class="nav-headers nav-headers-custom">
		<div class="image-galery">
			<a class="group" rel="group1" href="<c:url value="/img/3_b.jpg"/>"><img
				src="<c:url value="/img/3_b.jpg"/>" /></a>
		</div>
	</div>
</div>