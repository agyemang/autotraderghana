<%@ include file="/common/taglibs.jsp"%>
<c:set var="item" value="${pageContext.request.requestURI}"/>
<c:set var="isForm" value="${fn:contains(item, 'vehicleForm') || fn:contains(item, 'editVehicleForm')}" />
<c:set var="isPreview" value="${fn:contains(item, 'vehiclePreview')}" />
<c:set var="isPayment" value="${fn:contains(item, 'paymentConfirmForm')}" />
<div class="row-fluid">
	<div class="span12">
		<div class="widget">
			<div class="widget-body">
				<c:choose>
					<c:when test="${not empty vehicleId }">
						<ul class="breadcrumb-beauty">
							<li class="none"><a data-original-title=""> Home </a></li>
							<li class="${isForm ? 'active' : 'none'}"><a href="<c:url value="/sellCar/editVehicleForm/${vehicleId}"/>" data-original-title=""> Vehicle Registration </a></li>
							<li class="${isPreview ? 'active' : 'none'}"><a href="<c:url value="/sellCar/previewVehicleForm/${vehicleId}"/>" data-original-title=""> Advert Preview </a></li>
							<li class="${isPayment ? 'active' : 'none'}"><a data-original-title=""> Payment & Confirmation</a></li>
						</ul>
					</c:when>
					<c:otherwise>
						<ul class="breadcrumb-beauty">
							<li class="none"><a data-original-title=""> Home </a></li>
							<li class="${isForm ? 'active' : 'none'}"><a href="" data-original-title=""> Vehicle Registration </a></li>
							<li class="${isPreview ? 'active' : 'none'}"><a data-original-title=""> Advert Preview </a></li>
							<li class="${isPayment ? 'active' : 'none'}"><a data-original-title=""> Payment & Confirmation</a></li>
						</ul>
					</c:otherwise>
				</c:choose>
				
			</div>
		</div>
	</div>
</div>
