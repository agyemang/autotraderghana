<%@ include file="/common/taglibs.jsp"%>
<div class="header-container-fixed-top header-container">

	<!--=== Top ===-->
	<div id="top">
		<div class="container">

			<div class="row-fluid">
				<ul class="loginbar inline">
					<li><a href="mailto:info@anybiz.com"><i class="icon-envelope-alt"></i> info@anybiz.com</a></li>
					<c:if test="${empty pageContext.request.remoteUser}">
				        <li>
				            <%-- <a href="<c:url value="/login"/>"><i class="icon-user"></i> <fmt:message key="login.title"/></a> --%>
				            <a href="<c:url value="/login?ajax=true"/>" data-target="#myModal" role="button" data-toggle="modal"><i class="icon-user"></i> <fmt:message key="login.title"/></a>
				            
				        </li>
				         <li>
				            <a href="<c:url value="/signup"/>"><i class="icon-user"></i> <fmt:message key="signup.title"/></a>
				        </li>
				    </c:if>
				    <c:if test="${pageContext.request.remoteUser != null}">
				     <li>
		            	<a href="<c:url value="/userform"/>"><i class="icon-user" style="color: #72c02c;"></i><fmt:message key="user.status"/>  ${pageContext.request.remoteUser}</a>
		            </li>
		            </c:if>
				    <c:if test="${not empty pageContext.request.remoteUser}">
				        <li>
				            <a href="<c:url value="/logout"/>"><i class="icon-user"></i> <fmt:message key="logout.title"/></a>
				        </li>
				    </c:if>
				    <c:if test="${pageContext.request.remoteUser != null}">
				    <li>
			            <a  href="<c:url value="/userform"/>">My Account</a>
			        </li>
			        <!--   <li>
			            <a id="status">My Account</a>
			        </li> -->
			        
			        </c:if>
				</ul>
			</div>

		</div>
		<!--/container-->
	</div>
	<!--/top-->

	<!--=== Header ===-->
	<div id="header">
		<div class="container">
			<!-- Logo -->
			<div id="logo">
				<a href="<c:url value='/'/>"><img src="<c:url value="/img/logo1.png"/>" alt="Logo"></a>
			</div>
			<!-- /logo -->

			<!-- Menu -->
			<div class="navbar">
				<div class="navbar-inner">
					<a class="btn btn-navbar custom-btn" data-toggle="collapse"
						data-target=".nav-collapse"> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
					</a>
					<%@ include file="/common/menu.jsp"%>
				</div>
				<!-- /navbar-inner -->
			</div>
			<!-- /navbar -->

		</div>
		<!-- /container -->
	</div>
	<!-- /header -->
	<div class="clearfix"></div>
</div>



<%--

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<button type="button" class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="brand" href="<c:url value='/'/>"><fmt:message
					key="webapp.name" /></a>
			<%@ include file="/common/menu.jsp"%>
			<c:if test="${pageContext.request.locale.language ne 'en'}">
				<div id="switchLocale">
					<a href="<c:url value='/?locale=en'/>"> <fmt:message
							key="webapp.name" /> in English
					</a>
				</div>
			</c:if>
		</div>
	</div>
</div>
--%>