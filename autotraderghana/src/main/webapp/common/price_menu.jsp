<%@ include file="/common/taglibs.jsp"%>
<c:set var="item" value="${pageContext.request.requestURI}"/>
<c:set var="isDealer" value="${fn:contains(item, 'admin/dealerPrice')}" />
<c:set var="isPrivate" value="${fn:contains(item, 'admin/privatePrice')}" />

<div class="row-fluid">
	<div class="span12">
	
	<ul class="nav nav-pills">
	  <li class="${isPrivate ? 'active' : 'none'}"><a href="<c:url value="/admin/settings/pricing/private"/>" data-original-title="">Dealer Price Settings</a></li>
	  <li class="${isDealer ? 'active' : 'none'}"><a href="<c:url value="/admin/settings/pricing/dealer"/>" data-original-title="">Private Seller Price Settings</a></li>
	</ul>
	</div>
</div>


