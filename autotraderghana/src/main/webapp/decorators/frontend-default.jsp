<!DOCTYPE html>
<%@ include file="/common/taglibs.jsp"%>
<html lang="en">
<head>
    <meta http-equiv="Cache-Control" content="no-store"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon">
    <title><decorator:title/> | <fmt:message key="webapp.name"/></title>

    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/bootstrap.min.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/style.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/menu/style-menu1.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/icon-style.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/bootstrap-responsive.min.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/bootstrap-rowlink/bootstrap-rowlink.min.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/style_responsive.css'/>" />
    <link href="<c:url value='/ui-resources/uploadifive/uploadifive.css'/>" media="all" rel="stylesheet" type="text/css" />
    <link href="<c:url value='/ui-resources/alertify-theme/alertify.core.css'/>" media="all" rel="stylesheet" type="text/css" />
	<link href="<c:url value='/ui-resources/alertify-theme/alertify.bootstrap.css'/>" media="all" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/source/jquery.fancybox.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/style_responsive.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/select2-3.3.2/select2.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/font-awesome/css/font-awesome.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/ui-resources/css/pagination.css'/>" />
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    

    <c:if test="${!jqueryProvided}">
    <script type="text/javascript" src="<c:url value='/scripts/lib/jquery-1.9.1.min.js'/>"></script>
    </c:if>
    <script type="text/javascript" src="<c:url value='/scripts/lib/bootstrap-2.2.1.min.js'/>"></script>
     <script type="text/javascript" src="<c:url value='/ui-resources/js/jquery-ui-1.10.3.custom.js'/>"></script>
    <script src="<c:url value='/ui-resources/uploadifive/jquery.uploadifive.min.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/ui-resources/js/alertify.min.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/ui-resources/js/handlebars.js'/>" type="text/javascript"></script>
    <script type="text/javascript" src="<c:url value='/ui-resources/js/underscore-min.js'/>"></script>
    <%-- <script type="text/javascript" src="<c:url value='/ui-resources/js/image-gallery.js'/>"></script> --%>
    <script type="text/javascript" src="<c:url value='/ui-resources/js/jquery.mousewheel-3.0.6.pack.js'/>"></script>
    <%-- <script type="text/javascript" src="<c:url value='/ui-resources/source/jquery.fancybox.js'/>"></script> --%>
    <script type="text/javascript" src="<c:url value='/scripts/lib/plugins/jquery.cookie.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/ui-resources/select2-3.3.2/select2.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/ui-resources/js/json2.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/ui-resources/js/jquery.pagination.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/script.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/ui-resources/js/global.js'/>"></script>
     <script type="text/javascript" src="<c:url value='/ui-resources/js/jquery.tablesorter.min.js'/>"></script>
     <script type="text/javascript" src="<c:url value='/ui-resources/js/spin.min.js'/>"></script>
        
    <decorator:head/>
 </head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true"/><decorator:getProperty property="body.class" writeEntireProperty="true"/>>
    <c:set var="currentMenu" scope="request"><decorator:getProperty property="meta.menu"/></c:set>
<%@ include file="/common/header.jsp" %>
<div class="container">
		
		<div class="row-fluid">
		<c:choose>
			<c:when test="${currentMenu == 'SellCarMenu'}">
				<div class="span12">
					<%@ include file="/common/messages.jsp" %>
					<decorator:body/>	
				</div><!--/.span12 -->
			</c:when>
			<c:otherwise>
			<div class="span3 ">
				<%@ include file="/common/sidebar.jsp" %>
				<%-- <c:choose>
					<c:when test="${currentMenu == 'Login'}">
						<%@ include file="/common/login-sidebar.jsp" %>
					</c:when>
					<c:otherwise>
						<%@ include file="/common/sidebar.jsp" %>
					</c:otherwise>	
				</c:choose> --%>
							
			</div><!--/.span3 -->
			<div class="span9">
				<%@ include file="/common/messages.jsp" %>
				<decorator:body/>	
			</div><!--/.span9 -->
			</c:otherwise>
		</c:choose>
	
			
	</div><!--/.row-fluid -->
</div><!--/.container -->
<%@ include file="/common/footer.jsp" %>
    
<%= (request.getAttribute("scripts") != null) ?  request.getAttribute("scripts") : "" %>

<script type="text/javascript">

    var secure = true;

    /* var getHost = function() {
        var port = (window.location.port == "8080") ? ":8443" : "";
        return ((secure) ? 'https://' : 'http://') + window.location.hostname + port;
    };
 */
 
 var getHost = function() {
	 var secure = false;
     var port =":"+ window.location.port;
     return ((secure) ? 'https://' : 'http://') + window.location.hostname + port;
 };

    var dialog = $('.modal-body');

    $(document).ready(function() {
        // see if the user is logged in

        /* $.ajax({url: '${ctx}/login?ajax=true',
            type: 'GET',
            success: function(data) {
                dialog.html(data);
                 dialog.dialog({
                    autoOpen: false,
                    title: 'Authentication Required'
                }); 
            }}); */

/*         $('#demo').click(function() {
            dialog.dialog('open');
            // prevent the default action, e.g., following a link
            return false;
        }); */

        $('#status').click(function() {
            $.ajax({url: getHost() + '${ctx}/api/login.json',
                type: 'GET',
                beforeSend: function(xhr){
                    xhr.withCredentials = true;
                },
                success: function(data, status) {
                    $(".status").remove();
                    $("#status").after("<span class='status'> Logged In: " + data.loggedIn + "</span>");
                }
            });
        })
    });
</script>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3 id="myModalLabel"><i class="iconbig-lock"></i>
					<fmt:message key="login.heading" /></h3>
  </div>
  <div class="modal-body">
    
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>
<div class="rc_notify_main"><div id="rc_notify"></div></div>
</body>
</html>
