<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="signup.title" /></title>
<meta name="menu" content="SellCarMenu" />

</head>




<body class="sellCar" />
 <jsp:include page="/common/vehicle_registration_menu.jsp">
       <jsp:param name="vehicleId" value="${vehicle.vehicleId}" />
</jsp:include>
<spring:bind path="vehicle.*">
	<c:if test="${not empty status.errorMessages}">
		<div class="alert alert-error fade in">
			<a href="#" data-dismiss="alert" class="close">&times;</a>
			Error encountered while trying to save registration. Please correct
			error(s) and try again!
		</div>
	</c:if>
</spring:bind>
<spring:hasBindErrors name="vehicle">
	<script type="text/javascript">
		$(document).ready(function() {
			$("div.control-group>div.controls>.error").parent().parent().addClass("error");
		});
	</script>
</spring:hasBindErrors>

<c:choose>
	<c:when test="${empty vehicle.vehicleId}">
		<input type="hidden" id="vehicleId" value="NEW-REG">
	</c:when>
	<c:otherwise>
		<input type="hidden" id="vehicleId" value="${vehicle.vehicleId}">
	</c:otherwise>
</c:choose>
<input type="hidden" id="sessionId" value="${sessionId}">

<div class="bs-docs-example bs-docs-example-2 reg-vehicle" style="margin-bottom: 20px;">
	<form:form modelAttribute="vehicle" action="/sellCar/editVehicleForm/${vehicle.vehicleId}">
	
		<div class="row-fluid ">

			<div class="form-horizontal">
					
					<div class="public-profile-2">
				<h4>Details about the car you're selling</h4>
					<div class="row-fluid no-margin margin-bottom-10">
						<div class="control-group no-margin span6">
						<label class="control-label" for="inputEmail">Seller Type</label>
						<div class="controls fix-radio">
							<form:radiobuttons path="customer.userType" items="${userType}" element="label" cssClass="fix-me-label" itemLabel="value" disabled="true"/>
							<form:errors path="customer.userType" cssClass="error help-inline inline" element="span" />
						</div>
					</div>
					<div class="control-group no-margin span6">
						<label class="control-label" for="inputEmail">Vehicle Condition</label>
						<div class="controls fix-radio">
							<form:radiobuttons path="vehicleCondition" items="${vehicleCondition}" element="label" cssClass="fix-me-label"/>
							<form:errors path="vehicleCondition" cssClass="error help-inline inline" element="span" />
						</div>
					</div>
					
					</div>
					<div class="row-fluid no-margin margin-bottom-5">
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Vehicle Make</label>
							<div class="controls">
								<form:select path="vehicleMake" id="vehicleMake" cssClass="span12">
									<option></option>
									<form:options items="${makes}" itemValue="id" itemLabel="make" />
									
								</form:select>
								<form:errors path="vehicleMake" cssClass="error help-inline inline" element="span" />
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Vehicle Model</label>
							<div class="controls">
								<input id="vehicleModelSelect" type="hidden" class="span12" value="${vehicle.vehicleModel.id}"/>
								<input id="vehicleModel" name="vehicleModel" value="${vehicle.vehicleModel.id}" type="hidden">
								<%-- <form:hidden path="vehicleModel" value="${vehicle.vehicleModel.id}"/> --%>
								<form:errors path="vehicleModel" cssClass="error help-inline inline" element="span" />
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Vehicle Year</label>
							<div class="controls">
								<form:select path="year" class="select2 span12" tabindex="" data-placeholder="Select Vehicle Year">
									<option></option>
									<form:options items="${years}" />
									
								</form:select>
								<form:errors path="year" cssClass="error help-inline inline" element="span" />
							</div>
						</div>
					</div>
					<div class="row-fluid no-margin margin-bottom-5">
					
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Body Type</label>
							<div class="controls">
								<form:select path="bodyType" class="select2 span12" tabindex="" data-placeholder="Select Body Type">
									<option></option>
									<form:options items="${bodyTypes}" itemValue="id" itemLabel="name" />
								</form:select>
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">GearBox Type</label>
							<div class="controls">
								<form:select path="gearBoxType" class="select2 span12" tabindex="" data-placeholder="Select GearBox Type">
									<option></option>
									<form:options items="${gearBoxTypes}" itemValue="id" itemLabel="name" />
								</form:select>
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Fuel Type</label>
							<div class="controls">
								<form:select path="fuelType" class="select2 span12" tabindex="" data-placeholder="Select Fuel Type">
									<option></option>
									<form:options items="${fuelTypes}" itemValue="id" itemLabel="name" />
								</form:select>
							</div>
						</div>
							
					</div>
					<div class="row-fluid no-margin margin-bottom-5">
						
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Engine Size</label>
							<div class="controls">
								<form:select path="engineSize" class="select2 span12" tabindex="" data-placeholder="Select Engine Size">
									<option></option>
									<form:options items="${engineSizes}" />
								</form:select>
							</div>
						</div>
						<div class="control-group span4 no-margin ">
							<label class="control-label" for="inputEmail">Color</label>
							<div class="controls">
								<form:select path="color" class="select2 span12" tabindex="" data-placeholder="Select Color">
									<option></option>
									<form:options items="${colors}" itemValue="id" itemLabel="name" />
								</form:select>
							</div>
						</div>
						
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Color Type</label>
							<div class="controls">
								<form:select path="colorType" class="select2 span12" tabindex="" data-placeholder="Select Color Type">
									<option></option>
									<form:options items="${colorTypes}" itemValue="id" itemLabel="name" />
								</form:select>
							</div>
						</div>	
					</div>
					<div class="row-fluid no-margin margin-bottom-10">
						
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Door Count</label>
							<div class="controls">
								<form:select path="doorCount" class="select2 span12" tabindex="" data-placeholder="Select Door Count">
									<option></option>
									<form:options items="${doorCount}" />
								</form:select>
							</div>
						</div>
						
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Mileage</label>
							<div class="controls">
								<form:input path="mileage"  cssErrorClass="error" placeholder="Enter Mileage" cssClass="span12"/>
								<form:errors path="mileage" cssClass="error help-inline inline" element="span" />
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail"></label>
							<div class="controls">
								<label class="checkbox inline">
									<form:checkbox path="includeMileageAd" /> Mileage in Advert?
								</label>
							</div>
						</div>
					</div>
					
					
					
					
					
					<div class="row-fluid no-margin margin-bottom-5_">
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Price</label>
							<div class="controls">
								<form:input path="price"  cssClass="span12" placeholder="Enter Asking Price" />
								<form:errors path="price" cssClass="error help-inline inline" element="span" />
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Price Currency</label>
							<div class="controls">
								<form:select path="priceCurrency" class="select2 span12" tabindex="" data-placeholder="Select Currency">
									<option></option>
									<form:options items="${currencies}" itemValue="id" itemLabel="name" />
									
								</form:select>
								<form:errors path="priceCurrency" cssClass="error help-inline inline" element="span" />
							</div>
						</div>
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail">Price Info</label>
							<div class="controls">
								<form:select path="priceInfo" class="select2 span12" tabindex="" data-placeholder="Select Price Info">
									<option></option>
									<form:options items="${askingPriceInfos}" itemValue="id" itemLabel="name" />
								</form:select>
							</div>
						</div>
						
					</div>
					<div class="row-fluid no-margin margin-bottom-10">
						
						
						<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail"></label>
							<div class="controls">
								<label class="checkbox inline">
									<form:checkbox path="includePriceAd" /> Include Price in Ad?
								</label>
							</div>
						</div>
						
					</div>
					<br>
					<div class="row-fluid no-margin margin-bottom-5">
						<div class="control-group span12 no-margin">
							<label class="control-label">Vehicle Description</label>
							<div class="controls">
								<!-- <span class="help-block" style="margin-top: 5px;"></span> -->
								<form:textarea path="vehicleNotes" cssClass="span8" rows="3" placeholder="Enter the vehicle description you want to appear on your advert"/>
							</div>
						</div>
						
					</div>
				</div>
				</div>
			
		</div>

		<div class="public-profile-2">
			<h4>Select Vehicle Accessories</h4>
		
			<div class="row-fluid ">
				<span class="bs-docs-example safety span4 fix-check"> <form:checkboxes
						path="chosen_safety" items="${safetyVehicleAccessories}"
						cssClass="checkbox" itemValue="id" itemLabel="name" element="label" />
				</span> 
				<span class="bs-docs-example interior span4 fix-check"> <form:checkboxes
						path="chosen_interior" items="${interiorVehicleAccessories}"
						cssClass="checkbox" itemValue="id" itemLabel="name" element="label" />
				</span> 
				<span class="bs-docs-example other span4 fix-check"> <form:checkboxes
						path="chosen_other" items="${otherVehicleAccessories}"
						cssClass="checkbox" itemValue="id" itemLabel="name" element="label" />
				</span>
		
			</div>
		</div>
		
		<div class="public-profile-2">
			<h4>Attach Vehicle Images</h4>
		
			<div class="row-fluid ">
				<input type="file" name="file_upload1" id="file_upload1" />
				<div class="bs-docs-example form-horizontal attachment margin-top-10">

					<div id="vehicle-attachment-holder"></div>
					<p class="lead no-attachment" style="font-size: 15px; display: none;">
						Vehicle pictures not uploaded. Please upload atleast one photo.</p>
				</div>

			</div>
		</div>
		<div class="form-horizontal">
		<div class="public-profile-2">
			<h4>Advertising Details</h4>
		
			<div class="row-fluid no-margin margin-bottom-10">
                <div class="control-group span4 no-margin">
                    <appfuse:label styleClass="control-label" key="user.address.city"/>
                    <div class="controls">
                        <form:input path="advertisingDetails.city" id="address.city" value="${vehicle.customer.address.city }" cssClass="span12" disabled="true"/>
                        <form:errors path="advertisingDetails.city" cssClass="error help-inline inline" element="span" />
                    </div>
                </div>
                <div class="control-group span4 no-margin">
                    <appfuse:label styleClass="control-label" key="user.address.country"/>
                    <div class="controls">
                         <form:input path="advertisingDetails.country" id="address.country" value="${vehicle.customer.address.country }" cssClass="span12" disabled="true"/>
                         <form:errors path="advertisingDetails.country" cssClass="error help-inline inline" element="span" />
                    </div>
                </div>
                 <div class="control-group span4 no-margin">
                 <label class="control-label" for="advertisingDetails.contactPerson">Contact Person</label>		  
		            <div class="controls">
		                <form:input path="advertisingDetails.contactPerson"  value="${vehicle.customer.fullName}" cssClass="span12" disabled="true"/>
		                <form:errors path="advertisingDetails.contactPerson" cssClass="help-inline"/>
		            </div>
		        </div>
		          
		    </div>
		    <div class="control-group span4 no-margin">		  
		            <appfuse:label styleClass="control-label" key="user.email"/>
		            <div class="controls">
		                <form:input path="advertisingDetails.email" id="email" value="${vehicle.customer.email}" cssClass="span12" disabled="true"/>
		                <form:errors path="advertisingDetails.email" cssClass="help-inline"/>
		            </div>
		        </div>
		    <div class="row-fluid no-margin margin-bottom-10">
		        <div class="control-group span4 no-margin">
		            <appfuse:label styleClass="control-label" key="user.phoneNumber"/>
		            <div class="controls">
		                <form:input path="advertisingDetails.phoneNumber1" id="phoneNumber" value="${vehicle.customer.phoneNumber}" cssClass="span12" disabled="true"/>
		                <form:errors path="advertisingDetails.phoneNumber1" cssClass="error help-inline inline" element="span" />
		            </div>
		        </div>
		        <div class="control-group span4 no-margin">
		            <appfuse:label styleClass="control-label" key="user.phoneNumber"/>
		            <div class="controls">
		                <form:input path="advertisingDetails.phoneNumber2" id="phoneNumber" cssClass="span12"/>
		                <form:errors path="advertisingDetails.phoneNumber2" cssClass="error help-inline inline" element="span" />
		            </div>
		        </div>
		        
		        
		</div>
			<div class="row-fluid no-margin margin-bottom-10">
			<div class="control-group span4 no-margin">
							<label class="control-label" for="inputEmail"></label>
							<div class="controls">
								<label class="checkbox inline">
									<form:checkbox path="includeTelephoneInAd" /> Include Tel. in Ad?
								</label>
							</div>
						</div>
				<div class="control-group span4 no-margin">
					<label class="control-label" for="inputEmail">Tel. 1 Contact Time</label>
					<div class="controls">
						<form:select path="advertisingDetails.phoneNumber1Info" class="select2 span12"
							tabindex="" data-placeholder="Select Contact Time">
							<option></option>
							<form:options items="${telephoneAdditionalInfo}" itemValue="id"
								itemLabel="name" cssClass="span12"/>
						</form:select>
					</div>
				</div>
				<div class="control-group span4 no-margin">
					<label class="control-label" for="inputEmail">Tel. 2 Contact Time</label>
					<div class="controls">
						<form:select path="advertisingDetails.phoneNumber2Info" class="select2 span12"
							tabindex="" data-placeholder="Select Contact Time">
							<option></option>
							<form:options items="${telephoneAdditionalInfo}" itemValue="id"
								itemLabel="name" cssClass="span12"/>
						</form:select>
					</div>
				</div>
				<div class="control-group span4 no-margin">
					<label class="control-label" for="inputEmail"></label>
					<div class="controls">
						
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="span12 form-actions no-margin">
			<button type="submit" class="btn btn-warning pull-right" name="save">
				<i class="icon-ok icon-white"></i>
				Save and Continue
			</button>
		</div>
		<div class="clearfix"></div>
		
</form:form>
</div>
<script src="<c:url value='/ui-resources/js/attachment.upload.js'/>" type="text/javascript"></script>
<script id="template-vehicle-attachment" type="text/x-handlebars-template">
    {{#if attachments}}
	<div class="row-fluid">
		{{#each attachments}}
			<span class="img-thumb">
				<img src="data:image/png;base64,{{picture}}" alt="Picture" class="pic-thumb"/>
				{{link-delete-pic}}
			</span>
        {{/each}}
	</div>

    {{/if}}

</script>