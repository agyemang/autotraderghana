<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="signup.title" /></title>
<meta name="menu" content="DetailsMenu" />
</head>




<body class="preview" />


<div class="row-fluid">

	<div class="span12">

		<div class="row-fluid vehicle-details">
			<h4 class="title-vehicle" style="margin-bottom: 0;">
				${vehicle.displayTitle }
				<c:choose>
                            	<c:when test="${pageContext.request.remoteUser == vehicle.customer.username}">
					<span class="pull-right"><a href="<c:url value="/sellCar/editVehicleForm/${vehicle.vehicleId}"/>"
						class="btn btn-success btn-small">Edit Vehicle Details</a></span>
				</c:when>
				<c:otherwise>
				<c:choose>
                            	<c:when test="${vehicle.includePriceAd}">
                            		<span class="pull-right">Price : <a>${vehicle.price}&nbsp;${vehicle.priceCurrency}</a></span>
                            	</c:when>
                            	<c:otherwise>
                            		<span class="pull-right">Price : <a>Contact Seller</a></span>
                            	</c:otherwise>
                            </c:choose>
				
				</c:otherwise>
				</c:choose>

				
			</h4>
			<div class="row-fluid">
				<div class="span8">
					<div class="span12 image-detail">
						<div class="auto image-single">
							<img src="<c:url value="/img/4_b.jpg"/>" class="centered">
						</div>
					</div>
				</div>

				<div class="span4 special">

					<div class="row-fluid">
						<div class="span12">
							<form class="form-horizontal preview-details margin-top-10">

								<div class="cat-liked">
									Make : <a>${vehicle.vehicleMake.make}</a>
								</div>
								<div class="cat-liked">
									Model : <a>${vehicle.vehicleModel.model}</a>
								</div>

								<div class="cat-liked">
									Year Model : <a><fmt:formatDate value="${vehicle.year}"
											pattern="yyyy" /></a>
								</div>
								<c:if test="${not empty vehicle.bodyType}">
									<div class="cat-liked">
										Body Type : <a>${vehicle.bodyType}</a>
									</div>
								</c:if>
								<c:if test="${not empty vehicle.color}">
									<div class="cat-liked">
										Color : <a>${vehicle.color}</a>
									</div>
								</c:if>
								<c:if test="${not empty vehicle.colorType}">
									<div class="cat-liked">
										Color Type : <a>${vehicle.colorType}</a>
									</div>
								</c:if>
								<c:if test="${not empty vehicle.gearBoxType}">
									<div class="cat-liked">
										Gearbox Type : <a>${vehicle.gearBoxType}</a>
									</div>
								</c:if>


								<c:choose>
									<c:when
										test="${not empty vehicle.mileage and vehicle.includeMileageAd }">
										<div class="cat-liked">
											Mileage : <a>${vehicle.mileage}</a>
										</div>
									</c:when>
									<c:otherwise>
										<div class="cat-liked">
											Mileage : <a>Not Specified</a>
										</div>
									</c:otherwise>
								</c:choose>

								<c:if test="${not empty vehicle.transmission}">
									<div class="cat-liked">
										Transmission : <a>${vehicle.transmission}</a>
									</div>
								</c:if>
								<c:if test="${not empty vehicle.fuelType}">
									<div class="cat-liked">
										Fuel Type : <a>${vehicle.fuelType}</a>
									</div>
								</c:if>
								<c:if test="${not empty vehicle.engineSize}">
									<div class="cat-liked">
										Engine Size : <a>${vehicle.engineSize}</a>
									</div>
								</c:if>
								<c:if test="${not empty vehicle.doorCount}">
									<div class="cat-liked">
										Door Count : <a>${vehicle.doorCount}</a>
									</div>
								</c:if>
								<div class="cat-liked">
									Condition : <a>${vehicle.vehicleCondition}</a>
								</div>

								<c:choose>
									<c:when test="${vehicle.includePriceAd}">
										<div class="cat-liked">
											Asking Price : <a>${vehicle.price}&nbsp;${vehicle.priceCurrency}</a>
										</div>
									</c:when>
									<c:otherwise>
										<div class="cat-liked">
											Asking Price : <a>Contact Seller</a>
										</div>
									</c:otherwise>
								</c:choose>

								<div class="cat-liked">
									Seller Type : <a>${vehicle.customer.userType.value}</a>
								</div>
								<div class="cat-liked">
									Last Updated: <a><fmt:formatDate
											value="${vehicle.updateDate}" pattern="dd-MM-yyyy" /></a>
								</div>

								
							</form>
						</div>

					</div>
					<div class="row-fluid ">
						<div class="span12 contact">
							<div class="cat-liked">
								Contact Person&nbsp;:&nbsp;<a>${vehicle.customer.fullName}</a>
							</div>
							<div class="cat-liked">
								Tel&nbsp;:&nbsp;<a>${vehicle.customer.phoneNumber}</a>
							</div>
							<div class="cat-liked">
								Email&nbsp;:&nbsp;<a>${vehicle.customer.email}</a>
							</div>
						</div>

					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="change-font">
		<c:if
			test="${not empty vehicle.chosen_safety || not empty vehicle.chosen_interior || not empty vehicle.chosen_other }">
			<div class="row-fluid margin-top-10">
				<div class="span12  bs-docs-example bs-docs-example-2 bs-docs-example-3 accessory ">
					<c:if test="${not empty vehicle.chosen_safety}">
						<div class="span12 no-margin">
							<p class="accessory-list">
								<span>Safety :</span>
								<c:forEach items="${vehicle.chosen_safety}" var="safety">
									<span class="comma">,&nbsp;</span>
									<span><c:out value="${safety.name}" /></span>
								</c:forEach>

							</p>
						</div>
					</c:if>

					<c:if test="${not empty vehicle.chosen_interior}">
						<div class="span12 no-margin">
							<p class="accessory-list">
								<span>Interior :</span>
								<c:forEach items="${vehicle.chosen_interior}" var="interior">
									<span class="comma">,&nbsp;</span>
									<span><c:out value="${interior.name}" /></span>
								</c:forEach>
							</p>
						</div>
					</c:if>
					<c:if test="${not empty vehicle.chosen_other}">
						<div class="span12 no-margin">
							<p class="accessory-list">
								<span>Other :</span>
								<c:forEach items="${vehicle.chosen_other}" var="other">
									<span class="comma">,&nbsp;</span>
									<span><c:out value="${other.name}" /></span>
								</c:forEach>

							</p>
						</div>
					</c:if>
				</div>

			</div>
		</c:if>
		<c:if test="${not empty vehicle.vehicleNotes }">
			<div class="row-fluid margin-top-10">
				<div class="span12  bs-docs-example bs-docs-example-2 description">
					<!--<h4 style="padding-left: 20px;">Description</h4>-->
					<p>${vehicle.vehicleNotes }</p>
				</div>

			</div>
		</c:if>
		</div>
	</div>



</div>

