<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="mainMenu.title" /></title>
<meta name="menu" content="MainMenu" />
</head>
<body class="home">
	<div class="bs-docs-example bs-docs-example-2 latest" style="margin-bottom: 20px;">
		<div class="row-fluid">
			<div class="row-fluid ">
				<div class="span12 list-images no-margin-left">
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
				</div>
				<div class="span12 list-images no-margin-left">
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bs-docs-example bs-docs-example-2 feature" style="margin-bottom: 20px;">
		<div class="row-fluid">
			<div class="row-fluid ">
				<div class="span12 list-images no-margin-left">
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
					<div class="span3">
						<a href="#" class="thumbnail"><img
							src="http://placehold.it/160x120" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid advert">
		<div class="span12">
			<div class="span4 galery-home">

				<div class="image-galery">
					<a class="group" rel="group1" href="img/1_b.jpg"><img
						src="img/1_b.jpg" /></a>
				</div>
			</div>
			<div class="span4 galery-home">

				<div class="image-galery">
					<a class="group" rel="group1" href="img/1_b.jpg"><img
						src="img/1_b.jpg" /></a>
				</div>
			</div>
			<div class="span4 galery-home">

				<div class="image-galery">
					<a class="group" rel="group1" href="img/1_b.jpg"><img
						src="img/1_b.jpg" /></a>
				</div>
			</div>
		</div>
	</div>
	<%--><fmt:message key="mainMenu.heading"/></h2> <p><fmt:message
	key="mainMenu.message"/></p> <ul class="glassList"> <li> <a
	href="<c:url value='/userform'/>"><fmt:message key="menu.user"/></a>
	</li> <li> <a href="<c:url value='/fileupload'/>"><fmt:message
	key="menu.selectFile"/></a> </li> </ul>--%>
</body>
