<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="mainMenu.title" /></title>
<meta name="menu" content="NewCarsMenu" />
<script type="text/javascript" src="<c:url value='/ui-resources/js/vehicle.search.js'/>"></script>
<script type="text/javascript" src="<c:url value='/ui-resources/bootstrap-rowlink/bootstrap-rowlink.js'/>"></script>
</head>
<body class="home">
	<div class="bs-docs-example bs-docs-example-2 vehicle-result"
		style="margin-bottom: 20px;">
		
		
		<div id="spinner"></div>
		<h4>
			Search Result: <span id="vehicle-count"></span> item(s) found
		</h4>
		
		<div class="nav-headers nav-headers-2 ">
		<ul id="myTab" class="nav nav-tabs my-search-tab">
				<li class="active" id="search-all"><a href="" data-toggle="tab" data-searchtype="all">All</a></li>
				<li class="" id="search-dealer"><a href="" data-toggle="tab" data-searchtype="dealer">Dealers</a></li>
				<li class="" id="search-private"><a href="" data-toggle="tab" data-searchtype="private">Private Seller</a></li>
				<li class="" id="new-search-24"><a href="" data-toggle="tab" data-searchtype="new24">New, 24H</a></li>
				<li class="" id="update-search-24"><a href="" data-toggle="tab" data-searchtype="update24">Updated, 24H</a></li>
			</ul>
	</div>
		
		
		
		<div class="row-fluid">
			<div class="span6">
				<input id="sortPattern" class="span8" type="hidden">
					
			</div>
		</div>

		<div class="pagination-holder"></div>
		<div class="pagination-clear"></div>
		<div id="vehicle-list-holder">
			
		</div>
		
		<div class="pagination-holder"></div>
		<div class="pagination-clear"></div>
		<div class="clearfix"></div>
	</div>

</body>
