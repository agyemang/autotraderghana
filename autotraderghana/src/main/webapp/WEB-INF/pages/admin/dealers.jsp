<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="mainMenu.title" /></title>
<meta name="menu" content="AdminMenu" />
<script type="text/javascript" src="<c:url value='/ui-resources/js/admin.dealer.search.js'/>"></script>
<script type="text/javascript" src="<c:url value='/ui-resources/bootstrap-rowlink/bootstrap-rowlink.js'/>"></script>
</head>
<div class="span10">
	<div class="bs-docs-example bs-docs-example-2 vehicle-result"
		style="margin-bottom: 20px;">
		
		
		<h4>
			Search Result: <span id="dealer-count"></span> item(s) found
		</h4>

		<div class="pagination-holder"></div>
		<div class="pagination-clear"></div>
		<div id="dealer-list-holder">
			
		</div>
		
		<div class="pagination-holder"></div>
		<div class="pagination-clear"></div>
		<div class="clearfix"></div>
	</div>
</div>

