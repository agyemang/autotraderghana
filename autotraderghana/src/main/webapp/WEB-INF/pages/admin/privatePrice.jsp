<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="signup.title" /></title>
<meta name="menu" content="AdminMenu" />

</head>




<body class="" />

<spring:bind path="postPrice.*">
	<c:if test="${not empty status.errorMessages}">
		<div class="alert alert-error fade in">
			<a href="#" data-dismiss="alert" class="close">&times;</a>
			Error encountered while trying to save registration. Please correct
			error(s) and try again!
		</div>
	</c:if>
</spring:bind>
<spring:hasBindErrors name="postPrice">
	<script type="text/javascript">
		$(document).ready(function() {
			$("div.control-group>div.controls>.error").parent().parent().addClass("error");
		});
	</script>
</spring:hasBindErrors>


<div class="span10">
	<%@ include file="/common/price_menu.jsp" %>
	<form:form modelAttribute="postPrice" action="/admin/settings/pricing/private" cssClass="form-horizontal span6" name="calculator">
		<form:hidden path="id"/>
		<form:hidden path="userType"/>
		<form:hidden path="amountAfterTax"/>

					
		<h4>Private Seller Vehicle Post Price Settings</h4>

		<div class="control-group">
			<label class="control-label"> Description</label>
			<div class="controls">
				<!-- <span class="help-block" style="margin-top: 5px;"></span> -->
				<form:textarea path="postAmountDescription" cssClass="span8" rows="2" placeholder="Description"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputEmail">amountBeforeTax</label>
			<div class="controls">
				<form:input path="amountBeforeTax"  cssClass="span12" placeholder="Price Exclusive Tax" onkeyup="calc_inc()" />
				<form:errors path="amountBeforeTax" cssClass="error help-inline inline" element="span" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputEmail">Tax Rate %</label>
			<div class="controls">
				<form:input path="taxRate"  cssClass="span12" placeholder="Tax Rate In %" onchange="calc_two()"/>
				<form:errors path="taxRate" cssClass="error help-inline inline" element="span" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputEmail">Price Currency</label>
			<div class="controls">
				<form:select path="priceCurrency" class="select2 span12" tabindex="" data-placeholder="Select Currency">
					<option>-- Select Currency --</option>
					<form:options items="${currencies}" itemValue="id" itemLabel="name" />
					
				</form:select>
				<form:errors path="priceCurrency" cssClass="error help-inline inline" element="span" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputEmail">Total Amount</label>
			<div class="controls">
				<input id="inc_vat" name="inc_vat"  class="span12" placeholder="Total" disabled="true" value="${amountAfterTax}"/>
			</div>
		</div>

		<div class="span12 form-actions no-margin">
			<button type="submit" class="btn btn-warning pull-right" name="save">
				<i class="icon-ok icon-white"></i>
				Save and Continue
			</button>
		</div>
		<div class="clearfix"></div>
	
</form:form>
</div>


<script type="text/javascript">


$(document).ready(function() {
	var total =$("#amountAfterTax").val();
	$("#inc_vat").val(total);
});

function calc_inc(){
    var frm = document.forms.calculator;
    var tax = (1 + frm.taxRate.value * 0.01);
    frm.inc_vat.value = (tax * frm.amountBeforeTax.value).toFixed(2);
   	$("#amountAfterTax").val(frm.inc_vat.value);
}

function calc_two(){
    var frm = document.forms.calculator;
    if (frm.amountBeforeTax.value != '') 
    	calc_inc(); 
}
</script>