<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="signup.title" /></title>
<meta name="menu" content="SellCarMenu" />

</head>




<body class="sellCar" />
<%@ include file="/common/vehicle_registration_menu.jsp"%>
<spring:bind path="vehicle.*">
	<c:if test="${not empty status.errorMessages}">
		<div class="alert alert-error fade in">
			<a href="#" data-dismiss="alert" class="close">&times;</a> Error
			encountered while trying to save registration. Please correct
			error(s) and try again!
		</div>
	</c:if>
</spring:bind>

<c:choose>
	<c:when test="${empty vehicle.vehicleId}">
		<input type="hidden" id="vehicleId" value="NEW-REG">
	</c:when>
	<c:otherwise>
		<input type="hidden" id="vehicleId" value="${vehicle.vehicleId}">
	</c:otherwise>
</c:choose>
<input type="hidden" id="sessionId" value="${sessionId}">

<div class="bs-docs-example bs-docs-example-2 reg-vehicle"
	style="margin-bottom: 20px;">

	<div class="">


	


	<div class="row-fluid ">
		<div class="form-horizontal span6">
			<div class="public-profile-2">

				<table class="table table-striped">
					<thead>
						<tr>
							<th colspan="4">Payment Summary For Vehicle Registration</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Amount Before Tax</td>
							<td>${pricing.amountBeforeTax}</td>
						</tr>
						<tr>
							<td>Tax</td>
							<td>${pricing.taxRate}</td>
						</tr>
						<tr>
							<td>Total</td>
							<td>${pricing.amountAfterTax} ${pricing.priceCurrency}</td>
						</tr>
					</tbody>
				</table>

			</div>
			<a class="btn btn-warning"
				onclick="document.forms['paypalForm'].submit();"> Pay with
				PayPal </a>
			
		</div>
		<div class="form-horizontal span6">
			
		</div>
	</div>
	</div>
	<!-- <form name="paypalForm" action="https://api-3t.sandbox.paypal.com/nvp" method="post"> -->
	<form name="paypalForm" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
	
		<%-- <input type="hidden" name="USER" value="kwabena.agyemang_api1.miagstan.com" /> <input
			type="hidden" name="PWD" value="1375540391" /> 
			<input type="hidden" name="METHOD" value="SetExpressCheckout" /> 
			<input type="hidden" name="VERSION" value="98"/>
			<input type="hidden" name="item_name" value="Vehicle Registration" /> 
			<input type="hidden" name="PAYMENTREQUEST_0_AMT" value="${pricing.amountAfterTax}" /> 
			<input type="hidden" name="PAYMENTREQUEST_0_CURRENCYCODE" value="${pricing.priceCurrency}" />
			<input type="hidden" name="PAYMENTREQUEST_0_PAYMENTACTION" value="SALE" /> 
			<input type="hidden" name="returnUrl" value="http://www.fixusystems.com/PaypalGS/paypalResponse.jsp" /> 
			<input type="hidden" name="cancelUrl" value="http://www.fixusystems.com/PaypalGS/paypalResponseCancel.jsp" /> 
			<input type="hidden" name="SIGNATURE" value="A02Q5sknggCCXsO4ZB4Fb.N-m5EGA8luFNEjfIGPsRo8QO5l0wOYUQbn" /> --%>
			<input type="hidden" name="cmd" value="_xclick" />
 <input type="hidden" name="business" value="kwabena.agyemang@miagstan.com" />
 <input type="hidden" name="password" value="1375540391" />
 <input type="hidden" name="custom" value="${vehicle.vehicleId}" />
 <input type="hidden" name="item_name" value="Vehicle Registration for Customer : ${vehicle.customer.id} - ${vehicle.customer.fullName} " />
 <input type="hidden" name="amount" value="${pricing.amountAfterTax}"/>
 <input type="hidden" name="rm" value="1" />
 <input type="hidden" name="return" value="http://www.fixusystems.com/PaypalGS/paypalResponse" />
 <input type="hidden" name="cancel_return" value="http://www.fixusystems.com/PaypalGS/paypalResponseCancel" />
 <input type="hidden" name="cert_id" value="A02Q5sknggCCXsO4ZB4Fb.N-m5EGA8luFNEjfIGPsRo8QO5l0wOYUQbn" />
	</form>
	<div class="clearfix"></div>
</div>
