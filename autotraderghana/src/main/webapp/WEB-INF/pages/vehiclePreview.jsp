<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="signup.title" /></title>
<meta name="menu" content="SellCarMenu" />

</head>




<body class="preview" />
 <jsp:include page="/common/vehicle_registration_menu.jsp">
       <jsp:param name="vehicleId" value="${vehicle.vehicleId}" />
</jsp:include>

<div class="row-fluid">
        
            <div class="span9">
                <div class="row-fluid">
                    <div class="span12 image-detail">
                    	<h4 class="title-vehicle">
                    		${vehicle.displayTitle } 
                    		<%--<c:choose>
                            	<c:when test="${vehicle.includePriceAd}">
                            		<span class="pull-right">Price : <a>${vehicle.price}&nbsp;${vehicle.priceCurrency}</a></span>
                            	</c:when>
                            	<c:otherwise>
                            		<span class="pull-right">Price : <a>Contact Seller</a></span>
                            	</c:otherwise>
                            </c:choose>--%>
                            <span class="pull-right"><a href="<c:url value="/sellCar/editVehicleForm/${vehicle.vehicleId}"/>" class="btn btn-success">Edit Vehicle Details</a></span>
                    	</h4>
                        <div class="auto image-single">
                            <img src="<c:url value="/img/4_b.jpg"/>" class="centered">
                        </div>
                    </div>
                    </div>
                    
                    <c:if test="${not empty chosen_safety || not empty chosen_interior || not empty chosen_other }">
                  <div class="row-fluid margin-top-10">
                    <div class="span12  bs-docs-example bs-docs-example-2 accessory ">
                        <!--<h4 style="padding-left: 20px;">Vehicle Accessory</h4>-->
                        
                        <c:if test="${not empty chosen_safety}">
                        	<div class="span12 no-margin">
                        	<p class="accessory-list">
                        		<span>Safety :</span>
                        		<c:forEach items="${chosen_safety}" var="safety">
                        			<span class="comma">,&nbsp;</span><span><c:out value="${safety.name}"/></span>
                        		</c:forEach>
                        		
                        	</p>
                        </div>
                        </c:if>
                        
                        <c:if test="${not empty chosen_interior}">
                        <div class="span12 no-margin">
                        	<p class="accessory-list">
                        		<span>Interior :</span>
                        		<c:forEach items="${chosen_interior}" var="interior">
                        			<span class="comma">,&nbsp;</span><span><c:out value="${interior.name}"/></span>
                        		</c:forEach>	
                        	</p>
                        </div>
                        </c:if>
                        <c:if test="${not empty chosen_other}">
                        <div class="span12 no-margin">
                        	<p class="accessory-list">
                        		<span>Other :</span>
                        		<c:forEach items="${chosen_other}" var="other">
                        			<span class="comma">,&nbsp;</span><span><c:out value="${other.name}"/></span>
                        		</c:forEach>
                        		
                        	</p>
                        </div>
                        </c:if>
                    </div>
                    
                </div>
                </c:if>
                <c:if test="${not empty vehicle.vehicleNotes }">
                <div class="row-fluid margin-top-10">
                    <div class="span12  bs-docs-example bs-docs-example-2 description">
                        <!--<h4 style="padding-left: 20px;">Description</h4>-->
                        <p>${vehicle.vehicleNotes }</p>
                    </div>
                    
                </div>
                </c:if>
            </div>
            
            <div class="span3">
            	<%-- <div class="row-fluid">
                	<div class="span12 bs-docs-example reg">
            			<div class="cat-tags"><a>${vehicle.displayTitle}</a></div>
                    </div>
                    
                </div> --%>
                <div class="row-fluid">
                	<div class="span12">
		<form class="bs-docs-example form-horizontal preview-details">
			
							<div class="cat-liked">Make : <a>${vehicle.vehicleMake.make}</a></div>
                            <div class="cat-liked">Model : <a>${vehicle.vehicleModel.model}</a></div>
                            
                            <div class="cat-liked">Year Model : <a><fmt:formatDate value="${vehicle.year}" pattern="yyyy" /></a></div>
                            <c:if test="${not empty vehicle.bodyType}">
                            	<div class="cat-liked">Body Type : <a>${vehicle.bodyType}</a></div>
                            </c:if>
                            <c:if test="${not empty vehicle.color}">
                            	<div class="cat-liked">Color : <a>${vehicle.color}</a></div>
                            </c:if>
                            <c:if test="${not empty vehicle.colorType}">
                            	<div class="cat-liked">Color Type : <a>${vehicle.colorType}</a></div>
                            </c:if>
                            <c:if test="${not empty vehicle.gearBoxType}">
                            	<div class="cat-liked">Gearbox Type : <a>${vehicle.gearBoxType}</a></div>
                            </c:if>
             
                            
                            <c:choose>
                            	<c:when test="${not empty vehicle.mileage and vehicle.includeMileageAd }">
                            		<div class="cat-liked">Mileage : <a>${vehicle.mileage}</a></div>
                            	</c:when>
                            	<c:otherwise>
                            		<div class="cat-liked">Mileage : <a>Not Specified</a></div>
                            	</c:otherwise>
                            </c:choose>
                            
                            <c:if test="${not empty vehicle.transmission}">
                            	<div class="cat-liked">Transmission : <a>${vehicle.transmission}</a></div>
                            </c:if>
                            <c:if test="${not empty vehicle.fuelType}">
                            	<div class="cat-liked">Fuel Type : <a>${vehicle.fuelType}</a></div>
                            </c:if>
                            <c:if test="${not empty vehicle.engineSize}">
                            	<div class="cat-liked">Engine Size : <a>${vehicle.engineSize}</a></div>
                            </c:if>
                            <c:if test="${not empty vehicle.doorCount}">
                            	<div class="cat-liked">Door Count : <a>${vehicle.doorCount}</a></div>
                            </c:if>
                            <div class="cat-liked">Condition : <a>${vehicle.vehicleCondition}</a></div>
                            
                            <c:choose>
                            	<c:when test="${vehicle.includePriceAd}">
                            		<div class="cat-liked">Asking Price : <a>${vehicle.price}&nbsp;${vehicle.priceCurrency}</a></div>
                            	</c:when>
                            	<c:otherwise>
                            		<div class="cat-liked">Asking Price : <a>Contact Seller</a></div>
                            	</c:otherwise>
                            </c:choose>
                            
                            <div class="cat-liked">Seller Type : <a>${vehicle.customer.userType.value}</a></div>
                            <div class="cat-liked">Last Updated: <a><fmt:formatDate value="${vehicle.updateDate}" pattern="dd-MM-yyyy" /></a></div>
			
			<div class="clearfix"></div>
		</form>
	</div>
                   
                </div>
                 <div class="row-fluid margin-top-10">
                	<div class="span12 bs-docs-example contact">
                		<div class="cat-liked">Contact Person&nbsp;:&nbsp;<a>${vehicle.customer.fullName}</a></div>
            			<div class="cat-liked">Tel&nbsp;:&nbsp;<a>${vehicle.customer.phoneNumber}</a></div>
            			<div class="cat-liked">Email&nbsp;:&nbsp;<a>${vehicle.customer.email}</a></div>
                    </div>
                    
                </div>
            </div>
            
        </div>

<div class="row-fluid margin-top-20">
	<div class="span12">
		<div class="widget">
			<div class="widget-body">
				<div class="">
					<a href="<c:url value="/sellCar/editVehicleForm/${vehicle.vehicleId}"/>" class="btn btn-info">
						<i class="icon-ok icon-white"></i>
						Edit Vehicle Registration 
					</a>

					<a  class="btn btn-warning pull-right" href="<c:url value="/sellCar/paymentConfirmForm/${vehicle.vehicleId}"/>">
						<i class="icon-ok icon-white"></i>
						Save and Continue
					</a>
				</div>
			</div>
		</div>
	</div>
</div>