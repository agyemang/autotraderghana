<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="mainMenu.title" /></title>
<meta name="menu" content="CarDealersMenu" />
<script type="text/javascript"
	src="<c:url value='/ui-resources/js/dealer.vehicle.search.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/ui-resources/bootstrap-rowlink/bootstrap-rowlink.js'/>"></script>
</head>
<body class="home">
	<div id="dealerId" style="display: none;">${dealerId}</div>
	<div class="bs-docs-example bs-docs-example-2 vehicle-result"
		style="margin-bottom: 20px;">
		

		<div id="dealer-info-holder"></div>

	</div>
	<div class="bs-docs-example bs-docs-example-2 vehicle-result"
		style="margin-bottom: 20px;">


		<div id="spinner"></div>
		<h4>
			Search Result: <span id="dealer-vehicle-count"></span> item(s) found
		</h4>


		<div class="pagination-holder"></div>
		<div class="pagination-clear"></div>
		<div id="dealer-vehicle-list-holder"></div>

		<div class="pagination-holder"></div>
		<div class="pagination-clear"></div>
		<div class="clearfix"></div>
	</div>

</body>
