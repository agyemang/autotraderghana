package com.autotraderghana.webapp.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Enumeration;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehiclePaymentInfo;
import com.autotraderghana.service.VehicleNotFoundException;
import com.autotraderghana.service.VehiclePaymentInfoService;
import com.autotraderghana.util.AppConstants;

@Controller
@RequestMapping("/PaypalGS*")
public class PayPalController extends BaseFormController {
	
	
	@Inject
	private VehiclePaymentInfoService paymentInfoService;

	@RequestMapping(value = "/paypalResponse", method = RequestMethod.POST)
	public String paypalResponse(Model model, HttpServletRequest request,
			HttpServletResponse response) throws IOException, VehicleNotFoundException {



		String transactionId = (request.getParameter("txn_id") != null ? request.getParameter("txn_id"): AppConstants.EMPTY_STRING) ;

		// read post from PayPal system and add ‘cmd‘

		@SuppressWarnings("unchecked")
		Enumeration <String> en = request.getParameterNames();
		System.err.println(en);
		String str = "cmd=_notify-validate";

		while (en.hasMoreElements()) {

			String paramName = (String) en.nextElement();
			String paramValue = request.getParameter(paramName);
			str = str + "&" + paramName + "=" + URLEncoder.encode(paramValue, AppConstants.UTF_8);

		}

		URL url = new URL("https://www.paypal.com/cgi-bin/webscr");
		URLConnection uc = url.openConnection();
		uc.setDoOutput(true);

		uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

		PrintWriter pw = new PrintWriter(uc.getOutputStream());
		pw.println(str);
		pw.close();

		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));

		String res = in.readLine();

		in.close();

		// assign posted variables to local variables

		String itemName = request.getParameter("item_name");
		String itemNumber = request.getParameter("item_number");
		String paymentStatus = request.getParameter("payment_status");
		String paymentAmount = request.getParameter("mc_gross");
		String paymentCurrency = request.getParameter("mc_currency");
		String txnId = request.getParameter("txn_id");
		String receiverEmail = request.getParameter("receiver_email");
		String payerEmail = request.getParameter("payer_email");
		String paymentDate = request.getParameter("payment_date");
		String payerId = request.getParameter("payer_id");
		String payerStatus = request.getParameter("payer_status");
		String pendingReason = request.getParameter("pending_reason");


		if (transactionId != null) {
			
			Vehicle vehicle = vehicleManager.findById(itemNumber);
			VehiclePaymentInfo info = new VehiclePaymentInfo();
			info.setPaymentAmount(paymentAmount);
			info.setPaymentDate(paymentDate);
			info.setPaymentdescription(itemName);
			info.setPaymentId(txnId);
			info.setPaymentCurrency(paymentCurrency);
			info.setPayerEmail(payerEmail);
			info.setPaymentStatus(paymentStatus);
			info.setVehicle(vehicle);
			info.setReceiverEmail(receiverEmail);
			info.setPayerId(payerId);
			info.setPayerStatus(payerStatus);
			info.setPendingReason(pendingReason);
			
			paymentInfoService.add(info);
			vehicle.setPaymentInfo(info);
			vehicleManager.update(vehicle);
			
			
			
			return ("paypalResponse");
		}

		else
			return ("redirect:paypalResponseCancel");


	}

	@RequestMapping(value = "/paypalResponseCancel", method = RequestMethod.GET)
	public String paypalResponseCancel(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		return "paypalResponseCancel";
	}


}
