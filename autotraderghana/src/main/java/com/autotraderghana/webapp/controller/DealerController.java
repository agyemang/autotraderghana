package com.autotraderghana.webapp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.mail.internet.NewsAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.ClassifierType;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.model.enums.VehicleSortPattern;
import com.autotraderghana.service.DealerService;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.service.VehicleNotFoundException;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.webapp.util.ClassifierTypeFilter;
import com.autotraderghana.webapp.util.DealerDTO;
import com.autotraderghana.webapp.util.VehicleDTO;

/**
 * Implementation of <strong>SimpleFormController</strong> that interacts with
 * the {@link UserManager} to retrieve/persist values to the database.
 *
 * <p><a href="UserFormController.java.html"><i>View Source</i></a>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Controller
@RequestMapping("/dealer*")
public class DealerController extends BaseFormController {

	protected static final String ERROR_MESSAGE_KEY_ATTACHMENT_EMPTY = "error.message.attachment.empty";

	@Inject
	protected VehicleService vehicleManager;
	
	@Inject
	protected DealerService dealerService;
	
	@Inject
	protected VehicleAttachmentService attachmentService;
	
	@Inject
	protected UserManager userManager;
	
	@Inject
	protected ClassifierManager classifierManager;

    public DealerController() {
        setCancelView("redirect:/mainMenu");
        setLoginView("redirect:/login");
        setSuccessView("redirect:/admin/users");
    }

   
    
    @RequestMapping(value = "/{dealerId}",method=RequestMethod.GET)
    public ModelAndView vehicleDetails(@ModelAttribute("searchFilter") @Valid SearchFilter searchFilter,@PathVariable("dealerId") String dealerId, Model model,HttpServletRequest request) throws VehicleNotFoundException{
    	
    	ModelAndView mv = new ModelAndView("dealerDetails");
//		User user = userManager.get(Long.valueOf(dealerId));
		mv.addObject("dealerId", dealerId);
		return mv;
	}
    

 
	
	private DealerDTO constructDealerDTO(User user) {
		DealerDTO dto = new DealerDTO();
		dto.setCustomerId(user.getId());
		dto.setCity(user.getAddress()!= null? user.getAddress().getCity():"");
		dto.setCountry(user.getAddress()!= null? user.getAddress().getCountry():"");
		dto.setEmail(user.getEmail());
		dto.setPhone1(user.getPhoneNumber());
		dto.setPhone2(user.getPhoneNumber2());
		dto.setTradingName(user.getTradingName());
		
		dto.setVehicleCount(vehicleManager.retrieveAllVehiclesByDealerCount(user.getId(), StatusEnum.LIVE));
		
		return dto;
	}



	@RequestMapping(method=RequestMethod.GET)
	@ModelAttribute
    public SearchFilter showDealers(HttpServletRequest request, HttpServletResponse response) {
		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setVehicleCondition(VehicleCondition.USED);
//		model.addAttribute("searchFilter", searchFilter);
		return searchFilter;
    }
	
	

    
    @ModelAttribute("makes")
	public List<VehicleMake> getVehicleMakes() {
		return vehicleManager.retrieveAllVehicleMake();
	}
    
	@ModelAttribute("engineSizes")
	public List<Double> getEngineSizes() {

		List<Double> engineSizeList = new ArrayList<Double>();
		double x = 0.0;
		double y = 10.0;
		double z;
		
		while (x <= y) {
			x += 0.1;
			z = (int)(x * 10) / 10.0;
			
			engineSizeList.add(z);
		}
		
	

		return engineSizeList;
	}

	@ModelAttribute("doorCount")
	public List<Integer> getDoorCount() {

		final List<Integer> doorCountList = Arrays.asList(new Integer[] { 1,2,3,4,5,6,7,8,9,10});
		
		return doorCountList;
	}

	@ModelAttribute("years")
	public List<String> getYears() {
		final List<String> yearList = new ArrayList<String>();
		
			Calendar cal = Calendar.getInstance();
			for (int i = cal.get(Calendar.YEAR) + 1; i >= cal.get(Calendar.YEAR)-110; i--) {
				String tz = Integer.toString(i);
				yearList.add(tz);
			}   

		return yearList;
	}
	
    @ModelAttribute("gearBoxTypes")
   	public List<ClassifierItem> getGearBoxTypes() {
    	return classifierManager.findItems(ClassifierType.GEARBOXTYPE);
   	}
    
    @ModelAttribute("userType")
    public UserType[] getUserTypes() {
        return   UserType.values();
    }
    
    @ModelAttribute("sortPattern")
    public VehicleSortPattern[] getVehicleSortPattern() {
        return   VehicleSortPattern.values();
    }
    
    @ModelAttribute("vehicleCondition")
    public VehicleCondition[] getVehicleConditions() {
        return    VehicleCondition.values();
    }
    
    @ModelAttribute("bodyTypes")
    public List<ClassifierItem> getBodyTypes(){
    	return classifierManager.findItems(ClassifierType.BODYTYPE);
    }
    
    @ModelAttribute("fuelTypes")
    public List<ClassifierItem> getFuelTypes(){
    	return classifierManager.findItems(ClassifierType.FUELTYPE);
    }
    
    @ModelAttribute("colors")
    public List<ClassifierItem> getColors(){
    	return classifierManager.findItems(ClassifierType.COLOR);
    }
    
    @ModelAttribute("colorTypes")
    public List<ClassifierItem> getColorTypes(){
    	return classifierManager.findItems(ClassifierType.COLORTYPE);
    }
    
    @ModelAttribute("currencies")
    public List<ClassifierItem> getCurrencies(){
    	return classifierManager.findItems(ClassifierType.CURRENCY);
    }
    
    @ModelAttribute("askingPriceInfos")
    public List<ClassifierItem> getAskingPriceInfos(){
    	return classifierManager.findItems(ClassifierType.PRICEINFO);
    }
    
    @ModelAttribute("telephoneAdditionalInfo")
    public List<ClassifierItem> getTelephoneAdditionalInfos(){
    	return classifierManager.findItems(ClassifierType.TELEADDITIONALINFO);
    }
    
    @ModelAttribute("safetyVehicleAccessories")
    public List<ClassifierItem> getSafetyVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.SAFETY));
    }
    
    @ModelAttribute("interiorVehicleAccessories")
    public List<ClassifierItem> getInteriorVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.INTERIOR_AND_COMFORT));
    }
    
    @ModelAttribute("otherVehicleAccessories")
    public List<ClassifierItem> getOtherVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.OTHER));
    } 
    
}
