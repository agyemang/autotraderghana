package com.autotraderghana.webapp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.ClassifierType;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.webapp.util.ClassifierTypeFilter;

/**
 * Implementation of <strong>SimpleFormController</strong> that interacts with
 * the {@link UserManager} to retrieve/persist values to the database.
 *
 * <p><a href="UserFormController.java.html"><i>View Source</i></a>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Controller
@RequestMapping("/mainMenu*")
public class MainMenuController extends BaseFormController {

	protected static final String ERROR_MESSAGE_KEY_ATTACHMENT_EMPTY = "error.message.attachment.empty";

	@Inject
	protected VehicleService vehicleManager;
	
	@Inject
	protected VehicleAttachmentService attachmentService;
	
	@Inject
	protected ClassifierManager classifierManager;

    public MainMenuController() {
        setCancelView("redirect:/mainMenu");
        setLoginView("redirect:/login");
        setSuccessView("redirect:/admin/users");
    }

   
    
    @RequestMapping(method=RequestMethod.GET)
    public String vehicleDetails( Model model,HttpServletRequest request){
		model.addAttribute("searchFilter", new SearchFilter());
		return "mainMenu";
	}
    
    

    private boolean isFormSubmission(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase("post");
    }

    protected boolean isAdd(HttpServletRequest request) {
        String method = request.getParameter("method");
        return (method != null && method.equalsIgnoreCase("add"));
    }
    
   
    
}
