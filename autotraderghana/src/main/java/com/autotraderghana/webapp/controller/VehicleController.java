package com.autotraderghana.webapp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.mail.internet.NewsAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.ClassifierType;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.model.enums.VehicleSortPattern;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.service.VehicleNotFoundException;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.webapp.util.ClassifierTypeFilter;
import com.autotraderghana.webapp.util.VehicleDTO;

/**
 * Implementation of <strong>SimpleFormController</strong> that interacts with
 * the {@link UserManager} to retrieve/persist values to the database.
 *
 * <p><a href="UserFormController.java.html"><i>View Source</i></a>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Controller
@RequestMapping("/vehicle*")
//@SessionAttributes/*(value="searchFilter", types=SearchFilter.class)*/
public class VehicleController extends BaseFormController {

	protected static final String ERROR_MESSAGE_KEY_ATTACHMENT_EMPTY = "error.message.attachment.empty";

	@Inject
	protected VehicleService vehicleManager;
	
	@Inject
	protected VehicleAttachmentService attachmentService;
	
	@Inject
	protected ClassifierManager classifierManager;

    public VehicleController() {
        setCancelView("redirect:/mainMenu");
        setLoginView("redirect:/login");
        setSuccessView("redirect:/admin/users");
    }

   
    
    @RequestMapping(value = "/{vehicleId}",method=RequestMethod.GET)
    public ModelAndView vehicleDetails(@ModelAttribute("searchFilter") @Valid SearchFilter searchFilter,@PathVariable("vehicleId") String vehicleId, Model model,HttpServletRequest request) throws VehicleNotFoundException{
    	
    	ModelAndView mv = new ModelAndView("/details");
		Vehicle vehicle = vehicleManager.findById(vehicleId);

		mv.addObject("vehicle", vehicle);
		return mv;
	}
    
    @RequestMapping(value = "/findVehicleForm", method = RequestMethod.POST)
    protected String findVehicleForm(@ModelAttribute("searchFilter") @Valid SearchFilter searchFilter, BindingResult result,Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) throws VehicleNotFoundException {
 
    	Vehicle vehicle = null;
    	if(!StringUtils.isBlank(searchFilter.getVehicleId()))
    		vehicle = vehicleManager.findById(searchFilter.getVehicleId());
    	
    	if(vehicle != null){
//    		model.addAttribute("searchFilter", searchFilter);
    		saveMessage(request, "Vehicle with id "+searchFilter.getVehicleId()+" found");
    		return "redirect:" + vehicle.getVehicleId() ;
    	}else {
			saveError(request, "Vehicle with id "+searchFilter.getVehicleId()+" wsa not found");
			return cancelView;
		} 
    }
    

	@RequestMapping(value = "/searchVehicleForm", method = RequestMethod.POST)
	public String showSearchResultPage(@ModelAttribute("searchFilter") SearchFilter searchFilter,BindingResult bindingResult, final RedirectAttributes redirectAttributes,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.debug("Rendering search result page for search criteria: "+ searchFilter);
		if(searchFilter.getVehicleCondition().equals(VehicleCondition.USED)){	
			return "usedcars";
		}
		else {
			return "newcars";
		}
		
	}
	
  
	
	@RequestMapping(value="/newcars", method=RequestMethod.GET)
    public String showNewVehicles(Model model,HttpServletRequest request, HttpServletResponse response) {
		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setVehicleCondition(VehicleCondition.NEW);
		model.addAttribute("searchFilter", searchFilter);
		return "newcars";
    }
	
	@RequestMapping(value="/usedcars", method=RequestMethod.GET)
    public String showUsedVehicles(Model model,HttpServletRequest request, HttpServletResponse response) {
		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setVehicleCondition(VehicleCondition.USED);
		model.addAttribute("searchFilter", searchFilter);
		return "usedcars";
    }
	
	
	
	

    private boolean isFormSubmission(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase("post");
    }

    protected boolean isAdd(HttpServletRequest request) {
        String method = request.getParameter("method");
        return (method != null && method.equalsIgnoreCase("add"));
    }
    
    @ModelAttribute("makes")
	public List<VehicleMake> getVehicleMakes() {
		return vehicleManager.retrieveAllVehicleMake();
	}
    
	@ModelAttribute("engineSizes")
	public List<Double> getEngineSizes() {

		List<Double> engineSizeList = new ArrayList<Double>();
		double x = 0.0;
		double y = 10.0;
		double z;
		
		while (x <= y) {
			x += 0.1;
			z = (int)(x * 10) / 10.0;
			
			engineSizeList.add(z);
		}
		
	

		return engineSizeList;
	}

	@ModelAttribute("doorCount")
	public List<Integer> getDoorCount() {

		final List<Integer> doorCountList = Arrays.asList(new Integer[] { 1,2,3,4,5,6,7,8,9,10});
		
		return doorCountList;
	}

	@ModelAttribute("years")
	public List<String> getYears() {
		final List<String> yearList = new ArrayList<String>();
		
			Calendar cal = Calendar.getInstance();
			for (int i = cal.get(Calendar.YEAR) + 1; i >= cal.get(Calendar.YEAR)-110; i--) {
				String tz = Integer.toString(i);
				yearList.add(tz);
			}   

		return yearList;
	}
	
    @ModelAttribute("gearBoxTypes")
   	public List<ClassifierItem> getGearBoxTypes() {
    	return classifierManager.findItems(ClassifierType.GEARBOXTYPE);
   	}
    
    @ModelAttribute("userType")
    public UserType[] getUserTypes() {
        return   UserType.values();
    }
    
    @ModelAttribute("sortPattern")
    public VehicleSortPattern[] getVehicleSortPattern() {
        return   VehicleSortPattern.values();
    }
    
    @ModelAttribute("vehicleCondition")
    public VehicleCondition[] getVehicleConditions() {
        return    VehicleCondition.values();
    }
    
    @ModelAttribute("bodyTypes")
    public List<ClassifierItem> getBodyTypes(){
    	return classifierManager.findItems(ClassifierType.BODYTYPE);
    }
    
    @ModelAttribute("fuelTypes")
    public List<ClassifierItem> getFuelTypes(){
    	return classifierManager.findItems(ClassifierType.FUELTYPE);
    }
    
    @ModelAttribute("colors")
    public List<ClassifierItem> getColors(){
    	return classifierManager.findItems(ClassifierType.COLOR);
    }
    
    @ModelAttribute("colorTypes")
    public List<ClassifierItem> getColorTypes(){
    	return classifierManager.findItems(ClassifierType.COLORTYPE);
    }
    
    @ModelAttribute("currencies")
    public List<ClassifierItem> getCurrencies(){
    	return classifierManager.findItems(ClassifierType.CURRENCY);
    }
    
    @ModelAttribute("askingPriceInfos")
    public List<ClassifierItem> getAskingPriceInfos(){
    	return classifierManager.findItems(ClassifierType.PRICEINFO);
    }
    
    @ModelAttribute("telephoneAdditionalInfo")
    public List<ClassifierItem> getTelephoneAdditionalInfos(){
    	return classifierManager.findItems(ClassifierType.TELEADDITIONALINFO);
    }
    
    @ModelAttribute("safetyVehicleAccessories")
    public List<ClassifierItem> getSafetyVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.SAFETY));
    }
    
    @ModelAttribute("interiorVehicleAccessories")
    public List<ClassifierItem> getInteriorVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.INTERIOR_AND_COMFORT));
    }
    
    @ModelAttribute("otherVehicleAccessories")
    public List<ClassifierItem> getOtherVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.OTHER));
    } 
    
}
