package com.autotraderghana.webapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.autotraderghana.customFacade.BaseFacade;
import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.dto.VehicleModelDTO;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleSortPattern;
import com.autotraderghana.service.DealerService;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.util.DealerSearchDTO;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.util.SearchFilterResponse;
import com.autotraderghana.webapp.util.AttachmentSearch;
import com.autotraderghana.webapp.util.DealerDTO;
import com.autotraderghana.webapp.util.VehicleDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@Controller
@RequestMapping("data")
public class AjaxDataController extends BaseFormController  {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(AjaxDataController.class);
	
	private static final String DELETED = "DELETED";
    private static final String NOT_DELETED = "NOT_DELETED";
    
	@Inject
	protected VehicleService vehicleManager;
	
	@Inject
	protected DealerService dealerService;
	
	@Inject
	protected BaseFacade baseFacade;
	
	@Inject
	protected UserManager userManager;
	
	@Inject VehicleAttachmentService attachmentService;

	@RequestMapping(value="/vehicle/count", method = RequestMethod.POST)
    @ResponseBody
    public Long count(/*@RequestBody */SearchFilterResponse searchFilter) {
        log.debug("Finding vehicle count for search term: " + searchFilter);
        return vehicleManager.countVehicle(searchFilter);
    }
	
	@RequestMapping(value="/dealer/vehicle/count", method = RequestMethod.POST)
    @ResponseBody
    public Long count(@RequestBody DealerSearchDTO searchFilter) {
        log.debug("Finding vehicle count for dealer: " + searchFilter.getDealerId());
        Long result = vehicleManager.retrieveAllVehiclesByDealerCount(searchFilter.getDealerId(), StatusEnum.LIVE);
        return result;
    }

	@RequestMapping(value = "/dealers", method = RequestMethod.POST)
	@ResponseBody
	public List<DealerDTO>  getDealers(@RequestBody String page) {
		page = page.replace("\"","");
		List<User> list = dealerService.retrieveAllVehiclesDealers(Integer.parseInt(page));
		return constructDealerDTOs(list);
	}
	
	@RequestMapping(value = "/dealerInfo", method = RequestMethod.POST)
	@ResponseBody
	public DealerDTO  getDealerInfo(@RequestBody Long dealerId) {
		User user = userManager.get(dealerId);
		return constructDealerDTO(user);
	}
	
	
	@RequestMapping(value="/dealer/vehicles", method = RequestMethod.POST)
	@ResponseBody
    public  List<VehicleDTO> getDealerVehicles(@RequestBody DealerSearchDTO searchFilter) {
        log.debug("Searching fro dealer: " + searchFilter.getDealerId());
        Long dealerId = searchFilter.getDealerId();
        System.err.println(dealerId);
        List<Vehicle> vehicles = vehicleManager.retrieveAllVehiclesByDealer(dealerId, searchFilter.getPageIndex());
        log.debug("Found " + vehicles.size() + " vehicles");

        return constructDTOs(vehicles);
    }
	
	@RequestMapping(value="/dealer/count", method = RequestMethod.GET)
    @ResponseBody
    public Long count() {
        log.debug("Finding dealer count: " );
        Long result = dealerService.countAllVehiclesDealers();
        return result;
    }
	/**
     * Processes search person requests.
     * @param searchCriteria    The search criteria.
     * @return
     */
    @RequestMapping(value = "/vehicle/search/page", method = RequestMethod.POST)
    public @ResponseBody List<VehicleDTO> search(/*@RequestBody */SearchFilterResponse searchFilter) {
        log.debug("Searching vehicle with search criteria: " + searchFilter);
        
        List<Vehicle> vehicles = vehicleManager.search(searchFilter);
        log.debug("Found " + vehicles.size() + " vehicles");

        return constructDTOs(vehicles);
    }

   
	
	@RequestMapping(value = "/vehicleMakes", method = RequestMethod.GET)
	public @ResponseBody List<VehicleMake> getAllVehicleMakes(HttpServletRequest request, HttpServletResponse response)throws Exception {
		List<VehicleMake> vehicleMakes = vehicleManager.retrieveAllVehicleMake();
		return vehicleMakes;
	}
	
	
	@RequestMapping(value = "/vehicleModels", method = RequestMethod.POST)
	@ResponseBody
	public List<VehicleModelDTO>  getVehicleModels(@RequestBody Long id) {
		 return covertToVehicleModelUIBeans(vehicleManager.retrieveVehicleModelsByMakeId(id));
	}
	
	@RequestMapping(value = "/vehicleModel", method = RequestMethod.POST)
	@ResponseBody
	public VehicleModelDTO  getVehicleModel(@RequestBody Long id) {
		VehicleModel model = baseFacade.load(VehicleModel.class, id);
		return (new VehicleModelDTO(model.getId(), model.getModel()));
	}
	
	@RequestMapping(value = "/yearTo", method = RequestMethod.POST)
	@ResponseBody
	public List<String>  getYearsTo(@RequestBody String yearFrom) {
		yearFrom = yearFrom.replace("\"","");
		return getYearsToList(yearFrom);
	}
	
	@RequestMapping(value = "/priceTo", method = RequestMethod.POST)
	@ResponseBody
	public List<Integer>  getPriceTo(@RequestBody String priceFrom) {
		priceFrom = priceFrom.replace("\"","");
		return getPriceToList(Integer.parseInt(priceFrom));
	}
	
	@RequestMapping(value = "/priceFrom", method = RequestMethod.GET)
	@ResponseBody
	public List<Integer>  getPriceFrom() {
		return getPriceFromList();
	}
	
	@RequestMapping(value = "/sortPattern", method = RequestMethod.GET)
	@ResponseBody
	public VehicleSortPatternContainer getVehicleSortPattern() {
		
		VehicleSortPatternContainer cont = new VehicleSortPatternContainer();
		cont.setSortPatterns(VehicleSortPattern.values());	
		return cont;

	}
	
	
//	@RequestMapping(value = "/sortPattern", method = RequestMethod.GET)
//	@ResponseBody
//	public String getVehicleSortPattern() throws JsonProcessingException {
//		
//		VehicleSortPatternContainer cont = new VehicleSortPatternContainer();
//		cont.setSortPatterns(VehicleSortPattern.values());
//		ObjectMapper objectMapper = new ObjectMapper();
//		return objectMapper.writeValueAsString(cont);
//
//	}
	
	

	@RequestMapping(value = "/mileageTo", method = RequestMethod.POST)
	@ResponseBody
	public List<Integer>  getMileageTo(@RequestBody String mileageFrom) {
		mileageFrom = mileageFrom.replace("\"","");
		return getMileageToList(Integer.parseInt(mileageFrom));
	}
	
	@RequestMapping(value = "/mileageFrom", method = RequestMethod.GET)
	@ResponseBody
	public List<Integer>  getMileageFrom() {
		return getMileageFromList();
	}
	
	@RequestMapping(value = "/engineSizeTo", method = RequestMethod.POST)
	@ResponseBody
	public List<Double>  getEngineSizeTo(@RequestBody String engineSizeFrom) {
		System.err.println(engineSizeFrom);
		engineSizeFrom = engineSizeFrom.replace("\"","");
		return getEngineSizes(engineSizeFrom);
	}
	
	private List<VehicleModelDTO> covertToVehicleModelUIBeans(List<VehicleModel> vehicleModels) {
		List<VehicleModelDTO> vehicleModelDTOs = new ArrayList<VehicleModelDTO>();
		for(VehicleModel model : vehicleModels){
			VehicleModelDTO dto = new VehicleModelDTO(model.getId(), model.getModel());
			vehicleModelDTOs.add(dto);
		}
		return vehicleModelDTOs;
	}
	
	
	@RequestMapping(value = "/vehicleattachments", method = RequestMethod.POST)
	@ResponseBody
	public List<VehicleAttachment> getImageAttachment(@RequestBody String vehicleId,HttpServletRequest request) throws Exception {

		List<VehicleAttachment> attachments = new ArrayList<VehicleAttachment>();
		vehicleId = vehicleId.replaceAll("^\"|\"$", "");

		if (vehicleId !=null) {
			System.out.println("vehicleId: "+vehicleId);
			Vehicle vehicle = vehicleManager.findById(vehicleId);
			
			if (vehicle == null) {
				HttpSession session = (HttpSession) request.getSession();
				vehicle = (Vehicle) session.getAttribute("vehicle");

				if (vehicle == null) {
					throw new RuntimeException("Vehicle not found");
				}
				
				attachments = vehicle.getImageAttachments();
			}else {
				attachments =attachmentService.findVehicleAttactments(vehicleId);
			}
		}
		return attachments;
	}
	@RequestMapping(value = "/vehicleattachments/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteImageAttachment(@RequestBody AttachmentSearch attachmentSearch,HttpServletRequest request) throws Exception {

		List<VehicleAttachment> attachments = new ArrayList<VehicleAttachment>();
		VehicleAttachment attachment = null;
		if (attachmentSearch.getVehicleId() != null && attachmentSearch.getIdToUse()!=null ) {
			Vehicle vehicle = vehicleManager.findById(attachmentSearch.getVehicleId());
			
			if (vehicle == null) {
				HttpSession session = (HttpSession) request.getSession();
				vehicle = (Vehicle) session.getAttribute("vehicle");

				if (vehicle == null) {
					throw new RuntimeException("Vehicle not found");
				}			
				attachments = vehicle.getImageAttachments();
				for(VehicleAttachment cst : attachments) {
		        	 if(cst.getTransientId().equals(attachmentSearch.getIdToUse())) {
		        		 attachment = cst;	        		 
		        		 break;
		        	 }
		         }
		         if(attachment != null){
		        	 attachments.remove(attachment);
		        	 return DELETED;
		         }
			}else {
				attachmentService.delete(attachmentSearch.getIdToUse());
				return DELETED;
			}
		}
		return NOT_DELETED;
	}
	
	
	class VehicleSortPatternContainer implements Serializable {

		private static final long serialVersionUID = 1L;
		
		private VehicleSortPattern[] sortPatterns;

		public VehicleSortPattern[] getSortPatterns() {
			return sortPatterns;
		}

		public void setSortPatterns(VehicleSortPattern[] sortPatterns) {
			this.sortPatterns = sortPatterns;
		}

	}
	private List<DealerDTO> constructDealerDTOs(List<User> list) {
		List<DealerDTO> dtos = new ArrayList<DealerDTO>();
		
		for (User user : list) {
			DealerDTO dto = new DealerDTO();
			dto.setCustomerId(user.getId());
			dto.setCity(user.getAddress()!= null? user.getAddress().getCity():"");
			dto.setCountry(user.getAddress()!= null? user.getAddress().getCountry():"");
			dto.setEmail(user.getEmail());
			dto.setPhone1(user.getPhoneNumber());
			dto.setPhone2(user.getPhoneNumber2());
			dto.setTradingName(user.getTradingName());
			
			dto.setVehicleCount(vehicleManager.retrieveAllVehiclesByDealerCount(user.getId(), StatusEnum.LIVE));
			
			dtos.add(dto);
		}
		
		return dtos;
	}
	
	
	private DealerDTO constructDealerDTO(User user) {
		DealerDTO dto = new DealerDTO();
		dto.setCustomerId(user.getId());
		dto.setCity(user.getAddress()!= null? user.getAddress().getCity():"");
		dto.setCountry(user.getAddress()!= null? user.getAddress().getCountry():"");
		dto.setEmail(user.getEmail());
		dto.setPhone1(user.getPhoneNumber());
		dto.setPhone2(user.getPhoneNumber2());
		dto.setTradingName(user.getTradingName());
		
		dto.setVehicleCount(vehicleManager.retrieveAllVehiclesByDealerCount(user.getId(), StatusEnum.LIVE));
		
		return dto;
	}
	
	 private List<VehicleDTO> constructDTOs(List<Vehicle> vehicles) {
	        List<VehicleDTO> dtos = new ArrayList<VehicleDTO>();

	        for (Vehicle vehicle: vehicles) {
	        	VehicleDTO dto = new VehicleDTO();
	        	VehicleAttachment picture = new VehicleAttachment();
	        	picture= attachmentService.findOneVehicleAttactments(vehicle.getVehicleId());
	        	
	        	dto.setVehicleId(vehicle.getVehicleId());
	            dto.setCondition(vehicle.getVehicleCondition().name());
	            dto.setMake(vehicle.getVehicleMake().getMake());
	            dto.setModel(vehicle.getVehicleModel().getModel());
	            if(null != vehicle.getMileage())
	            	dto.setMileage(vehicle.getIncludeMileageAd().equals(Boolean.TRUE) ? vehicle.getMileage().toString():"-");
	            else
	            	dto.setMileage("-");
	            dto.setPrice(vehicle.getIncludePriceAd().equals(Boolean.TRUE) ? vehicle.getPrice().toString():"");
	            dto.setCurrency(vehicle.getIncludePriceAd().equals(Boolean.TRUE) ? vehicle.getPriceCurrency().getName():"");
	            dto.setSellerType(vehicle.getCustomer().getUserType().getValue());
	            dto.setPicture(picture.getPicture()); 
	            
	            dto.setBodyType(vehicle.getBodyType() !=null  ? vehicle.getBodyType().getName():"");
	            dto.setEngineSize(vehicle.getEngineSize() !=null ? String.valueOf(vehicle.getEngineSize()) + "L" : "" );
	            dto.setFueltype(vehicle.getFuelType() != null ? vehicle.getFuelType().getName():"");
	            dto.setGearBoxType(vehicle.getGearBoxType() != null ? vehicle.getGearBoxType().getName() : "");
	            
	            Calendar cal = Calendar.getInstance();
	            cal.setTime(vehicle.getYear());
	            int year = cal.get(Calendar.YEAR);
	  
	           
	            dto.setYear(String.valueOf(year));
	            //TODO - Check advertising address
	            dto.setCity(vehicle.getCustomer().getAddress().getCity());
	            dto.setCustomer(vehicle.getCustomer().getUserType().equals(UserType.PRIVATE_SELLER)? vehicle.getCustomer().getFullName():"Some Trading Company");
	           
	            if (vehicle.getCreateDate().equals(vehicle.getUpdateDate()) && getUpdateTimeDifference(vehicle.getUpdateDate(),new Date()) <= 72) {
					if (getUpdateTimeDifference(vehicle.getUpdateDate(),new Date()) <= 24) {
						
						dto.setTimeDifference("NEW 24H");
					} else {
						dto.setTimeDifference("NEW 72H");
					}
				} else if (!vehicle.getCreateDate().equals(vehicle.getUpdateDate()) && getUpdateTimeDifference(vehicle.getCreateDate(),vehicle.getUpdateDate()) <= 72) {
					if (getUpdateTimeDifference(vehicle.getCreateDate(),vehicle.getUpdateDate()) <= 24) {
						 System.err.println(vehicle.getDisplayTitle());
						dto.setTimeDifference("UPDATED 24H");
					} else {
						dto.setTimeDifference("UPDATED 72H");
					}
				} else {
					dto.setTimeDifference("");
				}
			    
//	            System.err.println(vehicle.getDisplayTitle() + "is :"+dto.getTimeDifference());
	            
	            dtos.add(dto);
	            
    
	            
	        }

	        return dtos;
	    }
	 
	 

	private Long getUpdateTimeDifference(Date start, Date end){		 
//		 Period p = new Period(new DateTime(start), new DateTime(end));
//		 long hours = p.getHours();
////		 long minutes = p.getMinutes();
//		 return hours;
		long diff = (end.getTime() - start.getTime())/(60*60 * 1000);
		return diff;
	 }
	 
	public List<Double> getEngineSizes(String engineSizeFrom) {

		List<Double> engineSizeList = new ArrayList<Double>();
		double x = Double.parseDouble(engineSizeFrom);  
		double y = 10.0;
		double z;
		
		while (x <= y) {
			
			z = (int)(x * 10) / 10.0;
			
			engineSizeList.add(z);
			x += 0.1;
		}
		
	

		return engineSizeList;
	}

	public List<Integer> getPriceFromList(){
		List<Integer> priceList = new ArrayList<Integer>();
		
		int amount = 0;

		while (amount <= 150000) {
			amount += 500;			
			priceList.add(amount);
		}

		return priceList;
	}
	
	public List<Integer> getPriceToList(Integer priceFrom){
		List<Integer> priceList = new ArrayList<Integer>();
		
		int amount = priceFrom;

		while (amount <= 150000) {
			
			if(amount == 1)	{
				priceList.add(amount);
				amount += 499;
			}else {
				priceList.add(amount);
				amount += 500;
			}
				
		}

		return priceList;
	}
	
	public List<String> getYearsToList(String yearFrom) {
		final List<String> yearList = new ArrayList<String>();
		
			Calendar cal = Calendar.getInstance();
			for (int i = Integer.parseInt(yearFrom); i >= cal.get(Calendar.YEAR)-110; i--) {
				String tz = Integer.toString(i);
				yearList.add(tz);
			}   

		return yearList;
	}
	
	public List<Integer> getMileageFromList(){
		List<Integer> mileageList = new ArrayList<Integer>();
		
		int start = 0;

		while (start <= 600000) {
			start += 5000;			
			mileageList.add(start);
		}

		return mileageList;
	}
	
	public List<Integer> getMileageToList(Integer mileageFrom){
		List<Integer> mileageList = new ArrayList<Integer>();
		
		int start = mileageFrom;

		while (start <= 600000) {
					
			mileageList.add(start);
			start += 5000;	
		}

		return mileageList;
		
	}
}
