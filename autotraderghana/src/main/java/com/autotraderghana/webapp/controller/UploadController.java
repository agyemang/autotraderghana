/*
 * SpringSourcery - http://www.springsourcery.org
 *
 * Copyright (C) 2011 Dan Macklin.
 *
 * SpringSourcery is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * SpringSourcery is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SpringSourcery; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

package com.autotraderghana.webapp.controller;

import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.service.VehicleService;

@Controller
@RequestMapping("/uploadAttachments")
public class UploadController {

	@Inject
	protected VehicleAttachmentService attachmentService;

	@Inject
	protected VehicleService vehicleManager;

	private static Log LOGGER = LogFactory.getLog(UploadController.class);

	public UploadController() {
	}

	@RequestMapping(value = "/processUpload", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody String processUpload(HttpServletRequest req, HttpServletResponse response) throws Exception {

		String vehicleId = req.getParameter("vehicleId");

		LOGGER.debug("Uploading File : Request Received");
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) req;
		CommonsMultipartFile file = (CommonsMultipartFile) multipartRequest.getFile("filedata");

		if (file != null && vehicleId != null) {

			Vehicle vehicle = vehicleManager.findById(vehicleId);

			if (vehicle == null) {
				HttpSession session = (HttpSession) req.getSession();
				vehicle = (Vehicle) session.getAttribute("vehicle");

				if (vehicle == null) {
					throw new RuntimeException("Vehicle not found");
				}
			}

			VehicleAttachment attachment = new VehicleAttachment();
			attachment.setMimeType("image/jpeg");
			attachment.setPicture(file.getBytes());

			if (vehicle.getVehicleId() != null) {
				attachment.setVehicleId(vehicleId);
				attachmentService.save(attachment);
			} else {
				attachment.setVehicleId(vehicleId);
				attachment.setTransientId(UUID.randomUUID().toString());
				attachmentService.save(attachment, vehicle);
			}
			return "OK";
		} else {
			return "NOTOK";
		}

	}

}
