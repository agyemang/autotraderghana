package com.autotraderghana.webapp.controller;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.autotraderghana.util.SearchFilter;

@Controller
@RequestMapping("/Login")
public class LoginController {

    @RequestMapping(method = RequestMethod.GET)
	public String login(Locale locale, Model model) {
    	model.addAttribute("searchFilter", new SearchFilter());
		return "login";
 
	}
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String loginWithError(Locale locale, Model model) {
    	model.addAttribute("searchFilter", new SearchFilter());
        model.addAttribute("error", true);
        return "login";
    }
}
