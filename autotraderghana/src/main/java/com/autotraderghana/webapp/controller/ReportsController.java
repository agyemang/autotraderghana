package com.autotraderghana.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.autotraderghana.Constants;
import com.autotraderghana.dao.SearchException;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehicleService;

@Controller
@RequestMapping("/admin/report*")
public class ReportsController {

	private UserManager userManager = null;

	@Autowired
	VehicleService vehicleManager;

	@Autowired
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	@RequestMapping(value = "/user/enabled", method = RequestMethod.GET)
	public ModelAndView handleRequestUserEnabled() throws Exception {
		Model model = new ExtendedModelMap();
		model.addAttribute(userManager.getEnableUsers());
		return new ModelAndView("admin/userList", model.asMap());
	}

	@RequestMapping(value = "/user/accountExpired", method = RequestMethod.GET)
	public ModelAndView handleRequestUserAccountExpired() throws Exception {
		Model model = new ExtendedModelMap();
		model.addAttribute(userManager.getExpiredUsers());
		return new ModelAndView("admin/userList", model.asMap());
	}

	@RequestMapping(value = "/user/accountLocked", method = RequestMethod.GET)
	public ModelAndView handleRequestUserAccountLocked() throws Exception {
		Model model = new ExtendedModelMap();
		model.addAttribute(userManager.getlockedUsers());
		return new ModelAndView("admin/userList", model.asMap());
	}

	@RequestMapping(value = "/user/credentialsExpired", method = RequestMethod.GET)
	public ModelAndView handleRequestUserCredentialsExpired() throws Exception {
		Model model = new ExtendedModelMap();
		model.addAttribute(userManager.getCredentialsExpiredUsers());
		return new ModelAndView("admin/userList", model.asMap());
	}
	
	@RequestMapping(value = "/user/cardealers", method = RequestMethod.GET)
	public ModelAndView handleRequestUserDealers() throws Exception {
		Model model = new ExtendedModelMap();
		return new ModelAndView("admin/dealers", model.asMap());
	}
	
	
	

}
