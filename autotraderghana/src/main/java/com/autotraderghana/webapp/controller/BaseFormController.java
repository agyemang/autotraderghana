package com.autotraderghana.webapp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.autotraderghana.Constants;
import com.autotraderghana.customFacade.BaseFacade;
import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.ClassifierType;
import com.autotraderghana.model.User;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.service.MailEngine;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.webapp.util.ClassifierTypeFilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import java.beans.PropertyEditorSupport;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Implementation of <strong>SimpleFormController</strong> that contains
 * convenience methods for subclasses.  For example, getting the current
 * user and saving messages/errors. This class is intended to
 * be a base class for all Form controllers.
 *
 * <p><a href="BaseFormController.java.html"><i>View Source</i></a></p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public class BaseFormController implements ServletContextAware {
	
	
	
    protected final transient Log log = LogFactory.getLog(getClass());
    
    
    private static final String FLASH_ERROR_MESSAGE = "errorMessage";
    private static final String FLASH_SUCCESS_MESSAGE = "successMessage";
    private static final String FLASH_FEEDBACK_MESSAGE = "feedbackMessage";
    private static final String FLASH_WARNING_MESSAGE = "feedbackMessage";
    
    public static final String MESSAGES_KEY = "successMessages";
    private UserManager userManager = null;
    protected MailEngine mailEngine = null;
    protected SimpleMailMessage message = null;
    protected String templateName = "accountCreated.vm";
    protected String cancelView;
    protected String successView;
    
    @Resource
    private MessageSource messageSource;
    
    @Inject
	protected ClassifierManager classifierManager;
    
    @Inject
    protected BaseFacade baseFacade;
    
    @Inject
	protected VehicleService vehicleManager;
    
    protected String loginView;

    private MessageSourceAccessor messages;
    private ServletContext servletContext;

//    @Autowired(required = false)
    Validator validator;

    @Autowired
    public void setMessages(MessageSource messageSource) {
        messages = new MessageSourceAccessor(messageSource);
    }

    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public UserManager getUserManager() {
        return this.userManager;
    }

    @SuppressWarnings("unchecked")
    public void saveError(HttpServletRequest request, String error) {
        List errors = (List) request.getSession().getAttribute("errors");
        if (errors == null) {
            errors = new ArrayList();
        }
        errors.add(error);
        request.getSession().setAttribute("errors", errors);
    }
    
    @SuppressWarnings("unchecked")
    public void saveMessage(HttpServletRequest request, String msg) {
        List messages = (List) request.getSession().getAttribute(MESSAGES_KEY);

        if (messages == null) {
            messages = new ArrayList();
        }

        messages.add(msg);
        request.getSession().setAttribute(MESSAGES_KEY, messages);
    }

    /**
     * Convenience method for getting a i18n key's value.  Calling
     * getMessageSourceAccessor() is used because the RequestContext variable
     * is not set in unit tests b/c there's no DispatchServlet Request.
     *
     * @param msgKey
     * @param locale the current locale
     * @return
     */
    public String getText(String msgKey, Locale locale) {
        return messages.getMessage(msgKey, locale);
    }

    /**
     * Convenient method for getting a i18n key's value with a single
     * string argument.
     *
     * @param msgKey
     * @param arg
     * @param locale the current locale
     * @return
     */
    public String getText(String msgKey, String arg, Locale locale) {
        return getText(msgKey, new Object[] { arg }, locale);
    }

    /**
     * Convenience method for getting a i18n key's value with arguments.
     *
     * @param msgKey
     * @param args
     * @param locale the current locale
     * @return
     */
    public String getText(String msgKey, Object[] args, Locale locale) {
        return messages.getMessage(msgKey, args, locale);
    }

    /**
     * Convenience method to get the Configuration HashMap
     * from the servlet context.
     *
     * @return the user's populated form from the session
     */
    public Map getConfiguration() {
        Map config = (HashMap) servletContext.getAttribute(Constants.CONFIG);

        // so unit tests don't puke when nothing's been set
        if (config == null) {
            return new HashMap();
        }

        return config;
    }

    /**
     * Set up a custom property editor for converting form inputs to real objects
     * @param request the current request
     * @param binder the data binder
     */
    @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Integer.class, null, new CustomNumberEditor(Integer.class, null, true));
        binder.registerCustomEditor(Long.class, null, new CustomNumberEditor(Long.class, null, true));
        binder.registerCustomEditor(Double.class, null, new CustomNumberEditor(Double.class, null, true));
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        
//        SimpleDateFormat dateFormat =  new SimpleDateFormat(getText("date.format", request.getLocale()));
//        dateFormat.setLenient(false);
//        binder.registerCustomEditor(Date.class, null, new CustomDateEditor(dateFormat, true));
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        dateFormat.setLenient(false);


        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        

		binder.registerCustomEditor(ClassifierItem.class, new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                if (text == null || text.trim().length() == 0)
                    setValue(null);
                try {
                	ClassifierItem classifierItem = classifierManager.findById(Long.parseLong(text));
                    setValue(classifierItem);
                } catch (Exception e) {
                    setValue(null);
                }
            }
        });
		binder.registerCustomEditor(VehicleMake.class, new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                if (text == null || text.trim().length() == 0)
                    setValue(null);
                try {
                	VehicleMake vehicleMake = baseFacade.load(VehicleMake.class, (Long.parseLong(text)));
                    setValue(vehicleMake);
                } catch (Exception e) {
                    setValue(null);
                }
            }
        });
		
		binder.registerCustomEditor(VehicleModel.class, new PropertyEditorSupport() {

            @Override
            public void setAsText(String text) {
                if (text == null || text.trim().length() == 0)
                    setValue(null);
                try {
                	VehicleModel vehicleModel = baseFacade.load(VehicleModel.class, (Long.parseLong(text)));
                    setValue(vehicleModel);
                } catch (Exception e) {
                    setValue(null);
                }
            }
        });
    }

    /**
     * Convenience message to send messages to users, includes app URL as footer.
     * @param user the user to send a message to.
     * @param msg the message to send.
     * @param url the URL of the application.
     */
    protected void sendUserMessage(User user, String msg, String url) {
        if (log.isDebugEnabled()) {
        	log.debug("sending e-mail to user [" + user.getEmail() + "]...");
        }

        message.setTo(user.getFullName() + "<" + user.getEmail() + ">");

        Map<String, Serializable> model = new HashMap<String, Serializable>();
        model.put("user", user);

        // TODO: once you figure out how to get the global resource bundle in
        // WebWork, then figure it out here too.  In the meantime, the Username
        // and Password labels are hard-coded into the template. 
        // model.put("bundle", getTexts());
        model.put("message", msg);
        model.put("applicationURL", url);
        mailEngine.sendMessage(message, templateName, model);
    }

    @Autowired
    public void setMailEngine(MailEngine mailEngine) {
        this.mailEngine = mailEngine;
    }

    @Autowired
    public void setMessage(SimpleMailMessage message) {
        this.message = message;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
   
    public final BaseFormController setCancelView(String cancelView) {
        this.cancelView = cancelView;
        return this;
    }
    
    public final BaseFormController setLoginView(String loginView) {
        this.loginView = loginView;
        return this;
    }
    

    public final String getLoginView() {
		return loginView;
	}

	public final String getCancelView() {
        // Default to successView if cancelView is invalid
        if (this.cancelView == null || this.cancelView.length()==0) {
            return getSuccessView();
        }
        return this.cancelView;   
    }

    public final String getSuccessView() {
        return this.successView;
    }
    
    public final BaseFormController setSuccessView(String successView) {
        this.successView = successView;
        return this;
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    protected ServletContext getServletContext() {
        return servletContext;
    }
    
    /**
     * Adds a new error message
     * @param model A model which stores the the error message.
     * @param code  A message code which is used to fetch the correct message from the message source.
     * @param params    The parameters attached to the actual error message.
     */
    protected void addErrorMessage(RedirectAttributes model, String code, Object... params) {
        log.debug("adding error message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedErrorMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedErrorMessage);
        model.addFlashAttribute(FLASH_ERROR_MESSAGE, localizedErrorMessage);
    }

    protected void addErrorMessage(ModelAndView model, String code, Object... params) {
        log.debug("adding error message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedErrorMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedErrorMessage);
        model.addObject(FLASH_ERROR_MESSAGE, localizedErrorMessage);
    }

    /**
     * Adds a new feedback message.
     * @param model A model which stores the feedback message.
     * @param code  A message code which is used to fetch the actual message from the message source.
     * @param params    The parameters which are attached to the actual feedback message.
     */
    protected void addFeedbackMessage(ModelAndView model, String code, Object... params) {
        log.debug("Adding feedback message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedFeedbackMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedFeedbackMessage);
        model.addObject(FLASH_FEEDBACK_MESSAGE, localizedFeedbackMessage);
    }
    protected void addFeedbackMessage(RedirectAttributes model, String code, Object... params) {
        log.debug("Adding feedback message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedFeedbackMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedFeedbackMessage);
        model.addFlashAttribute(FLASH_FEEDBACK_MESSAGE, localizedFeedbackMessage);
    }

    /**
     * Adds a new success message.
     * @param model A model which stores the success message.
     * @param code  A message code which is used to fetch the actual message from the message source.
     * @param params    The parameters which are attached to the actual success message.
     */
    protected void addSuccessMessage(ModelAndView model, String code, Object... params) {
        log.debug("Adding success message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedSuccessMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedSuccessMessage);
        model.addObject(FLASH_SUCCESS_MESSAGE, localizedSuccessMessage);
    }
    protected void addSuccessMessage(RedirectAttributes model, String code, Object... params) {
        log.debug("Adding success message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedSuccessMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedSuccessMessage);
        model.addFlashAttribute(FLASH_SUCCESS_MESSAGE, localizedSuccessMessage);
    }


    /**
     * Adds a new warning message.
     * @param model A model which stores the warning message.
     * @param code  A message code which is used to fetch the actual message from the message source.
     * @param params    The parameters which are attached to the actual warning message.
     */
    protected void addWarningMessage(ModelAndView model, String code, Object... params) {
        log.debug("Adding warning message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedWarningMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedWarningMessage);
        model.addObject(FLASH_WARNING_MESSAGE, localizedWarningMessage);
    }
    protected void addWarningMessage(RedirectAttributes model, String code, Object... params) {
        log.debug("Adding warning message with code: " + code + " and params: " + params);
        Locale current = LocaleContextHolder.getLocale();
        log.debug("Current locale is " + current);
        String localizedWarningMessage = messageSource.getMessage(code, params, current);
        log.debug("Localized message is: " + localizedWarningMessage);
        model.addFlashAttribute(FLASH_WARNING_MESSAGE, localizedWarningMessage);
    }
    
    @ModelAttribute("priceFroms")
    public List<Integer> getPriceFromList(){
		List<Integer> priceList = new ArrayList<Integer>();
		
		int amount = 1;

		while (amount <= 150000) {
					
			if(amount == 1)	{
				priceList.add(amount);
				amount += 499;
			}else {
				priceList.add(amount);
				amount += 500;
			}	
		}

		return priceList;
	}
    
    @ModelAttribute("mileageFroms")
    public List<Integer> getMileageFromList(){
		List<Integer> mileageList = new ArrayList<Integer>();
		
		int start = 0;

		while (start <= 600000) {
					
			mileageList.add(start);
			start += 5000;	
		}

		return mileageList;
	}
    
    @ModelAttribute("makes")
	public List<VehicleMake> getVehicleMakes() {
		return vehicleManager.retrieveAllVehicleMake();
	}
    
	@ModelAttribute("engineSizes")
	public List<Double> getEngineSizes() {

		List<Double> engineSizeList = new ArrayList<Double>();
		double x = 0.0;
		double y = 10.0;
		double z;
		
		while (x <= y) {
			x += 0.1;
			z = (int)(x * 10) / 10.0;
			
			engineSizeList.add(z);
		}
		
	

		return engineSizeList;
	}

	@ModelAttribute("doorCount")
	public List<Integer> getDoorCount() {

		final List<Integer> doorCountList = Arrays.asList(new Integer[] { 1,2,3,4,5,6,7,8,9,10});
		
		return doorCountList;
	}

	@ModelAttribute("years")
	public List<String> getYears() {
		final List<String> yearList = new ArrayList<String>();
		
			Calendar cal = Calendar.getInstance();
			for (int i = cal.get(Calendar.YEAR) + 1; i >= cal.get(Calendar.YEAR)-110; i--) {
				String tz = Integer.toString(i);
				yearList.add(tz);
			}   

		return yearList;
	}
	
    @ModelAttribute("gearBoxTypes")
   	public List<ClassifierItem> getGearBoxTypes() {
    	return classifierManager.findItems(ClassifierType.GEARBOXTYPE);
   	}
    
    @ModelAttribute("userType")
    public UserType[] getUserTypes() {
        return   UserType.values();
    }
    
    @ModelAttribute("vehicleCondition")
    public VehicleCondition[] getVehicleConditions() {
        return    VehicleCondition.values();
    }
    
    @ModelAttribute("bodyTypes")
    public List<ClassifierItem> getBodyTypes(){
    	return classifierManager.findItems(ClassifierType.BODYTYPE);
    }
    
    @ModelAttribute("fuelTypes")
    public List<ClassifierItem> getFuelTypes(){
    	return classifierManager.findItems(ClassifierType.FUELTYPE);
    }
    
    @ModelAttribute("colors")
    public List<ClassifierItem> getColors(){
    	return classifierManager.findItems(ClassifierType.COLOR);
    }
    
    @ModelAttribute("colorTypes")
    public List<ClassifierItem> getColorTypes(){
    	return classifierManager.findItems(ClassifierType.COLORTYPE);
    }
    
    @ModelAttribute("currencies")
    public List<ClassifierItem> getCurrencies(){
    	return classifierManager.findItems(ClassifierType.CURRENCY);
    }
    
    @ModelAttribute("askingPriceInfos")
    public List<ClassifierItem> getAskingPriceInfos(){
    	return classifierManager.findItems(ClassifierType.PRICEINFO);
    }
    
    @ModelAttribute("telephoneAdditionalInfo")
    public List<ClassifierItem> getTelephoneAdditionalInfos(){
    	return classifierManager.findItems(ClassifierType.TELEADDITIONALINFO);
    }
    
    @ModelAttribute("safetyVehicleAccessories")
    public List<ClassifierItem> getSafetyVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.SAFETY));
    }
    
    @ModelAttribute("interiorVehicleAccessories")
    public List<ClassifierItem> getInteriorVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.INTERIOR_AND_COMFORT));
    }
    
    @ModelAttribute("otherVehicleAccessories")
    public List<ClassifierItem> getOtherVehicleAccessories(){
    	return classifierManager.findItems(ClassifierType.VEHICLEACCESSORY, classifierManager.findItemByName(ClassifierType.VEHICLEACCESSORY, ClassifierTypeFilter.OTHER));
    } 
    @ModelAttribute("searchFilter")
    public SearchFilter getSearchFilter(){
    	return new SearchFilter();
    } 
}
