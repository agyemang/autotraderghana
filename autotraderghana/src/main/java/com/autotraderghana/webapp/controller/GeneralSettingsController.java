package com.autotraderghana.webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.autotraderghana.Constants;
import com.autotraderghana.dao.SearchException;
import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehiclePostPrice;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehiclePricePostService;
import com.autotraderghana.service.VehicleService;

@Controller
@RequestMapping("/admin/setting*")
public class GeneralSettingsController extends BaseFormController{

	private UserManager userManager = null;

	@Autowired
	VehicleService vehicleManager;
	
	@Autowired
	private VehiclePricePostService postPriceService;

	@Autowired
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	@RequestMapping(value = "/pricing/dealer", method = RequestMethod.GET)
	protected String loadFormDealerPrice(Model model, HttpServletRequest request)
			throws Exception {
		
		VehiclePostPrice postPrice = null;
		postPrice = postPriceService.findVehiclePostPriceDealer();
		if (postPrice !=null)
			model.addAttribute("postPrice", postPrice);
		else {
			postPrice = new VehiclePostPrice();
			postPrice.setUserType(UserType.CAR_DEALER);
			model.addAttribute("postPrice", postPrice);
		}

		return "/admin/dealerPrice";
	}

	@RequestMapping(value = "/pricing/private", method = RequestMethod.GET)
	protected String loadFormPrivatePrice(Model model,
			HttpServletRequest request) throws Exception {
		VehiclePostPrice postPrice = null;
		postPrice = postPriceService.findVehiclePostPricePrivate();
		if (postPrice !=null)
			model.addAttribute("postPrice", postPrice);
		else {
			postPrice = new VehiclePostPrice();
			postPrice.setUserType(UserType.PRIVATE_SELLER);
			model.addAttribute("postPrice", postPrice);
		}

		return "/admin/privatePrice";
	}
	
	@RequestMapping(value = "/pricing/dealer", method = RequestMethod.POST)
    public String validateDealerForm(@ModelAttribute("postPrice") @Valid VehiclePostPrice postPrice, BindingResult result,Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
 
        if (result.hasErrors()) {
        	return "/admin/dealerPrice";     	
        }
        postPriceService.save(postPrice);
        saveMessage(request, "Pricing updated successfully.");
        
    	return "/admin/dealerPrice";
    	
    }
	
	@RequestMapping(value = "/pricing/private", method = RequestMethod.POST)
    public String validatePrivateForm(@ModelAttribute("postPrice") @Valid VehiclePostPrice postPrice, BindingResult result,Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
 
		if (result.hasErrors()) {
        	return "/admin/privatePrice";     	
        }
        postPriceService.save(postPrice);
        saveMessage(request, "Pricing updated successfully.");
    		return "/admin/privatePrice";			
		
    }

}
