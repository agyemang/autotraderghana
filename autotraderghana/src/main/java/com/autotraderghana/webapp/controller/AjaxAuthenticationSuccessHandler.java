package com.autotraderghana.webapp.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

public class AjaxAuthenticationSuccessHandler extends
		SavedRequestAwareAuthenticationSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(AjaxAuthenticationSuccessHandler.class);
	private ObjectMapper mapper = new ObjectMapper();

	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		String requestUrl;

		if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
			response.setContentType("application/json");
			LoginStatus status = new LoginStatus(true, authentication.getName());
			response.getWriter().print(mapper.writeValueAsString(status));

			response.getWriter().flush();
		} else {
			DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest) request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST_KEY");
			if (defaultSavedRequest != null) {
				requestUrl = defaultSavedRequest.getRequestURL();
				getRedirectStrategy().sendRedirect(request, response,requestUrl);

			} else {
				if (logger.isInfoEnabled()) {
					getRedirectStrategy().sendRedirect(request, response,request.getRequestURL().toString());
				} else {
					super.onAuthenticationSuccess(request, response,authentication);
				}

			}
		}

	}

	@Override
	protected String determineTargetUrl(HttpServletRequest request,HttpServletResponse response) {

		String url = request.getParameter("redirect_after_login");
		if (url != null) {
			return url;
		} else
			return super.determineTargetUrl(request, response);
	}

}
