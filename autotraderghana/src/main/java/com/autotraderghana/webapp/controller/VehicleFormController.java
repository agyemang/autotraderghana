package com.autotraderghana.webapp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.ClassifierType;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehiclePostPrice;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.service.UserManager;
import com.autotraderghana.service.VehiclePricePostService;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.service.VehicleNotFoundException;
import com.autotraderghana.webapp.util.ClassifierTypeFilter;

/**
 * Implementation of <strong>SimpleFormController</strong> that interacts with
 * the {@link UserManager} to retrieve/persist values to the database.
 *
 * <p><a href="UserFormController.java.html"><i>View Source</i></a>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Controller
@RequestMapping("/sellCar*")
@SessionAttributes(value="vehicle", types=Vehicle.class)
public class VehicleFormController extends BaseFormController {

	protected static final String ERROR_MESSAGE_KEY_ATTACHMENT_EMPTY = "error.message.attachment.empty";

	@Inject
	protected VehicleService vehicleManager;
	
	@Inject
	protected VehiclePricePostService pricePostService;
	
	@Inject
	protected VehicleAttachmentService attachmentService;
	
	@Inject
	protected ClassifierManager classifierManager;

    public VehicleFormController() {
        setCancelView("redirect:/mainMenu");
        setLoginView("redirect:/login");
        setSuccessView("redirect:/admin/users");
    }

    
    @RequestMapping(value = "/vehicleForm",method=RequestMethod.GET)
    protected String loadFormPage(Model model,HttpServletRequest request) throws Exception {
    	User cleanUser = getUserManager().getUserByUsername(request.getRemoteUser());
    	Vehicle vehicle = new Vehicle();
    	vehicle.setCustomer(cleanUser);
		model.addAttribute("vehicle", vehicle);
		model.addAttribute("sessionId", RequestContextHolder.currentRequestAttributes().getSessionId());
		return "vehicleForm";
	}
    
    
    
    @RequestMapping(value = "/vehicleForm", method = RequestMethod.POST)
    public String validateForm(@ModelAttribute("vehicle") @Valid Vehicle vehicle, BindingResult result,Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
 
        if (result.hasErrors()) {
            return "vehicleForm";
        }
        
        if (vehicle.getImageAttachments().isEmpty()){
        	saveError(request, "Vehicle pictures not uploaded. Please upload atleast one photo.");
            return "vehicleForm";
        }
        
        List<VehicleAttachment> attachments = vehicle.getImageAttachments();
        vehicle.setStatus(StatusEnum.PENDING);
        vehicle = vehicleManager.add(vehicle);
        
        for (VehicleAttachment attachment : attachments){
        	attachment.setVehicleId(vehicle.getVehicleId());
        	attachmentService.save(attachment);
        }
        
        
        return "redirect:/sellCar/previewVehicleForm/" + vehicle.getVehicleId() ;
 
    }
    
    @RequestMapping(value = "/previewVehicleForm/{vehicleId}",method = RequestMethod.GET)
    public ModelAndView previewVehicleForm(@PathVariable("vehicleId") String vehicleId,HttpServletRequest request,
       	 Model model) throws VehicleNotFoundException{
    	ModelAndView mv = new ModelAndView("/vehiclePreview");
		Vehicle vehicle = vehicleManager.findById(vehicleId);

		mv.addObject("vehicle", vehicle);
		mv.addObject("chosen_safety", vehicle.getChosen_safety());
		mv.addObject("chosen_interior", vehicle.getChosen_interior());
		mv.addObject("chosen_other", vehicle.getChosen_other());
		return mv;
       	
    }
    
    @RequestMapping(value = "/paymentConfirmForm/{vehicleId}",method = RequestMethod.GET)
    public ModelAndView paymentConfirmForm(@PathVariable("vehicleId") String vehicleId,HttpServletRequest request,
       	Model model) throws VehicleNotFoundException{
    	ModelAndView mv = new ModelAndView("/paymentConfirmForm");
		Vehicle vehicle = vehicleManager.findById(vehicleId);
		
		if (vehicle.getCustomer().getUserType().equals(UserType.CAR_DEALER)) {
			mv.addObject("pricing", pricePostService.findVehiclePostPriceDealer());
		}else {
			mv.addObject("pricing", pricePostService.findVehiclePostPricePrivate());
		}
		
		mv.addObject("vehicle", vehicle);
		return mv;
       	
    }
    
    @RequestMapping(value = "/paymentConfirmForm/{vehicleId}", method = RequestMethod.POST)
    protected String paymentConfirmFormOnSubmit(@ModelAttribute("vehicle") @Valid Vehicle vehicle, BindingResult result,Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) throws VehicleNotFoundException {
 
        if (result.hasErrors()) {
            return "editVehicleForm";
        }  
        List<VehicleAttachment> attachments = attachmentService.findVehicleAttactments(vehicle.getVehicleId());
        if (attachments.isEmpty()){
        	saveError(request, "Vehicle pictures not uploaded. Please upload atleast one photo.");
            return "editVehicleForm";
        }
        vehicle = vehicleManager.update(vehicle);
        saveMessage(request, "Vehicle sucessfully updated.");
        return "redirect:/sellCar/previewVehicleForm/" + vehicle.getVehicleId() ;
 
    }
    
    
    @RequestMapping(value = "/editVehicleForm/{vehicleId}",method=RequestMethod.GET)
    public String editVehicleForm(@PathVariable("vehicleId") String vehicleId, Model model,HttpServletRequest request) throws VehicleNotFoundException{
    	
    	Vehicle vehicle = vehicleManager.findById(vehicleId);
		model.addAttribute("vehicle", vehicle);
		model.addAttribute("sessionId", RequestContextHolder.currentRequestAttributes().getSessionId());
		return "editVehicleForm";
	}
    
    
    @RequestMapping(value = "/editVehicleForm/{vehicleId}", method = RequestMethod.POST)
    protected String editFormOnSubmit(@ModelAttribute("vehicle") @Valid Vehicle vehicle, BindingResult result,Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) throws VehicleNotFoundException {
 
        if (result.hasErrors()) {
            return "editVehicleForm";
        }  
        List<VehicleAttachment> attachments = attachmentService.findVehicleAttactments(vehicle.getVehicleId());
        if (attachments.isEmpty()){
        	saveError(request, "Vehicle pictures not uploaded. Please upload atleast one photo.");
            return "editVehicleForm";
        }
        vehicle = vehicleManager.update(vehicle);
        saveMessage(request, "Vehicle sucessfully updated.");
        return "redirect:/sellCar/previewVehicleForm/" + vehicle.getVehicleId() ;
 
    }
    
    
    
    

    private boolean isFormSubmission(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase("post");
    }

    protected boolean isAdd(HttpServletRequest request) {
        String method = request.getParameter("method");
        return (method != null && method.equalsIgnoreCase("add"));
    }
    
    
    
}
