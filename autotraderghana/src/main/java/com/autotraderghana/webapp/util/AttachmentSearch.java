package com.autotraderghana.webapp.util;

import java.io.Serializable;

public class AttachmentSearch implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String vehicleId;
	private String idToUse;
	
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getIdToUse() {
		return idToUse;
	}
	public void setIdToUse(String idToUse) {
		this.idToUse = idToUse;
	}
	
}
