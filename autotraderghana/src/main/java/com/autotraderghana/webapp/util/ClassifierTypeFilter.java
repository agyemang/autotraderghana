package com.autotraderghana.webapp.util;

public final class ClassifierTypeFilter {
  
    public static final String CURRENCY = "Currency";
    public static final String FUEL_TYPE = "Fuel Type";
    public static final String BODY_TYPE = "Body Type";
    public static final String GEARBOX_TYPE = "Gearbox Type";	
    public static final String COLOR = "Color";
    public static final String COLOR_TYPE = "Color Type";	
    public static final String PRICE_INFO = "Asking price info";	
    public static final String TELEPHONE_ADDITIONAL_INFO = "Telephone Additional info";	
    public static final String VEHICLE_ACCESSORY = "Vehicle Accessory";	
    public static final String SAFETY = "Safety";	
    public static final String INTERIOR_AND_COMFORT = "Interior and Comforts";	
    public static final String OTHER = "Other";
    
    private ClassifierTypeFilter(){
    }  
}

