package com.autotraderghana.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.VehicleCondition;


@Entity
@Table(name="VEHICLE")
public class Vehicle extends AbstractSuperEntity {
	
	private static final long serialVersionUID = 6978962210399222394L;

	@Id
	@Column(name = "VEHICLE_ID")
	private String vehicleId;
	
	
	@NotNull(message="year is mandatory.")
	@Column(name = "YEAR", nullable = false)
	@DateTimeFormat(pattern="yyyy")
	@Temporal(TemporalType.DATE)
	private Date year;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FUELTYPE_ID", referencedColumnName = "ID")
	private ClassifierItem fuelType;
	
	@NotNull(message="currency is mandatory.")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "price_currency", referencedColumnName = "ID")
	private ClassifierItem priceCurrency;
	
	@NotNull(message="price is mandatory.")
	@Column(name = "PRICE", nullable = false)
	private Double price;
	
	@Column(name = "INCLUDE_PRICE_IN_AD", nullable = true)
	private Boolean includePriceAd = Boolean.TRUE;
	
	@Column(name = "REGISTRATION_COMPLETE")
	@NotNull
	private Boolean isRegCompleted = Boolean.FALSE;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PRICEINFO_ID", referencedColumnName = "ID")
	private ClassifierItem priceInfo;
	
	@Column(name = "MILEAGE", nullable = true)
	private Integer mileage;

	@Column(name = "INCLUDE_MILEAGE_IN_AD", nullable = true)
	private Boolean includeMileageAd = Boolean.TRUE;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "GEARBOXTYPE_ID", referencedColumnName = "ID")
	private ClassifierItem gearBoxType;
	
	@Column(name = "DOOR_COUNT", nullable = true)
	private Integer doorCount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COLOR_ID", referencedColumnName = "ID")
	private ClassifierItem color;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COLORTYPE_ID", referencedColumnName = "ID")
	private ClassifierItem colorType;
	
	@Column(name = "ENGINE_SIZE", nullable = true, precision=1)
	private Double engineSize;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID")
	private User customer;

	
	@OneToOne(cascade = CascadeType.ALL,targetEntity=VehiclePaymentInfo.class)
	@JoinColumn(name = "PAYMENT_ID", referencedColumnName = "ID")
	private VehiclePaymentInfo paymentInfo;
	
	@Column(name = "VEHICLE_NOTES", length=10000)
	private String vehicleNotes;
	
	@Column(name = "TRANSMISSION")
	private String transmission;
	
	@Column(name="VEHICLE_STATUS", nullable=false)
	@Enumerated(EnumType.STRING)
	private StatusEnum status;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name = "vehicle_safety_accessory", joinColumns = { @JoinColumn(name = "vehicle_id") }, inverseJoinColumns = { @JoinColumn(name = "item_id") })
	private List<ClassifierItem> chosen_safety = new ArrayList<ClassifierItem>(); 
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name = "vehicle_interior_accessory", joinColumns = { @JoinColumn(name = "vehicle_id") }, inverseJoinColumns = { @JoinColumn(name = "item_id") })
	private List<ClassifierItem> chosen_interior = new ArrayList<ClassifierItem>(); 
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(name = "vehicle_other_accessory", joinColumns = { @JoinColumn(name = "vehicle_id") }, inverseJoinColumns = { @JoinColumn(name = "item_id") })
	private List<ClassifierItem> chosen_other = new ArrayList<ClassifierItem>(); 

	@NotNull(message="condition is mandatory.")
	@Column(name="VEHICLE_CONDITION", nullable=false)
	@Enumerated(EnumType.STRING)
	private VehicleCondition vehicleCondition;
	
	@NotNull(message="make is mandatory.")
	@ManyToOne
	@JoinColumn(name = "VEHICLE_MAKE_ID", referencedColumnName = "ID")
	private VehicleMake vehicleMake;
	
	@NotNull(message="model is mandatory.")
	@ManyToOne
	@JoinColumn(name = "VEHICLE_MODEL_ID", referencedColumnName = "ID")
	private VehicleModel vehicleModel;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "BODYTYPE_ID", referencedColumnName = "ID")
	private ClassifierItem bodyType;

	@Column(name = "INCLUDE_TELEPHONE_IN_AD", nullable = true)
	private Boolean includeTelephoneInAd = Boolean.TRUE; 
	
	@Embedded
	private AdvertisingDetails advertisingDetails = new AdvertisingDetails();
	
	@Transient
	private String displayTitle;
	
	@Transient
    private List<VehicleAttachment> imageAttachments = new ArrayList<VehicleAttachment>();


	public List<ClassifierItem> getChosen_safety() {
		return chosen_safety;
	}

	public void setChosen_safety(List<ClassifierItem> chosenSafety) {
		chosen_safety = chosenSafety;
	}

	public List<ClassifierItem> getChosen_interior() {
		return chosen_interior;
	}

	public void setChosen_interior(List<ClassifierItem> chosenInterior) {
		chosen_interior = chosenInterior;
	}

	public List<ClassifierItem> getChosen_other() {
		return chosen_other;
	}

	public void setChosen_other(List<ClassifierItem> chosenOther) {
		chosen_other = chosenOther;
	}

	public Vehicle() {
		setStatus(StatusEnum.PENDING);
	}
	
	public Date getYear() {
		return year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public ClassifierItem getFuelType() {
		return fuelType;
	}

	public void setFuelType(ClassifierItem fuelType) {
		this.fuelType = fuelType;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getMileage() {
		return mileage;
	}

	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}

	public ClassifierItem getGearBoxType() {
		return gearBoxType;
	}

	public void setGearBoxType(ClassifierItem gearBoxType) {
		this.gearBoxType = gearBoxType;
	}

	public Integer getDoorCount() {
		return doorCount;
	}

	public void setDoorCount(Integer doorCount) {
		this.doorCount = doorCount;
	}

	public ClassifierItem getColor() {
		return color;
	}

	public void setColor(ClassifierItem color) {
		this.color = color;
	}

	public ClassifierItem getColorType() {
		return colorType;
	}

	public void setColorType(ClassifierItem colorType) {
		this.colorType = colorType;
	}

	public User getCustomer() {
		return customer;
	}

	public void setCustomer(User customer) {
		this.customer = customer;
	}

	public String getVehicleNotes() {
		return vehicleNotes;
	}

	public void setVehicleNotes(String vehicleNotes) {
		this.vehicleNotes = vehicleNotes;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	public Double getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(Double engineSize) {
		this.engineSize = engineSize;
	}

	
	public VehicleMake getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(VehicleMake vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public VehicleModel getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(VehicleModel vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public ClassifierItem getBodyType() {
		return bodyType;
	}

	public void setBodyType(ClassifierItem bodyType) {
		this.bodyType = bodyType;
	}
	
	public Boolean getIncludePriceAd() {
		return includePriceAd;
	}

	public void setIncludePriceAd(Boolean includePriceAd) {
		this.includePriceAd = includePriceAd;
	}

	public ClassifierItem getPriceInfo() {
		return priceInfo;
	}

	public void setPriceInfo(ClassifierItem priceInfo) {
		this.priceInfo = priceInfo;
	}
	
	

	public AdvertisingDetails getAdvertisingDetails() {
		return advertisingDetails;
	}

	public void setAdvertisingDetails(AdvertisingDetails advertisingDetails) {
		this.advertisingDetails = advertisingDetails;
	}

	public ClassifierItem getPriceCurrency() {
		return priceCurrency;
	}

	public void setPriceCurrency(ClassifierItem priceCurrency) {
		this.priceCurrency = priceCurrency;
	}

	public Boolean getIncludeTelephoneInAd() {
		return includeTelephoneInAd;
	}

	public void setIncludeTelephoneInAd(Boolean includeTelephoneInAd) {
		this.includeTelephoneInAd = includeTelephoneInAd;
	}

	public String getDisplayTitle() {
		if (StringUtils.isNotBlank(displayTitle)) {
			return displayTitle;
		}
		String result = "";
		if (StringUtils.isNotBlank(year.toString())) {
			result = year.toString();
		}
//		if (!StringUtils.isEmpty(vehicleMake.getMake())) {
//			result += " " + vehicleMake.getMake();
//		}
		if (StringUtils.isNotBlank(vehicleModel.getModel())) {
			result += " " + vehicleModel.getModel();
		}
		if (null != engineSize) {
			result += " " + engineSize;
		}
		if (null != bodyType) {
			result += " " + bodyType.getName();
		}
		if (StringUtils.isNotBlank(result)) {
			displayTitle = result.trim();
			return displayTitle;
		} else {
			return "No Title";
		}
	}

	public void setDisplayTitle(String displayTitle) {
		this.displayTitle = displayTitle;
	}

	public VehicleCondition getVehicleCondition() {
		return vehicleCondition;
	}

	public void setVehicleCondition(VehicleCondition vehicleCondition) {
		this.vehicleCondition = vehicleCondition;
	}


	public Boolean getIncludeMileageAd() {
		return includeMileageAd;
	}

	public void setIncludeMileageAd(Boolean includeMileageAd) {
		this.includeMileageAd = includeMileageAd;
	}

	public VehiclePaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(VehiclePaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	public Boolean getIsRegCompleted() {
		return isRegCompleted;
	}

	public void setIsRegCompleted(Boolean isRegCompleted) {
		this.isRegCompleted = isRegCompleted;
	}

	/**
	 * @return the vehicleId
	 */
	public String getVehicleId() {
		return vehicleId;
	}

	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public List<VehicleAttachment> getImageAttachments() {
		return imageAttachments;
	}

	public void setImageAttachments(List<VehicleAttachment> imageAttachments) {
		this.imageAttachments = imageAttachments;
	}
	
	
	public void addImageAttachment(VehicleAttachment imageAttachment) {
		List<VehicleAttachment> attachments = imageAttachments;
		
		if(attachments == null)
			attachments = new ArrayList<VehicleAttachment>();
		
		attachments.add(imageAttachment);
		
	}
	
	
}
