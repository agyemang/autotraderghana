package com.autotraderghana.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="VEHICLE_MODEL")
public class VehicleModel extends AbstractEntity implements Comparable<VehicleModel>{

	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "MODEL", nullable = false, unique = true)
	private String model;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "VEHICLE_MAKE_ID", referencedColumnName = "ID")
	private VehicleMake vehicleMake;

	public VehicleModel() {
		
	}

	public VehicleModel(String model) {
		super();
		this.model = model;

	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public VehicleMake getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(VehicleMake vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((vehicleMake == null) ? 0 : vehicleMake.hashCode());
		result = prime * result
				+ ((model == null) ? 0 : model.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VehicleModel other = (VehicleModel) obj;
		if (vehicleMake == null) {
			if (other.vehicleMake != null)
				return false;
		} else if (!vehicleMake.equals(other.vehicleMake))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		return true;
	}

	public int compareTo(VehicleModel o) {
		return model.compareToIgnoreCase(o.model);
	}
	
	@Override
	public String toString(){
		return model;
	}
}
