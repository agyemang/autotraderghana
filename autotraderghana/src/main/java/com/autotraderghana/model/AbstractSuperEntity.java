/**
 * 
 */
package com.autotraderghana.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

@MappedSuperclass
public abstract class AbstractSuperEntity implements Serializable {

        private static final long serialVersionUID = 1L;

        @Column(name = "CREATE_DATE", nullable = false, updatable = false)
        @Temporal(TemporalType.TIMESTAMP)
        private Date createDate = new Date();

        @Column(name = "UPDATE_DATE", updatable = true)
        @Temporal(TemporalType.TIMESTAMP)
        private Date updateDate = new Date();

		/**
		 * @return the createDate
		 */
        @XmlTransient
		public Date getCreateDate() {
			return createDate;
		}

		/**
		 * @param createDate the createDate to set
		 */
		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}

		/**
		 * @return the updateDate
		 */
		@XmlTransient
		public Date getUpdateDate() {
			return updateDate;
		}

		/**
		 * @param updateDate the updateDate to set
		 */
		public void setUpdateDate(Date updateDate) {
			this.updateDate = updateDate;
		}

}