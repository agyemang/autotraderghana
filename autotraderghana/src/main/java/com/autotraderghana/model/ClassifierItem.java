/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.autotraderghana.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * http://www.xaloon.org
 * 
 * @author vytautas racelis
 */
@Entity
@Table(name="CLASSIFIER_ITEM")
public class ClassifierItem extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "CLASSIFIER_ID", referencedColumnName = "ID")
	private Classifier classifier;
	
	@Column(name="CODE", nullable = false)
	private String code;
	
	@Column(name="NAME", nullable = false)
	private String name;
	
	@Column(name="PATH", nullable = false)
	private String path;	
	
	@ManyToOne
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	private ClassifierItem parent;
	

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}

	public Classifier getClassifier() {
		return classifier;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setParent(ClassifierItem parent) {
		this.parent = parent;
	}

	public ClassifierItem getParent() {
		return parent;
	}
	
	@Override
	public int hashCode() {
		if (getId() != null) {
			return getId().intValue();
		} else {
			return super.hashCode();
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if ((obj != null) && (obj instanceof ClassifierItem)) {
			if ((getId() != null) && (((ClassifierItem)obj).getId() != null)) {
				return getId().equals(((ClassifierItem)obj).getId());
			}
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return this.getName() != null ? this.getName(): null;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}
	
}
