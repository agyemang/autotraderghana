package com.autotraderghana.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class VehiclePaymentInfo extends AbstractEntity{
	

	private static final long serialVersionUID = 1L;

	@OneToOne(mappedBy="paymentInfo",targetEntity=Vehicle.class,cascade =CascadeType.MERGE)
	private Vehicle vehicle;
		
	@Column(nullable = false)
	private String paymentId;
	
	@Column(nullable = false)
	private String paymentStatus;
	
	@Column(nullable = false)
	private String paymentAmount;
	
	@Column(nullable = false)
	private String paymentdescription;
	
	@Column(nullable = false)
	private String paymentDate;
	
	@Column(nullable = false)
	private String paymentCurrency;
	
	@Column(nullable = false)
	private String payerEmail;
	
	@Column(nullable = false)
	private String receiverEmail;
	
	@Column(nullable = false)
	private String payerStatus;
	
	@Column(nullable = false)
	private String payerId;
	
	private String pendingReason;
	

	
	public String getPendingReason() {
		return pendingReason;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getPayerStatus() {
		return payerStatus;
	}

	public void setPayerStatus(String payerStatus) {
		this.payerStatus = payerStatus;
	}

	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getPayerEmail() {
		return payerEmail;
	}

	public void setPayerEmail(String payerEmail) {
		this.payerEmail = payerEmail;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentdescription() {
		return paymentdescription;
	}

	public void setPaymentdescription(String paymentdescription) {
		this.paymentdescription = paymentdescription;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setPendingReason(String pendingReason) {
		this.pendingReason  = pendingReason;		
	}
	
	
}

