/*
 *
 */
package com.autotraderghana.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlTransient;


@MappedSuperclass
public abstract class AbstractEntity extends AbstractSuperEntity  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * XmlTransient: This annotation is necessary because of the circular
	 * references between Customer and Email and between Customer and Address,
	 * which is a logical result of their bidirectional relationship. This
	 * circular reference however is problematic for the JAX-WS (generated) code
	 * that tries to serialize the objects to XML. This code will enter an
	 * endless loop. The @XmlTransient annotation tells the serialization
	 * process not to consider this field.
	 */

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@XmlTransient
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
