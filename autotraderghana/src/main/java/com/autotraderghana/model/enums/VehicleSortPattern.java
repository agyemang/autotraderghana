package com.autotraderghana.model.enums;

import java.util.EnumSet;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum VehicleSortPattern {

	price_asc("price_asc", "Price Ascending"), 
	price_desc("price_desc","Price Descending"), 
	year_asc("year_asc", "Year Ascending"), 
	year_desc("year_desc", "Year Descending"), 
	mileage_asc("mileage_asc","Mileage Ascending"), 
	mileage_desc("mileage_desc","Mileage Descending");

	private String value;
	private String name;

	VehicleSortPattern(String value, String name) {
		this.value = value;
		this.name = name;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String toString() {
		return value;
	}

	public VehicleSortPattern getByValue(String value) {
		VehicleSortPattern returnValue = null;
		for (final VehicleSortPattern element : EnumSet
				.allOf(VehicleSortPattern.class)) {
			if (element.toString().equals(value)) {
				returnValue = element;
			}
		}
		return returnValue;
	}
}
