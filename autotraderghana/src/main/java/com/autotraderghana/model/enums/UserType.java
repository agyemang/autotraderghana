package com.autotraderghana.model.enums;

import java.util.EnumSet;



public enum UserType {
	
	CAR_DEALER("Car Dealer"), PRIVATE_SELLER("Private Seller");


	private String value;

	UserType(String value){
        this.value = value;
    }

	
    public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String toString(){
        return value;
    }

    public UserType  getByValue(String value){
    	UserType returnValue = null;
        for (final UserType element : EnumSet.allOf(UserType.class)) {
            if (element.toString().equals(value)) {
                returnValue = element;
            }
        }
        return returnValue;
    }
}