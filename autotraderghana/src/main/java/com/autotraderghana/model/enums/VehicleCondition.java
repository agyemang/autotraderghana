package com.autotraderghana.model.enums;

public enum VehicleCondition {

	NEW,
	USED
}
