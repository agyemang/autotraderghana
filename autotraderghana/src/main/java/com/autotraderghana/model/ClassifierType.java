package com.autotraderghana.model;

public interface ClassifierType {
	
	String CURRENCY = "CURRENCY";
	
	String FUELTYPE = "FUELTYPE";
	
	String BODYTYPE = "BODYTYPE";
	
	String GEARBOXTYPE = "GEARBOXTYPE";
	
	String COLOR = "COLOR";

	String COLORTYPE = "COLORTYPE";
	
	String PRICEINFO = "PRICEINFO";
	
	String TELEADDITIONALINFO = "TELEADDITIONALINFO";

	String VEHICLEACCESSORY = "VEHICLEACCESSORY";
	

}
