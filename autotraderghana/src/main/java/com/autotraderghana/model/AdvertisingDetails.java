package com.autotraderghana.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * This class is used to represent an address with address,
 * city, province and postal-code information.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Embeddable
public class AdvertisingDetails extends BaseObject implements Serializable {
    private static final long serialVersionUID = 3617859655330969141L;
    
    @Column(length = 50)
    private String city;
    
    @Column(length = 100)
    private String province;
    
    @Column(length = 100) 
    private String country;
    
    @Column(name = "postal_code", length = 15)
    private String postalCode;
    
    @Column(name = "phone_number_1")
//    @NotNull(message="phone 1 is mandatory.")
    @Length(max=10,min=10,message="Phone number is not valid. Should be of length 10.")
    @NotEmpty(message="Phone 1 field is mendatory.") 
    @NumberFormat(style= Style.NUMBER)
    private String phoneNumber1; 
    
    @Length(max=10,min=10,message="Phone number is not valid. Should be of length 10.")
    @NumberFormat(style= Style.NUMBER)
    @Column(name = "phone_number_2")
    private String phoneNumber2;

    @Column(nullable = true)
    private String email; 
    
    private String contactPerson;
    
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "phone_number_1_info_id", referencedColumnName = "ID")
	private ClassifierItem phoneNumber1Info;
    
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "phone_number_2_info_id", referencedColumnName = "ID")
	private ClassifierItem phoneNumber2Info;

    
    public String getContactPerson() {
		return contactPerson;
	}


	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}


	public String getCity() {
        return city;
    }

   
    public String getProvince() {
        return province;
    }

   
    public String getCountry() {
        return country;
    }

    
    public String getPostalCode() {
        return postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    
    

    public String getPhoneNumber1() {
		return phoneNumber1;
	}


	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}


	public String getPhoneNumber2() {
		return phoneNumber2;
	}


	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public ClassifierItem getPhoneNumber1Info() {
		return phoneNumber1Info;
	}


	public void setPhoneNumber1Info(ClassifierItem phoneNumber1Info) {
		this.phoneNumber1Info = phoneNumber1Info;
	}


	public ClassifierItem getPhoneNumber2Info() {
		return phoneNumber2Info;
	}


	public void setPhoneNumber2Info(ClassifierItem phoneNumber2Info) {
		this.phoneNumber2Info = phoneNumber2Info;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	/**
     * Overridden equals method for object comparison. Compares based on hashCode.
     *
     * @param o Object to compare
     * @return true/false based on hashCode
     */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdvertisingDetails)) {
            return false;
        }

        final AdvertisingDetails address1 = (AdvertisingDetails) o;

        return this.hashCode() == address1.hashCode();
    }

    /**
     * Overridden hashCode method - compares on address, city, province, country and postal code.
     *
     * @return hashCode
     */
    public int hashCode() {
        int result;
        result = (phoneNumber1 != null ? phoneNumber1.hashCode() : 0);
        result = 29 * result + (city != null ? city.hashCode() : 0);
        result = 29 * result + (province != null ? province.hashCode() : 0);
        result = 29 * result + (country != null ? country.hashCode() : 0);
        result = 29 * result + (postalCode != null ? postalCode.hashCode() : 0);
        return result;
    }

    /**
     * Returns a multi-line String with key=value pairs.
     *
     * @return a String representation of this class.
     */
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("country", this.country)
                .append("phoneNumber1", this.phoneNumber1)
                .append("province", this.province)
                .append("postalCode", this.postalCode)
                .append("city", this.city).toString();
    }
}
