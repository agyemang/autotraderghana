package com.autotraderghana.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.autotraderghana.model.enums.UserType;

@Entity
public class VehiclePostPrice extends AbstractEntity{
	

	private static final long serialVersionUID = 3093515734372073831L;
	
	
	@NotNull(message="amount before tax is mandatory.")
	@Column(nullable = false)
	private Double amountBeforeTax;
	
	@Column(nullable = false)
	private Double amountAfterTax ;
	
	private String postAmountDescription;
	
	@NotNull(message="price currency is mandatory.")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "price_currency", referencedColumnName = "ID")
	private ClassifierItem priceCurrency;
	
	@NotNull(message="tax rate is mandatory.")
	@Column(nullable = false)
	private Double taxRate = 0.0;
	
	@NotNull(message="customer type is mandatory.")
	@Column(nullable = false)
	private UserType userType;


	public Double getAmountBeforeTax() {
		return amountBeforeTax;
	}

	public void setAmountBeforeTax(Double amountBeforeTax) {
		this.amountBeforeTax = amountBeforeTax;
	}

	public Double getAmountAfterTax() {
		return amountAfterTax;
	}

	public void setAmountAfterTax(Double amountAfterTax) {
		this.amountAfterTax = amountAfterTax;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getPostAmountDescription() {
		return postAmountDescription;
	}

	public void setPostAmountDescription(String postAmountDescription) {
		this.postAmountDescription = postAmountDescription;
	}

	public ClassifierItem getPriceCurrency() {
		return priceCurrency;
	}

	public void setPriceCurrency(ClassifierItem priceCurrency) {
		this.priceCurrency = priceCurrency;
	}
	
	

}
