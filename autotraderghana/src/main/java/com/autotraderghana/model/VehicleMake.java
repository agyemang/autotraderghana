package com.autotraderghana.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name="VEHICLE_MAKE")
public class VehicleMake extends AbstractEntity implements Comparable<VehicleMake>{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "MAKE", nullable = false, unique = true)
	private String make;
	
	
	@OneToMany(mappedBy = "vehicleMake", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@OrderBy(value = "id")
	@Cascade(value = org.hibernate.annotations.CascadeType.DELETE)
	private Set<VehicleModel> models = new HashSet<VehicleModel>();

	
	public VehicleMake() {

	}

	public VehicleMake(String name, Set<VehicleModel> vehicleModels) {
		super();
		this.make = name;
		this.models = vehicleModels;
	}

	public String getMake() {
		return make;
	}


	public void setMake(String make) {
		this.make = make;
	}


	public Set<VehicleModel> getModels() {
		return models;
	}


	public void setModels(Set<VehicleModel> models) {
		this.models = models;
	}
	
	public void addChildModel(VehicleModel vehicleModel) {
		if (vehicleModel == null) {
			throw new IllegalArgumentException("child vehicleModel is null!");
		}

		if (vehicleModel.getVehicleMake() != null) {
		
			if (vehicleModel.getVehicleMake().equals(this)) {
				return;
			} else {
				vehicleModel.getVehicleMake().models.remove(this);
			}
		}

		vehicleModel.setVehicleMake(this);
		models.add(vehicleModel);
	}

	public void removeChildModel(VehicleModel vehicleModel) {
		if (vehicleModel == null) {
			throw new IllegalArgumentException("child vehicleModel is null!");
		}

		if (vehicleModel.getVehicleMake() != null && vehicleModel.getVehicleMake().equals(this)) {
			vehicleModel.setVehicleMake(null);
			models.remove(vehicleModel);
		} else {
			throw new IllegalArgumentException(
					"child vehicleModel not associated with this instance");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((make == null) ? 0 : make.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VehicleMake other = (VehicleMake) obj;
		if (make == null) {
			if (other.make != null)
				return false;
		} else if (!make.equals(other.make))
			return false;
		return true;
	}

	public int compareTo(VehicleMake o) {
		return make.compareToIgnoreCase(o.make);
	}

	@Override
	public String toString() {
		return make;
	}
	
	
}
