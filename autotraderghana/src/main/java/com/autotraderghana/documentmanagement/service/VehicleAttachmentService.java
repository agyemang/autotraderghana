package com.autotraderghana.documentmanagement.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.model.Vehicle;

public interface VehicleAttachmentService {

	
	public Page<VehicleAttachment> findAll(int page, int size);

	public List<VehicleAttachment> findVehicleAttactments(String vehicleId);
	
	public VehicleAttachment findOneVehicleAttactments(String vehicleId);
	
	public VehicleAttachment find(String id);

	public void save(VehicleAttachment image);

	public void save(VehicleAttachment image, Vehicle vehicle);
	
	public void delete(String id);

}
