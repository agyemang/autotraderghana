package com.autotraderghana.documentmanagement.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.autotraderghana.documentmanagement.domain.VehicleAttachment;
import com.autotraderghana.documentmanagement.repository.VehicleAttachmentRepository;
import com.autotraderghana.documentmanagement.service.VehicleAttachmentService;
import com.autotraderghana.model.Vehicle;

@Service
public class VehicleAttachmentServiceImpl implements VehicleAttachmentService{

	@Inject
	private VehicleAttachmentRepository attachmentRepository;

	public Page<VehicleAttachment> findAll(int page, int size) {
		Pageable pageable = new PageRequest(page, size);
		Page<VehicleAttachment> images = attachmentRepository.findAll(pageable);
		return images;
	}

	@Override
	public void save(VehicleAttachment image) {
		attachmentRepository.save(image);
	}

	@Override
	public List<VehicleAttachment> findVehicleAttactments(String vehicleId) {
		return attachmentRepository.findByVehicleIdOrderByCreateDateDesc(vehicleId);
	}

	@Override
	public void save(VehicleAttachment image, Vehicle vehicle) {
		if (vehicle.getVehicleId() == null) {
			vehicle.addImageAttachment(image);
		} else {
			save(image);
		}
		
	}

	@Override
	public void delete(String id) {
		attachmentRepository.delete(id);
	}

	@Override
	public VehicleAttachment find(String id) {
		return attachmentRepository.findOne(id);
	}

	@Override
	public VehicleAttachment findOneVehicleAttactments(String vehicleId) {
		return attachmentRepository.findByVehicleIdOrderByCreateDateDesc(vehicleId).get(0);
	}

}
