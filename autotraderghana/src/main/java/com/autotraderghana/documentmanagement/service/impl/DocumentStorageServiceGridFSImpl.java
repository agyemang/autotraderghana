package com.autotraderghana.documentmanagement.service.impl;

import java.io.InputStream;

import org.bson.types.ObjectId;

import com.autotraderghana.documentmanagement.service.DocumentStorageService;
import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;


public class DocumentStorageServiceGridFSImpl implements DocumentStorageService {

	private final GridFS gridFs;

	public DocumentStorageServiceGridFSImpl(DB gridfsDb) {
		gridFs = new GridFS(gridfsDb);
	}

	public String save(InputStream inputStream, String contentType, String filename) {
		
		GridFSInputFile input = gridFs.createFile(inputStream, filename, true);
		input.setContentType(contentType);
		input.save();
		return input.getId().toString();
	}

	public GridFSDBFile get(String id) {
		return gridFs.findOne(new ObjectId(id));
	}

	public GridFSDBFile getByFilename(String filename) {
		return gridFs.findOne(filename);
	}

	@Override
	public void deleteAttachment(String id) {
		gridFs.remove(new ObjectId(id));		
	}	
	
	
	
}

