package com.autotraderghana.documentmanagement.utils;

public enum SortPhase {
    AFTER_CONVERT,
    BEFORE_CONVERT;
}
