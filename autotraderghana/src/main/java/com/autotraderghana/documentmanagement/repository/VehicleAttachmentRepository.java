package com.autotraderghana.documentmanagement.repository;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.autotraderghana.documentmanagement.domain.VehicleAttachment;



public interface VehicleAttachmentRepository extends MongoRepository<VehicleAttachment, String> {
	
	List<VehicleAttachment> findByVehicleIdOrderByCreateDateDesc(String vehicleId);

}
