/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.autotraderghana.customFacade;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Simple query helper to build JPA/SQL queries
 * 
 * @author vytautas racelis
 *
 */
public class QueryBuilder {
	private static final Log logger = LogFactory.getLog(QueryBuilder.class);
	
	public static final String INNER_JOIN = " INNER JOIN ";
	public static final String OUTER_JOIN = " LEFT OUTER JOIN ";
	public enum Condition {
        AND, OR
    };
    
    private StringBuilder query = new StringBuilder();
    
    private StringBuilder filter = new StringBuilder();
    private StringBuilder orderBy = new StringBuilder();
    
    private Map<String, Object> parameters = new HashMap<String, Object>();
    
	public QueryBuilder(String sql) {
		query.append(sql);
	}

	/**
	 * Add simple parameter
	 * 
	 * @param name name of query field
	 * @param parameterName parameter name
	 * @param value parameter value
	 */
	public void addParameter(String name, String parameterName, Object value) {
		addParameter(name, parameterName, value, Condition.AND, false, false);
	}
	
	public void addParameter(String name, String parameterName, Object value, boolean isLike) {
		addParameter(name, parameterName, value, Condition.AND, isLike, isLike);
	}
	
	public void addParameter(String name, String parameterName, Object value, boolean isLike, boolean isUpper) {
		addParameter(name, parameterName, value, Condition.AND, isLike, isUpper);
	}

	private void addParameter(String name, String parameterName, Object value, Condition condition, boolean isLike, boolean isUpper) {
		// do not support null values yet
		if (isEmptyValue(value)) {
			return;
		}
		registerParameter(parameterName, value, isLike, isUpper);
		if (filter.length() != 0) {
			filter.append(' ').append(condition.name()).append(' ');
        }
		filter.append("(");
		if (isUpper) {
			filter.append("UPPER(");
		}
		filter.append(name);
		if (isUpper) {
			filter.append(")");
		}
		if (isLike) {
			filter.append(" LIKE ");
		} else {
			filter.append("=");
		}
		if (isUpper) {
			filter.append("UPPER(");
		}
		filter.append(":");
		filter.append(parameterName);
		if (isUpper) {
			filter.append(")");
		}
		filter.append(")");
	}

	private void registerParameter(String parameterName, Object value, boolean isLike, boolean isUpper) {
		if (value instanceof String) {
			StringBuilder parsedValue = new StringBuilder(((String) value).trim());
			if (isLike) {
				parsedValue.insert(0, "%");
				parsedValue.append("%");
			}			
            parameters.put(parameterName, parsedValue.toString());
        } else {
            parameters.put(parameterName, value);
        }
	}

	private boolean isEmptyValue(Object value) {
		if (value instanceof String) {
			return StringUtils.isEmpty((String)value);
		}
		return value == null;
	}

	/**
	 * 
	 * @return mapped parameters
	 * 
	 */
	public Map<String, Object> getParameters() {
		return parameters;
	}
	
	/**
	 * 
	 * @return full generated query
	 */
	public String getQuery() {
		StringBuilder fullQuery = new StringBuilder();
		fullQuery.append(query);
		if (filter.length() != 0) {
			fullQuery.append(" WHERE ");
			fullQuery.append(filter.toString());
        }
		if (orderBy.length() != 0) {
			fullQuery.append(' ');
			fullQuery.append(orderBy);
			fullQuery.append(' ');
        }
		String result = fullQuery.toString();
		if (logger.isDebugEnabled()) {
			logger.debug(result);
			StringBuilder params = new StringBuilder();
			for (Map.Entry<String, Object> o : parameters.entrySet()) {
				if (params.length() > 0) {
					params.append(",");
				}
				params.append(o.getKey());
				params.append("=");
				params.append(o.getValue());
			}
			logger.debug("Parameters: " + params);			
		}
		return result;
	}
	
	@Override
	public String toString() {
		return getQuery();
	}

	/**
	 * 
	 * @param orderBy how to order result
	 */
	public void addOrderBy(String parameters) {
		orderBy.append(" ORDER BY ");
		orderBy.append(parameters);
	}

	/**
	 * Add join to main query string
	 * 
	 * @param joinType join type - inner join, left outer join
	 * @param joinQuery what to join
	 */
	public void addJoin(String joinType, String joinQuery) {
		if (query.indexOf(joinQuery) < 0) {
			query.append(joinType);
			query.append(joinQuery);
		}
	}

	public void addExpression(String expression, String parameterName, Object value) {
		addExpression(expression, parameterName, Condition.AND, value);
	}
	
	public void addExpression(String expression) {
		registerExpression(expression, Condition.AND);
	}
	
	public void addExpression(String expression, String parameterName, Condition condition, Object value) {
		// do not support null values yet
		if (isEmptyValue(value)) {
			return;
		}
		registerParameter(parameterName, value, false, false);
		registerExpression(expression, condition);
	}

	private void registerExpression(String expression, Condition condition) {
		if (filter.length() != 0) {
			filter.append(' ').append(condition.name()).append(' ');
        }
		filter.append("(");
		filter.append(expression);
		filter.append(")");
	}

	/**
	 * Check if query contains filter parameters
	 * 
	 * @return true if no parameters are set to query
	 */
	public boolean isEmpty() {
		return parameters.isEmpty();
	}

	public void addExpression(String expression, String[] parameterNames, Object[] values) {
		if (values.length < 0) {
			return;
		}
		int i = 0;
		for (String parameterName : parameterNames) {
			registerParameter(parameterName, values[i++], false, false);
		}
		registerExpression(expression, Condition.AND);
	}
}
