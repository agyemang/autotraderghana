/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.autotraderghana.customFacade;

import java.util.List;

import com.autotraderghana.model.Classifier;
import com.autotraderghana.model.ClassifierItem;

/**
 * Classifier related methods
 * 
 * http://www.xaloon.org
 * 
 * @author vytautas racelis
 */
public interface ClassifierManager {
	/**
	 * Order returned classifier items by code
	 */
	int ORDER_BY_CODE = 1;
	
	/**
	 * Order returned classifier items by name
	 */
	int ORDER_BY_NAME = 2;
	
	ClassifierItem findById(Long id);
	
	/**
	 * find default value for selected classifier type
	 * 
	 * @param type
	 * @return
	 */
	ClassifierItem getDefaultValue(String type);

	/**
	 * find all values for selected classifier type
	 * 
	 * @param type
	 * @return
	 */
	List<ClassifierItem> findItems(String type);

	/**
	 * Find children entries by provided parent
	 * 
	 * @param continentCountry
	 * @param parent
	 * @return
	 */
	List<ClassifierItem> findItems(String continentCountry, ClassifierItem parent);

	/**
	 * Select total count of classifier items
	 * 
	 * @param classifierType
	 * @return
	 */
	Long retrieveClassifierItemCount(String classifierType, ClassifierItem parent);
	
	/**
	 * Select total count of classifier items
	 * 
	 * @param classifierType
	 * @param parentClassifierItemId
	 * @return
	 */
	Long retrieveClassifierItemCount(String classifierType, Long parentClassifierItemId);
	
	/**
	 * Find range of classifier items
	 * 
	 * @param classifierType
	 * @param first
	 * @param count
	 * @return
	 */
	List<ClassifierItem> findItems(String type, ClassifierItem parent, int first, int count);

	/**
	 * Find classifiers by parent name
	 * 
	 * @param classifierType
	 * @param parentClassifierItemName
	 * @param first
	 * @param count
	 * @return
	 */
	List<ClassifierItem> findItems(String classifierType, String parentClassifierItemName, int first, int count);

	/**
	 * Find classifier item by name
	 * 
	 * @param type
	 * @param name
	 * @return
	 */
	ClassifierItem findItemByName(String type, String name);

	/**
	 * Find classifier by type
	 * 
	 * @param type
	 * @return
	 */
	Classifier findClassifier(String type);

	
	/**
	 * Create classifier
	 * 
	 * @param classifier
	 * @return
	 */
	Classifier createClassifier(Classifier classifier);

	/**
	 * Create classifier item
	 * 
	 * @param result
	 * @return
	 */
	ClassifierItem createClassifierItem(ClassifierItem result);

	/**
	 * Find classifier item by code
	 * 
	 * @param type
	 * @param code
	 * @return
	 */
	ClassifierItem findItemByCode(String type, String code);

	/**
	 * Provide customer order type for selected classifier items
	 * 
	 * @param classifierType
	 * @param parentClassifierItemName
	 * @param first
	 * @param count
	 * @param orderByCode
	 * @return
	 */
	List<ClassifierItem> findItems(String classifierType, String parentClassifierItemName, int first, int count, int orderByCode);

	/**
	 * Provide customer order type for selected classifier items
	 * 
	 * @param classifierType
	 * @param parentClassifierItemId
	 * @param first
	 * @param count
	 * @return
	 */
	List<ClassifierItem> findItems(String classifierType, Long parentClassifierItemId, int first, int count);
	
	/**
	 * Return total count of classifiers
	 * @return
	 */
	int retrieveTotalClassifierCount();

	/**
	 * Return count of classifiers starting with first
	 * 
	 * @param first
	 * @param count
	 * @return
	 */
	List<Classifier> findAllClassifiers(int first, int count);

	/**
	 * Delete classifier item
	 * 
	 * @param classifierItem
	 */
	boolean deleteClassifierItem(ClassifierItem classifierItem);

}
