/**
 * 
 */
package com.autotraderghana.customFacade;


public interface GlobalCounterService {

	String getNextVehicleNumber();

}
