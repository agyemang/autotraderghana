package com.autotraderghana.customFacade.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;

import com.autotraderghana.customFacade.GlobalCounterService;

@Component("globalCounterService")
public class GlobalCounterServiceImpl implements GlobalCounterService {
	

	private EntityManager entityManager;

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public String getNextVehicleNumber() {
		return (String)entityManager.createNativeQuery("select pad_to_n(nextval('vehicle_number_seq'),8)").getSingleResult();
	}


}
