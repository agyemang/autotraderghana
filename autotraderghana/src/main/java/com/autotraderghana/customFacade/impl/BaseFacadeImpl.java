/*
 *    xaloon - http://www.xaloon.org
 *    Copyright (C) 2008-2009 Vytautas Racelis
 *
 *    This file is part of xaloon.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.autotraderghana.customFacade.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.autotraderghana.customFacade.BaseFacade;
import com.autotraderghana.customFacade.QueryBuilder;
import com.autotraderghana.model.AbstractEntity;

@Transactional
@SuppressWarnings("unchecked")
@Component("baseFacade")
public class BaseFacadeImpl implements BaseFacade {
	
	private transient EntityManager em;
	
	@PersistenceContext
	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	public EntityManager getEm() {
		return em;
	}

	public <T> void save (T o) {
		em.persist(o);
	}
	
	public <T> T merge (T o) {
		return em.merge(o);
	}
	
	public <T> void delete (T o) {
		em.remove (o);
	}
	
	public <T> void delete (Class<T> clazz, Long id) {
		em.remove (em.find(clazz, id));
	}	
	
	public <T> T load (Class<T> clazz, Long id) {
		return em.find(clazz, id);
	}
	
	public <T> T load (Class<T> clazz, String id) {
		return em.find(clazz, id);
	}
	
	public <T, K> int executeUpdate (String sql, Map<String, K> params) {
		Query q = em.createQuery(sql);
		if (params != null) {
			for (Map.Entry<String, K> param : params.entrySet()) {
				q.setParameter(param.getKey(), param.getValue());
			}
		}
		int result = q.executeUpdate();
		em.flush();
		return result;
	}
	
	public <T, K> List<T> executeQuery (String sql, Map<String, K> params, Boolean cache, int max) {
		return executeQuery(sql, params, cache, 0, max);
	}
	
	
	public <T, K> List<T> executeQuery (String sql, Map<String, K> params) {
		return executeQuery(sql, params, Boolean.TRUE, -1);
	}
	
	public <T, K> T executeQuerySingle (String sql, Map<String, K> params) {
		return (T)executeQuerySingle(sql, params, false);
	}
	
	public <T, K> T executeQuerySingle (String sql, Map<String, K> params, boolean cacheable) {
		Query q = em.createQuery(sql);
		return (T)processQuerySingle(params, q, cacheable);
	}

	private <T, K> T processQuerySingle(Map<String, K> params, Query q, boolean cacheable) {
		if (params != null) {
			for (Map.Entry<String, K> param : params.entrySet()) {
				q.setParameter(param.getKey(), param.getValue());
			}
		}
		try {
			q.setHint("org.hibernate.cacheable", cacheable);
			return (T)q.getSingleResult();
		} catch (NoResultException e) {}
		return null;
	}

	public <T, K> T executeNamedQuerySingle(String namedQuery, Map<String, K> params) {
		Query q = em.createNamedQuery(namedQuery);
		for (Map.Entry<String, K> param : params.entrySet()) {
			q.setParameter(param.getKey(), param.getValue());
		}
		try {
			
			return (T)q.getSingleResult();
		} catch (NoResultException e) {}
		return null;
	}

	public <T, K> List<T> executeNamedQuery(String namedQuery, Map<String, K> params) {
		Query q = em.createNamedQuery(namedQuery);
		if (params != null) {
			for (Map.Entry<String, K> param : params.entrySet()) {
				q.setParameter(param.getKey(), param.getValue());
			}
		}
		return q.getResultList();
	}

	public int executeSQLQuery(String sql) {
		Query q = em.createNativeQuery(sql);
		return q.executeUpdate();
	}

	public <T> void lock(T obj, LockModeType mode) {
		em.lock(obj, mode);
	}

	public <T, K> List<T> executeQuery(String sql, Map<String, K> params, Boolean cache, int first, int max) {
		Query q = em.createQuery(sql);
		return processQuery(params, cache, first, max, q);
	}

	private <T, K> List<T> processQuery(Map<String, K> params, Boolean cache, int first, int max, Query q) {
		if (first > 0) {
			q.setFirstResult(first);
		}
 		if (max > 0) {
			q.setMaxResults(max);
		}
 		if (params != null) {
			for (Map.Entry<String, K> param : params.entrySet()) {
				q.setParameter(param.getKey(), param.getValue());
			}
		}
		q.setHint("org.hibernate.cacheable", cache);
		
 		List<T> result = q.getResultList();
		result.size ();		
		return result;
	}

	public <T, K> T executeNativeQuerySingle(String sql) {
		return (T)em.createNativeQuery(sql).getSingleResult();
	}
	
	public <T> List<T> executeNativeQuery(String sql) {
		return em.createNativeQuery(sql).getResultList();
	}

	public AbstractEntity saveOrMerge(AbstractEntity entity) {
		if (entity.getId() == null ) {
			save(entity);
			return entity;
		} else {
			return merge(entity);
		}
	}

	public List<Object[]> executeNativeQuery(String sql, Map<String, Object> params, int first, int max) {
		Query q = em.createNativeQuery(sql);
		return processQuery(params, Boolean.FALSE, first, max, q);
	}

	public <T, K> T executeNativeQuerySingle(String sql, Map<String, K> params) {
		return (T)executeNativeQuerySingle(sql, params, false);
	}
	
	public <T, K> T executeNativeQuerySingle(String sql, Map<String, K> params, boolean cacheable) {
		Query q = em.createNativeQuery(sql);
		return (T)processQuerySingle(params, q, cacheable);
	}

	public <T> List<T> executeQuery(QueryBuilder sql) {
		return executeQuery(sql.getQuery(), sql.getParameters());
	}

	public <T> T executeQuerySingle(QueryBuilder sql) {
		return (T)executeQuerySingle(sql.getQuery(), sql.getParameters());
	}

	public List<Object[]> executeQuery(QueryBuilder sql, int first, int count) {
		return executeQuery(sql.getQuery(), sql.getParameters(), Boolean.TRUE, first, count);
	}
}
