/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.autotraderghana.customFacade.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.autotraderghana.customFacade.BaseFacade;
import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.model.Classifier;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.util.PathUtils;

/**
 * http://www.xaloon.org
 * 
 * @author vytautas racelis
 */
@Component("classifierFacade")
public class ClassifierManagerImpl implements ClassifierManager {
	private static final Log logger = LogFactory.getLog(ClassifierManagerImpl.class);
	
	@Autowired
	private BaseFacade baseFacade;
	
	public ClassifierItem getDefaultValue(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<ClassifierItem> findItems(String type) {
		return findItems(type, null);
	}

	public List<ClassifierItem> findItems(String type, ClassifierItem parent) {
		return findItems(type, parent, 0, -1);
	}

	public List<ClassifierItem> findItems(String type, ClassifierItem parent, int first, int count) {
		return findInternalItems(type, (parent != null)?parent.getId():null, null, first, count, ORDER_BY_NAME);
	}

	private List<ClassifierItem> findInternalItems(String type, Long parentClassifierItemId, String parentClassifierItemName, int first, int count, int defaultOrder) {
		String sql = "select i from ClassifierItem i join i.classifier c where c.type = :TYPE ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("TYPE", type);
		if (parentClassifierItemId != null) {
			params.put("PARENT", parentClassifierItemId);
			sql += " and i.parent.id = :PARENT";
		} 
		if ((parentClassifierItemId != null) || !StringUtils.isEmpty(parentClassifierItemName)) {
			if (parentClassifierItemId != null) {
				params.put("PARENT", parentClassifierItemId);
				sql += " and i.parent.id = :PARENT";
			} else {
				params.put("PARENT_NAME", parentClassifierItemName);
				sql += " and i.parent.name = :PARENT_NAME";
			}
		} else {
			sql += " and i.parent is null";
		}
		switch (defaultOrder) {
		case ORDER_BY_CODE:
			sql += " order by i.code";
			break;
		default:
			sql += " order by i.name";
			break;
		}
		List<ClassifierItem> result = baseFacade.executeQuery(sql, params, Boolean.TRUE, first, count);
		return result;
	}

	public Long retrieveClassifierItemCount(String classifierType, ClassifierItem parent) {
		return retrieveClassifierItemCount(classifierType, (parent != null)? parent.getId():null);	
	}

	public List<ClassifierItem> findItems(String classifierType, String parentClassifierItemName, int first, int count) {
		return findInternalItems(classifierType, null, parentClassifierItemName, first, count, ORDER_BY_NAME);
	}

	public ClassifierItem findItemByName(String type, String name) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CLASSIFIER_TYPE", type);
		params.put("NAME", name.toUpperCase());
		return baseFacade.executeQuerySingle("select i from ClassifierItem i inner join i.classifier c where c.type = :CLASSIFIER_TYPE and  upper(i.name) = :NAME", params);
	}

	public Classifier findClassifier(String type) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CLASSIFIER_TYPE", type);
		return baseFacade.executeQuerySingle("select c from Classifier c where c.type = :CLASSIFIER_TYPE", params, true);
	}

	public Classifier createClassifier(Classifier classifier) {
		classifier.setCreateDate(new Date());
		classifier.setUpdateDate(new Date());
		return baseFacade.saveOrMerge(classifier);
	}

	public ClassifierItem createClassifierItem(ClassifierItem item) {
		item.setCreateDate(new Date());
		item.setUpdateDate(new Date());
		item.setPath(PathUtils.encode(item.getName()));
		if (StringUtils.isEmpty(item.getCode())) {
			item.setCode(item.getPath());
		}
		return baseFacade.saveOrMerge(item);
	}

	public ClassifierItem findItemByCode(String type, String code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("CLASSIFIER_TYPE", type);
		params.put("CODE", code);
		return baseFacade.executeQuerySingle("select i from ClassifierItem i inner join i.classifier c where c.type = :CLASSIFIER_TYPE and  i.code = :CODE", params, true);
	}

	public List<ClassifierItem> findItems(String classifierType, String parentClassifierItemName, int first, int count, int orderType) {
		return findInternalItems(classifierType, null, parentClassifierItemName, first, count, orderType);
	}

	public List<Classifier> findAllClassifiers(int first, int count) {
		return baseFacade.executeQuery("select c from Classifier c", null, Boolean.TRUE, first, count);
	}

	public int retrieveTotalClassifierCount() {
		Long result = baseFacade.executeQuerySingle("select count(c) from Classifier c", null, true);
		return result.intValue();
	}

	public List<ClassifierItem> findItems(String classifierType, Long parentClassifierItemId, int first, int count) {
		return findInternalItems(classifierType, parentClassifierItemId, null, first, count, ORDER_BY_NAME);
	}

	public Long retrieveClassifierItemCount(String classifierType, Long parentClassifierItemId) {
		if (!StringUtils.isEmpty(classifierType)) {
			Map<String, Object> params = new HashMap<String, Object>();
			String sql = "select count(ci) from ClassifierItem ci inner join ci.classifier c where c.type = :CLASSIFIER_TYPE";
			params.put("CLASSIFIER_TYPE", classifierType);
			if (parentClassifierItemId != null && parentClassifierItemId > 0) {
				params.put("PARENT", parentClassifierItemId);
				sql += " and ci.parent.id = :PARENT";
			} else {
				sql += " and ci.parent is null";
			}
			return baseFacade.executeQuerySingle(sql, params, true);
		}
		return 0L;
	}

	public boolean deleteClassifierItem(ClassifierItem classifierItem) {
		boolean result = false;
		try {
			baseFacade.delete(ClassifierItem.class, classifierItem.getId());
			result = true;
		} catch (Exception e) {
			logger.error("Could not delete classifier item", e);
		}
		return result;
	}

	@Override
	public ClassifierItem findById(Long id) {
		return baseFacade.load(ClassifierItem.class, id);
	}
}
