/*
 *    xaloon - http://www.xaloon.org
 *    Copyright (C) 2008-2009 Vytautas Racelis
 *
 *    This file is part of xaloon.
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.autotraderghana.customFacade;

import java.util.List;
import java.util.Map;

import javax.persistence.LockModeType;

import com.autotraderghana.model.AbstractEntity;

public interface BaseFacade {
	<T> void save (T o);
	
	<T> T merge (T o);
	
	<T> void delete (T o);
	
	<T> void delete (Class<T> clazz, Long id);
	
	<T> T load (Class<T> clazz, Long id);
	
	<T> T load (Class<T> clazz, String id);
	
	<T, K> int executeUpdate (String sql, Map<String, K> params);
	
	<T, K> List<T> executeQuery (String sql, Map<String, K> params, Boolean cache, int max);	
	<T, K> List<T> executeQuery (String sql, Map<String, K> params, Boolean cache, int first, int max);	
	
	<T, K> List<T> executeQuery (String sql, Map<String, K> params);
	
	<T, K> T executeQuerySingle (String sql, Map<String, K> params);
	<T, K> T executeQuerySingle (String sql, Map<String, K> params, boolean cacheable);

	<T, K> T executeNamedQuerySingle(String namedQuery, Map<String, K> params);
	
	<T, K> List<T> executeNamedQuery(String namedQuery, Map<String, K> params);

	int executeSQLQuery(String sql);

	<T> void lock(T obj, LockModeType mode);

	<T, K> T executeNativeQuerySingle(String sql);
	
	<T> List<T> executeNativeQuery(String sql);
	
	<T> T saveOrMerge (AbstractEntity entity);

	List<Object[]> executeNativeQuery(String sql, Map<String, Object> params, int first, int max);

	<T, K> T executeNativeQuerySingle(String sql, Map<String, K> params);

	/**
	 * Return result for selected query
	 * 
	 * @param <T>
	 * @param query
	 * @return
	 */
	<T> List<T> executeQuery(QueryBuilder query);

	/**
	 * Return single row for selected query
	 * 
	 * @param <T>
	 * @param query
	 * @return
	 */
	<T> T executeQuerySingle(QueryBuilder query);

	/**
	 * Execute from from start point and result count of rows
	 * 
	 * @param query
	 * @param first
	 * @param count
	 * @return
	 */
	<T> List<T> executeQuery(QueryBuilder query, int first, int count);
}
