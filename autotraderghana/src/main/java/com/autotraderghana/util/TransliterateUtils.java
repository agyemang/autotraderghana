package com.autotraderghana.util;

import org.apache.commons.codec.language.Metaphone;
import org.apache.commons.lang.StringUtils;

public class TransliterateUtils {
	private static final String[] charsTransliterateFrom = new String[] { "Ą", "Č", "Ę", "Ė", "Į", "Š", "Ų", "Ū", "Ž", "ą", "č", "ę", "ė", "į", "š",
			"ų", "ū", "ž", "А", "а", "Б", "б", "В", "в", "Г", "г", "Д", "д", "Е", "е", "Ё", "ё", "Ж", "ж", "З", "з", "И", "и", "Й", "й", "К", "к",
			"Л", "л", "М", "м", "Н", "н", "О", "о", "П", "п", "Р", "р", "С", "с", "Т", "т", "У", "у", "Ф", "ф", "Х", "х", "Ц", "ц", "Ч", "ч", "Ш",
			"ш", "Щ", "щ", "Ъ", "ъ", "Ы", "ы", "Ь", "ь", "Э", "э", "Ю", "ю", "Я", "я" };
	private static final String[] charsTransliterateTo = new String[] { "A", "C", "E", "E", "I", "S", "U", "U", "Z", "a", "c", "e", "e", "i", "s",
			"u", "u", "z", "A", "a", "B", "b", "V", "v", "G", "g", "D", "d", "E", "e", "Yo", "yo", "Zh", "zh", "Z", "z", "I", "i", "J", "j", "K",
			"k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p", "R", "r", "S", "s", "T", "t", "U", "u", "F", "f", "H", "h", "Ts", "ts", "Ch",
			"ch", "Sh", "sh", "Sch", "sch", "'", "'", "I", "i", "'", "'", "E", "e", "Yu", "yu", "Ya", "ya" };

	public static String transliterate(String path) {
		for (int i = 0; i < charsTransliterateFrom.length; i++) {
			path = path.replace(charsTransliterateFrom[i], charsTransliterateTo[i]);
		}
		return path;
	}
	
	public static String fuzzyCustom(String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		String result = value.toUpperCase().replaceAll("[AEYWYIOU]", "");
		if (StringUtils.isEmpty(result)) {
			result = value;
		}
		return result.replaceAll("%", "");
	}
	
	public static String fuzzy(String value) {
		return fuzzyExternal(value);
	}

	public static String fuzzyExternal(String value) {
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		String result = "";
		String[] items = transliterate(value).split(" ");
		for (String item : items) {
			result += new Metaphone().metaphone(item);	
		}
		if (StringUtils.isEmpty(result)) {
			result = value;
		}
		return result.replaceAll("%", "");
	}
	
	public static String textNoHtml(String input) {
		if (StringUtils.isEmpty(input)) {
			return null;
		}
		return input.replaceAll("\\<[\\s]*tag[^>]*>","");
	}

	public static String transliterateAndJoin(String name, Long pathId) {
		String transliterated = transliterate(name);
		StringBuilder sb = new StringBuilder(transliterated);
		if (!name.equals(transliterated)) {			
			sb.append(" (");
			sb.append(name);
			sb.append(")");			
		}
		if (pathId != null) {
			sb.append(" [");
			sb.append(pathId);
			sb.append("]");
		}
		return sb.toString();
	}
}
