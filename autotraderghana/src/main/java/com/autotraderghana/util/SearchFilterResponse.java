package com.autotraderghana.util;

import java.io.Serializable;
import java.util.Date;

import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;

import com.autotraderghana.model.enums.VehicleSortPattern;


public class SearchFilterResponse implements Serializable {
	
	private static final long serialVersionUID = 6978962210399222394L;
	
	private Date yearFrom;
	private Date yearTo;
	private Long fuelType;
	private Double priceFrom;
	private Double priceTo;
	private Integer mileageFrom;
	private Integer mileageTo;
	private Long gearBoxType;
	private Double engineSizeFrom;
	private Double engineSizeTo;
	private UserType customerType;
	private VehicleCondition vehicleCondition;
	private Long vehicleMake;
	private Long vehicleModel;
	private Long bodyType;
	private VehicleSortPattern vehicleSortPattern;
	private int pageIndex = 0;
	private Boolean isNew24 = Boolean.FALSE;
	private Boolean isUpdated24 = Boolean.FALSE;
	private Boolean iSMoreSearch = Boolean.FALSE;
	
	
	public SearchFilterResponse() {
		setVehicleSortPattern(VehicleSortPattern.year_desc);
	}
	
	public Boolean noSearch() {
        if ( yearFrom == null
                && yearTo == null
                && fuelType == null
                && priceFrom == null
                && priceTo == null
                && mileageFrom == null
                && mileageTo == null
                && gearBoxType == null
                && engineSizeFrom == null
                && engineSizeTo == null
                && customerType == null
                && vehicleCondition == null
                && vehicleMake == null
                && vehicleModel == null
        		&& vehicleSortPattern ==null
        		&& isNew24 == null
        		&& isUpdated24 == null
                && bodyType == null)

            return true;
        else return false;
    }
	
	
	
	public Boolean getIsNew24() {
		return isNew24;
	}

	public void setIsNew24(Boolean isNew24) {
		this.isNew24 = isNew24;
	}

	public Boolean getIsUpdated24() {
		return isUpdated24;
	}

	public void setIsUpdated24(Boolean isUpdated24) {
		this.isUpdated24 = isUpdated24;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Boolean getiSMoreSearch() {
		return iSMoreSearch;
	}

	public void setiSMoreSearch(Boolean iSMoreSearch) {
		this.iSMoreSearch = iSMoreSearch;
	}

	public Date getYearFrom() {
		return yearFrom;
	}

	public void setYearFrom(Date yearFrom) {
		this.yearFrom = yearFrom;
	}

	public Date getYearTo() {
		return yearTo;
	}

	public void setYearTo(Date yearTo) {
		this.yearTo = yearTo;
	}

	public Long getFuelType() {
		return fuelType;
	}

	public void setFuelType(Long fuelType) {
		this.fuelType = fuelType;
	}

	public Double getPriceFrom() {
		return priceFrom;
	}

	public void setPriceFrom(Double priceFrom) {
		this.priceFrom = priceFrom;
	}

	public Double getPriceTo() {
		return priceTo;
	}

	public void setPriceTo(Double priceTo) {
		this.priceTo = priceTo;
	}

	public Integer getMileageFrom() {
		return mileageFrom;
	}

	public void setMileageFrom(Integer mileageFrom) {
		this.mileageFrom = mileageFrom;
	}

	public Integer getMileageTo() {
		return mileageTo;
	}

	public void setMileageTo(Integer mileageTo) {
		this.mileageTo = mileageTo;
	}

	public Long getGearBoxType() {
		return gearBoxType;
	}

	public void setGearBoxType(Long gearBoxType) {
		this.gearBoxType = gearBoxType;
	}

	public Double getEngineSizeFrom() {
		return engineSizeFrom;
	}

	public void setEngineSizeFrom(Double engineSizeFrom) {
		this.engineSizeFrom = engineSizeFrom;
	}

	public Double getEngineSizeTo() {
		return engineSizeTo;
	}

	public void setEngineSizeTo(Double engineSizeTo) {
		this.engineSizeTo = engineSizeTo;
	}

	public UserType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(UserType customerType) {
		this.customerType = customerType;
	}

	public VehicleCondition getVehicleCondition() {
		return vehicleCondition;
	}

	public void setVehicleCondition(VehicleCondition vehicleCondition) {
		this.vehicleCondition = vehicleCondition;
	}

	public Long getVehicleMake() {
		return vehicleMake;
	}

	public void setVehicleMake(Long vehicleMake) {
		this.vehicleMake = vehicleMake;
	}

	public Long getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(Long vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public Long getBodyType() {
		return bodyType;
	}

	public void setBodyType(Long bodyType) {
		this.bodyType = bodyType;
	}

	public VehicleSortPattern getVehicleSortPattern() {
		return vehicleSortPattern;
	}

	public void setVehicleSortPattern(VehicleSortPattern vehicleSortPattern) {
		this.vehicleSortPattern = vehicleSortPattern;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	
}
