package com.autotraderghana.util;

import java.io.Serializable;


public class DealerSearchDTO implements Serializable {
	
	private static final long serialVersionUID = 6978962210399222394L;
	
	private Long dealerId;
	private Integer pageIndex = 0;
	
	public DealerSearchDTO() {
	}
	
	public Boolean noSearch() {
        if ( dealerId == null && pageIndex == null)
            return true;
        else return false;
    }
	
	
	
	public Long getDealerId() {
		return dealerId;
	}

	public void setDealerId(Long dealerId) {
		this.dealerId = dealerId;
	}

	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		return pageIndex;
	}

	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	
	
	

}
