package com.autotraderghana.util;

import java.io.Serializable;
import java.util.Date;

import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.model.enums.VehicleSortPattern;


public class SearchFilter implements Serializable {
	
	private static final long serialVersionUID = 6978962210399222394L;
	
	private String vehicleId;
	private Date yearFrom;
	private Date yearTo;
	private ClassifierItem fuelType;
	private Double priceFrom;
	private Double priceTo;
	private Integer mileageFrom;
	private Integer mileageTo;
	private ClassifierItem gearBoxType;
	private Double engineSizeFrom;
	private Double engineSizeTo;
	private UserType customerType;
	private VehicleCondition vehicleCondition;
	private VehicleMake vehicleMake;
	private VehicleModel vehicleModel;
	private ClassifierItem bodyType;
	private VehicleSortPattern vehicleSortPattern;
	private Integer pageIndex = 0;
	private Boolean isNew24 = Boolean.FALSE;
	private Boolean isUpdated24 = Boolean.FALSE;
	private Boolean iSMoreSearch = Boolean.FALSE;
	
	public SearchFilter() {
		setVehicleSortPattern(VehicleSortPattern.year_desc);
	}
	
	public Boolean noSearch() {
        if ( vehicleId == null
                && yearFrom == null
                && yearTo == null
                && fuelType == null
                && priceFrom == null
                && priceTo == null
                && mileageFrom == null
                && mileageTo == null
                && gearBoxType == null
                && engineSizeFrom == null
                && engineSizeTo == null
                && customerType == null
                && vehicleCondition == null
                && vehicleMake == null
                && vehicleModel == null
        		&& vehicleSortPattern ==null
        		&& pageIndex == null
        		&& isNew24 == null
        		&& isUpdated24 == null
                && bodyType == null)

            return true;
        else return false;
    }
	
	
	
	public Boolean getIsNew24() {
		return isNew24;
	}

	public void setIsNew24(Boolean isNew24) {
		this.isNew24 = isNew24;
	}

	public Boolean getIsUpdated24() {
		return isUpdated24;
	}

	public void setIsUpdated24(Boolean isUpdated24) {
		this.isUpdated24 = isUpdated24;
	}

	public Boolean getiSMoreSearch() {
		return iSMoreSearch;
	}

	public void setiSMoreSearch(Boolean iSMoreSearch) {
		this.iSMoreSearch = iSMoreSearch;
	}

	/**
	 * @return the vehicleId
	 */
	public String getVehicleId() {
		return vehicleId;
	}
	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	/**
	 * @return the yearFrom
	 */
	public Date getYearFrom() {
		return yearFrom;
	}
	/**
	 * @param yearFrom the yearFrom to set
	 */
	public void setYearFrom(Date yearFrom) {
		this.yearFrom = yearFrom;
	}
	/**
	 * @return the yearTo
	 */
	public Date getYearTo() {
		return yearTo;
	}
	/**
	 * @param yearTo the yearTo to set
	 */
	public void setYearTo(Date yearTo) {
		this.yearTo = yearTo;
	}
	/**
	 * @return the fuelType
	 */
	public ClassifierItem getFuelType() {
		return fuelType;
	}
	/**
	 * @param fuelType the fuelType to set
	 */
	public void setFuelType(ClassifierItem fuelType) {
		this.fuelType = fuelType;
	}
	/**
	 * @return the priceFrom
	 */
	public Double getPriceFrom() {
		return priceFrom;
	}
	/**
	 * @param priceFrom the priceFrom to set
	 */
	public void setPriceFrom(Double priceFrom) {
		this.priceFrom = priceFrom;
	}
	/**
	 * @return the priceTo
	 */
	public Double getPriceTo() {
		return priceTo;
	}
	/**
	 * @param priceTo the priceTo to set
	 */
	public void setPriceTo(Double priceTo) {
		this.priceTo = priceTo;
	}
	/**
	 * @return the mileageFrom
	 */
	public Integer getMileageFrom() {
		return mileageFrom;
	}
	/**
	 * @param mileageFrom the mileageFrom to set
	 */
	public void setMileageFrom(Integer mileageFrom) {
		this.mileageFrom = mileageFrom;
	}
	/**
	 * @return the mileageTo
	 */
	public Integer getMileageTo() {
		return mileageTo;
	}
	/**
	 * @param mileageTo the mileageTo to set
	 */
	public void setMileageTo(Integer mileageTo) {
		this.mileageTo = mileageTo;
	}
	/**
	 * @return the gearBoxType
	 */
	public ClassifierItem getGearBoxType() {
		return gearBoxType;
	}
	/**
	 * @param gearBoxType the gearBoxType to set
	 */
	public void setGearBoxType(ClassifierItem gearBoxType) {
		this.gearBoxType = gearBoxType;
	}
//	/**
//	 * @return the doorCount
//	 */
//	public Integer getDoorCount() {
//		return doorCount;
//	}
//	
//	
//	/**
//	 * @param doorCount the doorCount to set
//	 */
//	public void setDoorCount(Integer doorCount) {
//		this.doorCount = doorCount;
//	}
//	/**
//	 * @return the color
//	 */
//	public ClassifierItem getColor() {
//		return color;
//	}
//	/**
//	 * @param color the color to set
//	 */
//	public void setColor(ClassifierItem color) {
//		this.color = color;
//	}
//	/**
//	 * @return the colorType
//	 */
//	public ClassifierItem getColorType() {
//		return colorType;
//	}
//	/**
//	 * @param colorType the colorType to set
//	 */
//	public void setColorType(ClassifierItem colorType) {
//		this.colorType = colorType;
//	}
	/**
	
	/**
	 * @return the customerType
	 */
	public UserType getCustomerType() {
		return customerType;
	}
	/**
	 * @return the engineSizeFrom
	 */
	public Double getEngineSizeFrom() {
		return engineSizeFrom;
	}

	/**
	 * @param engineSizeFrom the engineSizeFrom to set
	 */
	public void setEngineSizeFrom(Double engineSizeFrom) {
		this.engineSizeFrom = engineSizeFrom;
	}

	/**
	 * @return the engineSizeTo
	 */
	public Double getEngineSizeTo() {
		return engineSizeTo;
	}

	/**
	 * @param engineSizeTo the engineSizeTo to set
	 */
	public void setEngineSizeTo(Double engineSizeTo) {
		this.engineSizeTo = engineSizeTo;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(UserType customerType) {
		this.customerType = customerType;
	}
	/**
	 * @return the transmission
	 */
//	public String getTransmission() {
//		return transmission;
//	}
//	/**
//	 * @param transmission the transmission to set
//	 */
//	public void setTransmission(String transmission) {
//		this.transmission = transmission;
//	}
	/**
	 * @return the vehicleCondition
	 */
	public VehicleCondition getVehicleCondition() {
		return vehicleCondition;
	}
	/**
	 * @param vehicleCondition the vehicleCondition to set
	 */
	public void setVehicleCondition(VehicleCondition vehicleCondition) {
		this.vehicleCondition = vehicleCondition;
	}
	/**
	 * @return the vehicleMake
	 */
	public VehicleMake getVehicleMake() {
		return vehicleMake;
	}
	/**
	 * @param vehicleMake the vehicleMake to set
	 */
	public void setVehicleMake(VehicleMake vehicleMake) {
		this.vehicleMake = vehicleMake;
	}
	/**
	 * @return the vehicleModel
	 */
	public VehicleModel getVehicleModel() {
		return vehicleModel;
	}
	/**
	 * @param vehicleModel the vehicleModel to set
	 */
	public void setVehicleModel(VehicleModel vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	/**
	 * @return the bodyType
	 */
	public ClassifierItem getBodyType() {
		return bodyType;
	}
	/**
	 * @param bodyType the bodyType to set
	 */
	public void setBodyType(ClassifierItem bodyType) {
		this.bodyType = bodyType;
	}

	/**
	 * @return the vehicleSortPattern
	 */
	public VehicleSortPattern getVehicleSortPattern() {
		return vehicleSortPattern;
	}

	/**
	 * @param vehicleSortPattern the vehicleSortPattern to set
	 */
	public void setVehicleSortPattern(VehicleSortPattern vehicleSortPattern) {
		this.vehicleSortPattern = vehicleSortPattern;
	}

	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		return pageIndex;
	}

	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	
	
	

}
