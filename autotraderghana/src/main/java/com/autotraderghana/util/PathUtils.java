package com.autotraderghana.util;

import org.apache.commons.lang.StringUtils;

public class PathUtils {
	private static final String REGEX_DELIM_UNDER = "-";
	private static final String REGEX_DELIM_SPACE = " ";
	
	public static String encode(String path) {
		if (StringUtils.isEmpty(path)) {
			return null;
		}
		path = path.trim();
		path = TransliterateUtils.transliterate(path);
		path = path.replaceAll("[^a-zA-Z0-9\\s-]", "");
		path = path.replaceAll(REGEX_DELIM_SPACE, REGEX_DELIM_UNDER);
		return path.replaceAll("---", "-");		
	}
}
