package com.autotraderghana.service;

import java.util.List;

import com.autotraderghana.model.User;

public interface DealerService {

	public List<User> retrieveAllVehiclesDealers(int page);
	
	public Long countAllVehiclesDealers();
}
