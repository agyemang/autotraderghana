package com.autotraderghana.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehiclePaymentInfo;
import com.autotraderghana.repository.VehiclePaymentInfoRepository;
import com.autotraderghana.service.VehiclePaymentInfoService;

@Service
public class RepositoryVehiclePaymentInfoImpl implements
		VehiclePaymentInfoService {
	
	@Resource
	protected VehiclePaymentInfoRepository repository;

	@Override
	public VehiclePaymentInfo add(VehiclePaymentInfo paymentInfo) {
		return repository.save(paymentInfo);
	}

	@Override
	public List<VehiclePaymentInfo> findAll() {
		return repository.findAll();
	}

	@Override
	public List<VehiclePaymentInfo> findAllForPage(int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VehiclePaymentInfo findById(Long id) {
		return repository.findOne(id);
	}

	@Override
	public VehiclePaymentInfo update(VehiclePaymentInfo updated) {
		// TODO Auto-generated method stub
		return null;
	}

}
