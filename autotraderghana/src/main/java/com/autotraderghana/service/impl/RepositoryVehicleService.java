package com.autotraderghana.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autotraderghana.customFacade.GlobalCounterService;
import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.repository.VehicleMakeRepository;
import com.autotraderghana.repository.VehicleModelRepository;
import com.autotraderghana.repository.VehicleRepository;
import com.autotraderghana.service.VehicleNotFoundException;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.util.SearchFilterResponse;

@Service
public class RepositoryVehicleService implements VehicleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryVehicleService.class);

	protected static final int NUMBER_OF_VEHICLES_PER_PAGE = 10;
	 
	@Resource
	private VehicleRepository vehicleRepository;

	@Resource
	protected GlobalCounterService counterService;
	
	@Resource 
	private VehicleMakeRepository makeRepository;
	    
	@Resource 
	private VehicleModelRepository modelRepository;

	@Transactional
	@Override
	public Vehicle add(Vehicle vehicle) {
		LOGGER.debug("Creating a new vehicle with information: " + vehicle);
		vehicle.setVehicleId(counterService.getNextVehicleNumber());
		return vehicleRepository.save(vehicle);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Vehicle> findAll() {
		LOGGER.debug("Finding all vehicles");
		return vehicleRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Vehicle> findAllForPage(int pageIndex, int pageSize) {
		LOGGER.debug("Finding all vehicles fro page");
		return vehicleRepository.findAllForPage(pageIndex, pageSize);
	}

	@Transactional(readOnly = true)
	@Override
	public Vehicle findById(String vehicleId) throws VehicleNotFoundException {
		
		LOGGER.debug("Finding vehicle by id: " + vehicleId);
		
		Vehicle found = vehicleRepository.findOne(vehicleId);
		
		if (found == null && !StringUtils.equals(vehicleId, "NEW-REG")) {
			throw new VehicleNotFoundException("No vehicle found with id: "+ vehicleId);
		}
		return found;
	}
	
	@Transactional(rollbackFor = VehicleNotFoundException.class)
	@Override
	public Vehicle update(Vehicle vehicle) throws VehicleNotFoundException {
		LOGGER.debug("Updating vehicle with id: " + vehicle);
		
		Vehicle updated = vehicleRepository.findByVehicleIdAndStatusNot(vehicle.getVehicleId(), StatusEnum.DELETED);;
		if (updated == null) {
			LOGGER.debug("No vehicle found with id: " + vehicle.getVehicleId());
			throw new VehicleNotFoundException("No vehicle found with id: "+ vehicle.getVehicleId());
		}
        vehicleRepository.save(vehicle);
        return updated;
		
	}
	
	@Transactional(rollbackFor = VehicleNotFoundException.class)
	@Override
	public Vehicle delete(String vehicleId) throws VehicleNotFoundException {
		LOGGER.debug("Deleting vehicle with id: " + vehicleId);
		
		Vehicle deleted = vehicleRepository.findOne(vehicleId);
		if (deleted == null) {
			LOGGER.debug("No vehicle found with id: " + vehicleId);
			throw new VehicleNotFoundException("No vehicle found with id: "+ vehicleId);
		}
        
        vehicleRepository.delete(deleted);
        return deleted;
	}

	@Transactional(rollbackFor = VehicleNotFoundException.class)
	@Override
	public Vehicle markForDelete(String vehicleId) throws VehicleNotFoundException {
		LOGGER.debug("Marking vehicle with id: " + vehicleId + " for deletion");
		
		Vehicle markForDelete = vehicleRepository.findByVehicleIdAndStatusNot(vehicleId, StatusEnum.DELETED);
		
		if (markForDelete == null) {
			LOGGER.debug("No vehicle found with id: " + vehicleId);
			throw new VehicleNotFoundException("No vehicle found with id: "+ vehicleId);
		}
		markForDelete.setStatus(StatusEnum.DELETED);
        vehicleRepository.save(markForDelete);
        return markForDelete;
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<VehicleModel> retrieveVehicleModelsByMakeId(Long makeId) {
		LOGGER.debug("Retrieving all vehicleModels with makeId: " + makeId);
		return modelRepository.findByVehicleMakeIdOrderByModelAsc(makeId);
	}

	@Transactional(readOnly = true)
	@Override
	public List<VehicleMake> retrieveAllVehicleMake() {
		LOGGER.debug("Retrieving all vehicleMakes ");
		return makeRepository.findAll(new Sort(Sort.Direction.ASC, "make"));
	}

	@Transactional(readOnly = true)
	@Override
	public long countVehicle(SearchFilterResponse searchFilter) {
		return vehicleRepository.findVehicleCount(searchFilter);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Vehicle> search(SearchFilterResponse searchDTO) {
		return vehicleRepository.findVehiclesForPage(searchDTO, searchDTO.getPageIndex(), NUMBER_OF_VEHICLES_PER_PAGE);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Vehicle> retrieveCustomerArchivedVehicles(Long userId) {
		LOGGER.debug("Retrieving all archived vehicles for user with id "+userId);
		return null;
	}

	@Transactional(readOnly = true)
	@Override
	public List<Vehicle> retrieveCustomerLiveVehicles(Long userId) {
		LOGGER.debug("Retrieving all live vehicles for user with id "+userId);
		return null;
	}

	@Override
	public int retrieveTotalVehicleCount(VehicleCondition condition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Vehicle> retrieveAllVehiclesByDealer(Long userId, int page) {
		// TODO Auto-generated method stub
		return vehicleRepository.findDealerVehicles(userId, StatusEnum.LIVE, constructPageSpecification(page, 10, sortByVehicleMakeAsc()));
	}

	@Override
	public Long retrieveAllVehiclesByDealerCount(Long userId,StatusEnum statusEnum) {
		// TODO Auto-generated method stub
		return vehicleRepository.countByCustomerIdAndStatus(userId,statusEnum);
	}

	

    /**
     * Returns a Sort object which sorts persons in ascending order by using the last name.
     * @return
     */
    private Sort sortByVehicleMakeAsc() {
        return new Sort(Sort.Direction.ASC, "vehicleMake.make");
    }
    
    private Pageable constructPageSpecification(int pageIndex, int pageSize, Sort sort) {
        Pageable pageSpecification = new PageRequest(pageIndex, pageSize, sort);
        return pageSpecification;
    }
	

}
