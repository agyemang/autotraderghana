package com.autotraderghana.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.autotraderghana.model.VehiclePostPrice;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.repository.VehiclePricePostRepository;
import com.autotraderghana.service.VehiclePricePostService;


@Service
public class RepositoryPostPriceImpl implements VehiclePricePostService {

	@Resource
	private VehiclePricePostRepository repository;
	
	@Override
	public VehiclePostPrice save(VehiclePostPrice postPrice) {
		return repository.save(postPrice);
	}

	@Override
	public List<VehiclePostPrice> findAll() {
		return repository.findAll();
	}

	@Override
	public VehiclePostPrice findVehiclePostPricePrivate() {
		return repository.findByUserType(UserType.PRIVATE_SELLER);
	}

	@Override
	public VehiclePostPrice findVehiclePostPriceDealer() {
		return repository.findByUserType(UserType.CAR_DEALER);
	}

	@Override
	public VehiclePostPrice findById(Long id) {
		return repository.findOne(id);
	}


}
