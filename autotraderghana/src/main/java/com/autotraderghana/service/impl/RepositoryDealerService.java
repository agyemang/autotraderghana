package com.autotraderghana.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.autotraderghana.model.User;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleSortPattern;
import com.autotraderghana.repository.VehicleDealerRepository;
import com.autotraderghana.service.DealerService;

@Service
public class RepositoryDealerService implements DealerService {
	
	 protected static final int NUMBER_OF_PERSONS_PER_PAGE = 10;
	
	@Resource
	private VehicleDealerRepository repository;

	@Transactional(readOnly = true)
	@Override
	public List<User> retrieveAllVehiclesDealers(int page) {
		return repository.findUserType(UserType.CAR_DEALER, constructPageSpecification(page));
	}

	/**
     * Returns a new object which specifies the the wanted result page.
     * @param pageIndex The index of the wanted result page
     * @return
     */
    private Pageable constructPageSpecification(int pageIndex) {
        Pageable pageSpecification = new PageRequest(pageIndex, NUMBER_OF_PERSONS_PER_PAGE, sortByLastNameAsc());
        return pageSpecification;
    }

    /**
     * Returns a Sort object which sorts persons in ascending order by using the last name.
     * @return
     */
    private Sort sortByLastNameAsc() {
        return new Sort(Sort.Direction.ASC, "tradingName");
    }

	@Override
	public Long countAllVehiclesDealers() {
		// TODO Auto-generated method stub
		return repository.findUserTypeCount(UserType.CAR_DEALER);
	}
}
