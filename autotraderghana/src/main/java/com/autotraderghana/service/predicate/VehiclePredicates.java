package com.autotraderghana.service.predicate;



import com.autotraderghana.model.QVehicle;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.util.SearchFilterResponse;
import com.mysema.query.BooleanBuilder;

public class VehiclePredicates {
	
	public BooleanBuilder build(SearchFilterResponse searchFilter) {
		BooleanBuilder result = null;
		
//		BooleanBuilder vehicleIdExpression = CommonPredicates.stringIsLike(QVehicle.vehicle.vehicleId, searchFilter.getVehicleId(), false);
		BooleanBuilder vehicleMakeExpression = CommonPredicates.vehicleMakeEquals(QVehicle.vehicle.vehicleMake, searchFilter.getVehicleMake(), false);
		BooleanBuilder vehicleModelExpression = CommonPredicates.vehicleModelEquals(QVehicle.vehicle.vehicleModel, searchFilter.getVehicleModel(), false);
		BooleanBuilder vehicleBodyTypeExpression = CommonPredicates.classifierItemEquals(QVehicle.vehicle.bodyType, searchFilter.getBodyType(), false);
		BooleanBuilder vehicleFuelTypeExpression = CommonPredicates.classifierItemEquals(QVehicle.vehicle.fuelType, searchFilter.getFuelType(), false);
		BooleanBuilder vehicleGearBoxTypeExpression = CommonPredicates.classifierItemEquals(QVehicle.vehicle.gearBoxType, searchFilter.getGearBoxType(), false);
		BooleanBuilder vehicleYearExpression = CommonPredicates.vehicleYearBetween(QVehicle.vehicle.year, searchFilter.getYearFrom(), searchFilter.getYearTo(), false);
		BooleanBuilder vehiclePriceExpression = CommonPredicates.vehiclePriceBetween(QVehicle.vehicle.price, searchFilter.getPriceFrom(), searchFilter.getPriceTo(), false);
		BooleanBuilder vehicleMileageExpression = CommonPredicates.vehicleMileageBetween(QVehicle.vehicle.mileage, searchFilter.getMileageFrom(), searchFilter.getMileageTo(), false);
		BooleanBuilder vehicleEngineSizeExpression = CommonPredicates.vehicleEngineSizeBetween(QVehicle.vehicle.engineSize, searchFilter.getEngineSizeFrom(), searchFilter.getEngineSizeTo(), false);
		BooleanBuilder vehicleConditionExpression = CommonPredicates.vehicleConditionEquals(QVehicle.vehicle.vehicleCondition, searchFilter.getVehicleCondition(), false);
		BooleanBuilder vehicleSellerTypeExpression = CommonPredicates.vehicleSellerTypeEquals(QVehicle.vehicle.customer.userType, searchFilter.getCustomerType(), false);
		BooleanBuilder vehicleStatusExpression = CommonPredicates.statusIsEqual(QVehicle.vehicle.status, StatusEnum.LIVE, false);
		
		BooleanBuilder vehicleIsNewWithin24 = null;
		if(searchFilter.getIsNew24().equals(Boolean.TRUE)){
			vehicleIsNewWithin24 = CommonPredicates.vehicleIsNewWithin24(QVehicle.vehicle.createDate,QVehicle.vehicle.updateDate, false);
		}
		BooleanBuilder vehicleIsUpdatedWithin24=null;
		if(searchFilter.getIsUpdated24().equals(Boolean.TRUE)){
			vehicleIsUpdatedWithin24 = CommonPredicates.vehicleIsUpdatedWithin24(QVehicle.vehicle.createDate, QVehicle.vehicle.updateDate, false);
		}
		

		
		if(vehicleMakeExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleMakeExpression);
		}
		if(vehicleModelExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleModelExpression);
		}
		
		if(vehicleIsNewWithin24 != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleIsNewWithin24);
		}
		
		if(vehicleIsUpdatedWithin24 != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleIsUpdatedWithin24);
		}	
		
		if(vehicleBodyTypeExpression != null){
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleBodyTypeExpression);
		}
		
		if(vehicleFuelTypeExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleFuelTypeExpression);
		}		
		if(vehicleGearBoxTypeExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleGearBoxTypeExpression);
		}	

		if(vehicleYearExpression != null){
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleYearExpression);
		}		
		if(vehiclePriceExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehiclePriceExpression);
		}		
		if(vehicleMileageExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleMileageExpression);
		}		
		if(vehicleEngineSizeExpression != null)  {
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleEngineSizeExpression);
		}	
		if(vehicleConditionExpression != null){
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleConditionExpression);
		}		
		if(vehicleSellerTypeExpression != null){
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleSellerTypeExpression);
		}
		if(vehicleStatusExpression != null){
			if(result == null)
				result = new BooleanBuilder();
			result.and(vehicleStatusExpression);
		}
		
		return result;
	}

}
