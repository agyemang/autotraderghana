package com.autotraderghana.service.predicate;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.QClassifierItem;
import com.autotraderghana.model.QUser;
import com.autotraderghana.model.QVehicleMake;
import com.autotraderghana.model.QVehicleModel;
import com.autotraderghana.model.User;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;
import com.autotraderghana.model.enums.VehicleCondition;
import com.mysema.commons.lang.Pair;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.annotations.QueryDelegate;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EnumPath;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

public class CommonPredicates {

	
	
	public static BooleanBuilder vehicleMakeEquals(final QVehicleMake qVehicleMake,final Long vehicleMake,  boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (vehicleMake != null && qVehicleMake != null) {
			if (andOr)
				return builder.and(qVehicleMake.id.eq(vehicleMake));
			else
				return builder.or(qVehicleMake.id.eq(vehicleMake));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}

	public static BooleanBuilder vehicleModelEquals(final QVehicleModel qVehicleModel,final Long vehicleModel,  boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (vehicleModel != null && qVehicleModel != null) {
			if (andOr)
				return builder.and(qVehicleModel.id.eq(vehicleModel));
			else
				return builder.or(qVehicleModel.id.eq(vehicleModel));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}

	public static BooleanBuilder stringIsLike(StringPath path, final String searchTerm, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (path != null && searchTerm != null) {
			if (andOr)
				return builder.and(path.toLowerCase().contains(searchTerm.toLowerCase()));
			else
				return builder.or(path.toLowerCase().contains(searchTerm.toLowerCase()));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}

	public static BooleanBuilder statusIsEqual(EnumPath<StatusEnum> path, final StatusEnum searchTerm, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (path != null && searchTerm != null) {
			if (andOr) {
				return builder.and(path.eq(searchTerm));
			} else {
				return builder.or(path.eq(searchTerm));
			}
		}
		if (builder.hasValue()) {
			return builder;
		}
		return null;
	}
	
	public static BooleanBuilder userTypeEquals(EnumPath<UserType> path, final UserType searchTerm, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (path != null && searchTerm != null) {
			if (andOr) {
				return builder.and(path.eq(searchTerm));
			} else {
				return builder.or(path.eq(searchTerm));
			}
		}
		if (builder.hasValue()) {
			return builder;
		}
		return null;
	}
	
	public static BooleanBuilder vehicleConditionEquals(EnumPath<VehicleCondition> path, final VehicleCondition searchTerm, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (path != null && searchTerm != null) {
			if (andOr) {
				return builder.and(path.eq(searchTerm));
			} else {
				return builder.or(path.eq(searchTerm));
			}
		}
		if (builder.hasValue()) {
			return builder;
		}
		return null;
	}
	
	public static BooleanBuilder vehicleSellerTypeEquals(EnumPath<UserType> path, final UserType searchTerm, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (path != null && searchTerm != null) {
			if (andOr) {
				return builder.and(path.eq(searchTerm));
			} else {
				return builder.or(path.eq(searchTerm));
			}
		}
		if (builder.hasValue()) {
			return builder;
		}
		return null;
	}
	
	public static BooleanBuilder vehicleSellerTypeEquals( final QUser qCustomer,final User customer, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (customer != null && qCustomer != null) {
			if (andOr)
				return builder.and(qCustomer.userType.eq(customer.getUserType()));
			else
				return builder.or(qCustomer.userType.eq(customer.getUserType()));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}

	public static BooleanBuilder classifierItemEquals(final QClassifierItem qClassifierItem,final Long classifierItem,  boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (classifierItem != null && qClassifierItem != null) {
			if (andOr)
				return builder.and(qClassifierItem.id.eq(classifierItem));
			else
				return builder.or(qClassifierItem.id.eq(classifierItem));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}

	public static BooleanBuilder vehicleYearBetween(DateTimePath<Date> year, final Date searchTerm1, final Date searchTerm2, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (year != null && searchTerm1 != null && searchTerm2 != null) {
			if (andOr) {
				return builder.and(year.between(searchTerm1, searchTerm2));
			} else
				return builder.or(year.between(searchTerm1, searchTerm2));
		}else if(year != null && (searchTerm1 !=null || searchTerm2 != null)) {
			if (andOr) {
				return builder.and(year.between(searchTerm1, searchTerm2));
			} else
				return builder.or(year.between(searchTerm1, searchTerm2));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}
	
	public static BooleanBuilder vehicleIsNewWithin24(DateTimePath<Date> createDate,DateTimePath<Date> updateDate, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, -24);
		Date dateToCompare = c.getTime();
	    
	    if (createDate != null && dateToCompare != null ) {
			if (andOr) {
				return builder.and(createDate.eq(updateDate)).and(createDate.goe(dateToCompare).and(createDate.loe(Calendar.getInstance().getTime())));
			} else
				return builder.or(createDate.eq(updateDate)).and(createDate.goe(dateToCompare).and(createDate.loe(Calendar.getInstance().getTime())));
		}
	    	
		if (builder.hasValue())
			return builder;
		return null;

	}
	public static BooleanBuilder vehicleIsUpdatedWithin24(DateTimePath<Date> createDate, DateTimePath<Date> updateDate, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		
		
		
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, -24);
		Date dateToCompare = c.getTime();
		
		if (createDate != null && createDate != null && dateToCompare != null) {
			if (andOr) {
				return builder.and(updateDate.gt(createDate)).and(updateDate.goe(dateToCompare).and(updateDate.loe(Calendar.getInstance().getTime())));
			} else
				return builder.or(updateDate.gt(createDate)).and(updateDate.goe(dateToCompare).and(updateDate.loe(Calendar.getInstance().getTime())));
		}
		if (builder.hasValue())
			return builder;
		return null;

	}
	
	public static BooleanBuilder vehiclePriceBetween(NumberPath<Double> price, final Double searchTerm1, final Double searchTerm2, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (price != null && searchTerm1 != null && searchTerm2 != null) {
			if (andOr) {
				return builder.and(price.between(searchTerm1, searchTerm2));
			} else
				return builder.or(price.between(searchTerm1, searchTerm2));
		}else if(price != null && (searchTerm1 !=null || searchTerm2 != null)) {
			if (andOr) {
				return builder.and(price.between(searchTerm1, searchTerm2));
			} else
				return builder.or(price.between(searchTerm1, searchTerm2));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}
	
	public static BooleanBuilder vehicleMileageBetween(NumberPath<Integer> mileage, final Integer searchTerm1, final Integer searchTerm2, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (mileage != null && searchTerm1 != null && searchTerm2 != null) {
			if (andOr) {
				return builder.and(mileage.between(searchTerm1, searchTerm2));
			} else
				return builder.or(mileage.between(searchTerm1, searchTerm2));
		}else if(mileage != null && (searchTerm1 !=null || searchTerm2 != null)) {
			if (andOr) {
				return builder.and(mileage.between(searchTerm1, searchTerm2));
			} else
				return builder.or(mileage.between(searchTerm1, searchTerm2));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}
	public static BooleanBuilder vehicleEngineSizeBetween(NumberPath<Double> engineSize, final Double searchTerm1, final Double searchTerm2, boolean andOr) {
		BooleanBuilder builder = new BooleanBuilder();
		if (engineSize != null && searchTerm1 != null && searchTerm2 != null) {
			if (andOr) {
				return builder.and(engineSize.between(searchTerm1, searchTerm2));
			} else
				return builder.or(engineSize.between(searchTerm1, searchTerm2));
		}else if(engineSize != null && (searchTerm1 !=null || searchTerm2 != null)) {
			if (andOr) {
				return builder.and(engineSize.between(searchTerm1, searchTerm2));
			} else
				return builder.or(engineSize.between(searchTerm1, searchTerm2));
		}
		if (builder.hasValue())
			return builder;
		return null;
	}

}
