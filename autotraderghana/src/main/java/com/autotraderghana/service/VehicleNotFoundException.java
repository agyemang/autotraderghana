package com.autotraderghana.service;

/**
 * This exception is thrown if the wanted vehicle is not found.
 * @author Kwabena Agyemang
 */
public class VehicleNotFoundException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5606799525960945676L;

	public VehicleNotFoundException(String msg) {
        super(msg);
    }

    public VehicleNotFoundException(String msg, Exception e) {
        super(msg + " because of " + e.toString());
    }
}

