package com.autotraderghana.service;


import java.util.List;

import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.util.SearchFilterResponse;

public interface VehicleService {
	 /**
     * Creates a new vehicle.
     * @param vehicle   The information of the created vehicle.
     * @return  The created vehicle.
     */
    public Vehicle add(Vehicle vehicle);
    
    /**
     * Finds all vehicle.
     * @return  A list of vehicles.
     */
    public List<Vehicle> findAll();
    
    
    /**
     * Finds all vehicles for page.
     * @return  A list of vehicles.
     */
    public List<Vehicle> findAllForPage(int pageIndex, int pageSize) ;

    /**
     * Finds vehicle by id.
     * @param id    The id of the wanted vehicle.
     * @return  The found vehicle. If no vehicle is found, this method returns null.
     */
    public Vehicle findById(String vehicleId) throws VehicleNotFoundException;
    
    

    /**
     * Updates the information of a vehicle.
     * @param updated   The information of the updated vehicle.
     * @return  The updated vehicle.
     * @throws VehicleNotFoundException  if no vehicle is found with given id.
     */
    public Vehicle update(Vehicle updated) throws VehicleNotFoundException;

    /**
     * Gets the count of vehicles matching with the given search term.
     * @param searchTerm
     * @return
     */
    public long countVehicle(SearchFilterResponse searchFilter);

    /**
     * Deletes a vehicle.
     * @param personId  The id of the deleted vehicle.
     * @return  The deleted vehicle.
     * @throws VehicleNotFoundException  if no vehicle is found with the given id.
     */
    public Vehicle delete(String vehicleId) throws VehicleNotFoundException;
    
    /**
     * sets a vehicle status as deleted.
     * @param personId  The id of the deleted vehicle.
     * @return  The deleted vehicle.
     * @throws VehicleNotFoundException  if no vehicle is found with the given id.
     */
    public Vehicle markForDelete(String vehicleId) throws VehicleNotFoundException;

   
   
    
   
    /**
     * Searches persons for a given page by using the given search term.
     * @param searchTerm
     * @param pageIndex
     * @return  A list of vehicles whose last name begins with the given search term and who are belonging to the given page.
     *          If no vehicles is found, this method returns an empty list. This search is case insensitive.
     */
    public List<Vehicle> search(SearchFilterResponse searchFilter);

    
    
    public List<VehicleModel> retrieveVehicleModelsByMakeId(Long makeId);
	
	public List<VehicleMake> retrieveAllVehicleMake(); 
	
	public List<Vehicle> retrieveCustomerArchivedVehicles(Long userId);
	
	public List<Vehicle> retrieveCustomerLiveVehicles(Long userId);
	
	public int retrieveTotalVehicleCount(VehicleCondition condition);
	
	public List<Vehicle> retrieveAllVehiclesByDealer(Long userId, int page);
	
	public Long retrieveAllVehiclesByDealerCount(Long userId,StatusEnum statusEnum);
	
	
}
