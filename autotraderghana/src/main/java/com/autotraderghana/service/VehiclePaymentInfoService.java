package com.autotraderghana.service;


import java.util.List;

import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.VehiclePaymentInfo;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.util.SearchFilterResponse;

public interface VehiclePaymentInfoService {

    public VehiclePaymentInfo add(VehiclePaymentInfo paymentInfo);
    
    public List<VehiclePaymentInfo> findAll();
     
    public List<VehiclePaymentInfo> findAllForPage(int pageIndex, int pageSize) ;

    public VehiclePaymentInfo findById(Long id);
    
    public VehiclePaymentInfo update(VehiclePaymentInfo updated) ;

   
}
