package com.autotraderghana.service;


import java.util.List;

import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.model.VehiclePostPrice;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.VehicleCondition;
import com.autotraderghana.util.SearchFilter;
import com.autotraderghana.util.SearchFilterResponse;

public interface VehiclePricePostService {

    public VehiclePostPrice save(VehiclePostPrice postPrice);
    
    public List<VehiclePostPrice> findAll();
    
    public VehiclePostPrice findVehiclePostPricePrivate();
    
    public VehiclePostPrice findVehiclePostPriceDealer();

    public VehiclePostPrice findById(Long id);
    

	
}
