package com.autotraderghana.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;

public interface UserRepository extends JpaRepository<User, Long>{

	List<User> findByAccountExpiredTrue();

	List<User> findByAccountLockedTrue();

	List<User> findByCredentialsExpiredTrue();
	
	List<User> findByEnabledTrue();	

}
