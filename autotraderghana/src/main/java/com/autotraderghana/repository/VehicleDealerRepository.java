package com.autotraderghana.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.autotraderghana.model.User;
import com.autotraderghana.model.enums.UserType;

public interface VehicleDealerRepository extends JpaRepository<User, Long> {

	

	@Query("SELECT u FROM User u WHERE u.userType = :userType AND u.enabled = TRUE AND u.accountExpired = FALSE AND u.accountLocked = FALSE AND u.credentialsExpired = FALSE ")
	List<User> findUserType(@Param("userType") UserType userType, Pageable pageable);

	@Query("SELECT count(u)  FROM User u WHERE u.userType = :userType AND u.enabled = TRUE AND u.accountExpired = FALSE AND u.accountLocked = FALSE AND u.credentialsExpired = FALSE ")
	Long findUserTypeCount(@Param("userType") UserType userType);
	
	

}
