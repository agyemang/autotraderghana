package com.autotraderghana.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.autotraderghana.model.VehicleMake;

public interface VehicleMakeRepository extends JpaRepository<VehicleMake, Long> {

}
