package com.autotraderghana.repository;

import java.util.List;

import com.autotraderghana.model.Vehicle;
import com.autotraderghana.util.SearchFilterResponse;

public interface PaginatingVehicleRepository {

	public List<Vehicle> findAllForPage(int pageIndex, int pageSize);

	public List<Vehicle> findVehiclesForPage(SearchFilterResponse searchDTO, int pageIndex, int pageSize);
	
	public long findVehicleCount(SearchFilterResponse searchFilter);
	
}
