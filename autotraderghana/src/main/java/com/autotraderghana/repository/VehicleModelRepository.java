package com.autotraderghana.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.autotraderghana.model.VehicleModel;

public interface VehicleModelRepository extends JpaRepository<VehicleModel, Long> {

	List<VehicleModel> findByVehicleMakeIdOrderByModelAsc(Long makeId);

}
