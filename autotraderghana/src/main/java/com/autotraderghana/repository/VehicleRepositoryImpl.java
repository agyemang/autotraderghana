package com.autotraderghana.repository;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaMetamodelEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;

import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.enums.VehicleSortPattern;
import com.autotraderghana.service.predicate.VehiclePredicates;
import com.autotraderghana.util.SearchFilterResponse;
import com.mysema.query.BooleanBuilder;

public class VehicleRepositoryImpl implements PaginatingVehicleRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VehicleRepositoryImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	private QueryDslJpaRepository<Vehicle, String> repository;

	@PostConstruct
	public void init() {
		JpaEntityInformation<Vehicle, String> vehicleEntityInfo = new JpaMetamodelEntityInformation<Vehicle, String>(Vehicle.class, entityManager.getMetamodel());
		repository = new QueryDslJpaRepository<Vehicle, String>(vehicleEntityInfo, entityManager);
	}

	@Override
	public List<Vehicle> findAllForPage(int pageIndex, int pageSize) {
		Pageable pageSpec = buildPageSpecification(pageIndex, pageSize);
		Page<Vehicle> wanted = repository.findAll(pageSpec);
		return wanted.getContent();
	}

	@Override
	public List<Vehicle> findVehiclesForPage(SearchFilterResponse searchDTO,
			int pageIndex, int pageSize) {
		LOGGER.debug("Finding vehicles for page " + searchDTO.getPageIndex() + " with search term: " + searchDTO);

        BooleanBuilder result = new VehiclePredicates().build(searchDTO);
        //Passes the predicate and the page specification to the repository
        Page<Vehicle> requestedPage =  repository.findAll(result, constructPageSpecification(pageIndex,pageSize,searchDTO.getVehicleSortPattern()));
        return requestedPage.getContent();
	}
	

	@Override
	public long findVehicleCount(SearchFilterResponse searchDTO) {
		LOGGER.debug("Finding vehicle count with search term: " + searchDTO);
        BooleanBuilder result = new VehiclePredicates().build(searchDTO);
        //Passes the predicate to the repository
        return repository.count(result);
	}
	
	
	private Pageable buildPageSpecification(int pageIndex, int pageSize) {
		return new PageRequest(pageIndex, pageSize, sortBySortPattern(VehicleSortPattern.price_asc));
	}
	
	/**
     * Returns a new object which specifies the the wanted result page.
     * @param pageIndex The index of the wanted result page
     * @return
     */
    private Pageable constructPageSpecification(int pageIndex, int pageSize,VehicleSortPattern sortPattern) {
        Pageable pageSpecification = new PageRequest(pageIndex, pageSize, sortBySortPattern(sortPattern));
        return pageSpecification;
    }

    /**
     * Returns a Sort object which sorts persons in ascending order by using the last name.
     * @return
     */
    private Sort sortBySortPattern(VehicleSortPattern sortPattern) {
    	
		switch (sortPattern) {
		case mileage_asc:
			return new Sort(Sort.Direction.ASC, "mileage");
		case mileage_desc:
			return new Sort(Sort.Direction.DESC, "mileage");
		case price_asc:
			return new Sort(Sort.Direction.ASC, "price");
		case price_desc:
			return new Sort(Sort.Direction.DESC, "price");
		case year_asc:
			return new Sort(Sort.Direction.ASC, "year");
		case year_desc:
			return new Sort(Sort.Direction.DESC, "year");
		default:
			return new Sort(Sort.Direction.ASC, "price");
		}

    }

}
