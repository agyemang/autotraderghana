package com.autotraderghana.repository;


import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.autotraderghana.model.User;
import com.autotraderghana.model.Vehicle;
import com.autotraderghana.model.enums.StatusEnum;
import com.autotraderghana.model.enums.UserType;

public interface VehicleRepository extends JpaRepository<Vehicle, String>, PaginatingVehicleRepository{

	Vehicle findByVehicleIdAndStatusNot(String vehicleId, StatusEnum deleted);
	
	@Query("SELECT count(v) FROM Vehicle v WHERE v.customer.id =:customerId AND v.status =:statusEnum")
	Long countByCustomerIdAndStatus(@Param("customerId")Long customerId,@Param("statusEnum") StatusEnum statusEnum);
	
	@Query("SELECT v FROM Vehicle v WHERE v.customer.id = :dealerId AND v.status =:statusEnum")
	List<Vehicle> findDealerVehicles(@Param("dealerId") Long dealerId,@Param("statusEnum") StatusEnum statusEnum, Pageable pageable);

}
