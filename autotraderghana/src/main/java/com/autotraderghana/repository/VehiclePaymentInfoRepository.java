package com.autotraderghana.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.autotraderghana.model.VehiclePaymentInfo;

public interface VehiclePaymentInfoRepository extends JpaRepository<VehiclePaymentInfo, Long> {

}
