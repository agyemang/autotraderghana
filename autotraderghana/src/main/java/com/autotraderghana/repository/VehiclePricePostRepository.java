package com.autotraderghana.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.autotraderghana.model.VehiclePostPrice;
import com.autotraderghana.model.enums.UserType;

public interface VehiclePricePostRepository extends JpaRepository<VehiclePostPrice, Long>{

	VehiclePostPrice findByUserType(UserType userType);

	

}
