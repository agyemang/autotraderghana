package com.autotraderghana.databaseInitializer;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.autotraderghana.customFacade.BaseFacade;
import com.autotraderghana.customFacade.ClassifierManager;
import com.autotraderghana.model.Classifier;
import com.autotraderghana.model.ClassifierItem;
import com.autotraderghana.model.ClassifierType;
import com.autotraderghana.model.VehicleMake;
import com.autotraderghana.model.VehicleModel;
import com.autotraderghana.service.VehicleService;
import com.autotraderghana.webapp.util.ClassifierTypeFilter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = {"classpath:/applicationContext-resources.xml",
                "classpath:/applicationContext-dao.xml",
                "classpath:/applicationContext-service.xml",
                "classpath*:/applicationContext.xml",
                "/WEB-INF/applicationContext*.xml",
                "/WEB-INF/dispatcher-servlet.xml"})
public class DatabaseInitializer {

	@Autowired
	private ClassifierManager classifierFacade;
	@Autowired
	private BaseFacade baseFacade;
	
	@Autowired
	private VehicleService vehicleManager;
	
	
	protected final transient static Log log = LogFactory.getLog(DatabaseInitializer.class);
    
	
	@Test
	public void addInitialDataBeforeTest() throws IOException {
//		initClassifiers();
//		initClassifierItem();
//		initFindAllMakes();
		assertEquals(true, true);
	}
	
	private void initFindAllMakes() {
//		List<VehicleMake> vehicleMakes = vehicleManager.retrieveAllVehicleMake();
//		System.err.println(vehicleMakes.toString());
		List<ClassifierItem> classifierItems = classifierFacade.findItems(ClassifierType.CURRENCY);
		System.err.println(classifierItems.toString());
	}

	public void initClassifiers() {

//		classifierFacade.createClassifier(createClassifier("COLOR", "Color"));
//		classifierFacade.createClassifier(createClassifier("COLORTYPE","Color Type"));
//		classifierFacade.createClassifier(createClassifier("GEARBOXTYPE","Gearbox Type"));
//		classifierFacade.createClassifier(createClassifier("BODYTYPE","Body Type"));
//		classifierFacade.createClassifier(createClassifier("FUELTYPE","Fuel Type"));
//		classifierFacade.createClassifier(createClassifier("PRICEINFO","Asking price info"));
//		classifierFacade.createClassifier(createClassifier("TELEADDITIONALINFO", "Telephone Additional info"));
//		classifierFacade.createClassifier(createClassifier("VEHICLEACCESSORY", "Vehicle Accessory"));
//		classifierFacade.createClassifier(createClassifier(ClassifierType.CURRENCY, ClassifierTypeFilter.CURRENCY));
		

	}

	public void initClassifierItem() throws IOException {

//		GenerateColor();
//		GenerateColorType();
//		GenerateGearboxType();
//		GenerateBodyType();
//		GenerateFuelType();
//		GeneratePriceInfo();
//		GenerateTelephoneInfo();
//		GenerateVehicleAccessory();
//		GenerateVehicleAccessoryChild() ;
//		GenerateVehicleModelMakes();
//		GenerateCurrencies();
		

	}
	

	private void GenerateVehicleModelMakes() throws IOException {
		
		Map<String, String> map = new HashMap<String, String>();
		Map<String, List<String>> map2 = new HashMap<String, List<String>>();
		
		Properties props = new Properties();
		FileInputStream in = new FileInputStream("C:/devtools/autotrader/autotraderghana/src/main/java/com/autotraderghana/databaseInitializer/vehicleMakesAndModel.txt");
		props.load(in);
		in.close();
	

		for(Object object : props.keySet()) {
			String key = (String)object;
			map.put(key, (String)props.get(key));
		}
		
		System.out.println(map);
		
		props = new Properties();
		in = new FileInputStream("C:/devtools/autotrader/autotraderghana/src/main/java/com/autotraderghana/databaseInitializer/vehicleMakesAndModel.properties");
		props.load(in);
		in.close();
		
		Collection<Object> values =props.values();
		
		for(Object object : props.keySet()) {
			String key = (String)object;
			String[] makes = ((String)props.get(key)).split("\\,");
			map2.put(map.get(key),Arrays.asList(makes));
			
		}
		System.out.println(map2);

		for(String makeName : map2.keySet())  {
			
			VehicleMake vehicleMake = new VehicleMake();
			vehicleMake.setMake(makeName.trim());
			baseFacade.saveOrMerge(vehicleMake);
						
			for(String modelName : map2.get(makeName))  {
				
				VehicleModel vehicleModel = new VehicleModel();			
				vehicleModel.setModel(modelName.intern());
				vehicleModel.setVehicleMake(vehicleMake);				
				baseFacade.saveOrMerge(vehicleModel);
				
			}
			
		}
		
	}
	
	

	private void GenerateVehicleAccessoryChild() {
		final String[] SAFETY_TYPES = { "ABS Brakes", "Airbag and Comforts","Alarm System","Driving Assistant","Immobilizer","Xenon Light","Traction Control","Fog Lights" };
		final String[] INTERIOR_TYPES = { "Airconditioning", "Central Locking","Convertible Roof Mechanism","Cruise Control","Electric Mirrors","Parking distance control",
				"Power Windows","Satellite Navigator","Stereo","Leather Upholstery","On Board Computer","Power steering", "Electric Seat"};
		final String[] OTHER = { "Adaptive Lights", "Alloy Wheels","CD Player","Compact Disc","Electric Antenna","Tinted Windows ","Sun Roof","Stereo Amplifier","Subwoofer","Headlight Washers","Hands Free" };


		for (int i = 0; i < SAFETY_TYPES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("VEHICLEACCESSORY"));
			object.setParent(classifierFacade.findItemByName("VEHICLEACCESSORY", "Safety"));
			object.setName(SAFETY_TYPES[i].trim());
			classifierFacade.createClassifierItem(object);
		}
		for (int i = 0; i < INTERIOR_TYPES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("VEHICLEACCESSORY"));
			object.setParent(classifierFacade.findItemByName("VEHICLEACCESSORY", "Interior and Comforts"));
			object.setName(INTERIOR_TYPES[i].trim());
			classifierFacade.createClassifierItem(object);
		}
		for (int i = 0; i < OTHER.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("VEHICLEACCESSORY"));
			object.setParent(classifierFacade.findItemByName("VEHICLEACCESSORY", "Other"));
			object.setName(OTHER[i].trim());
			classifierFacade.createClassifierItem(object);
		}
		
		
	}

	private void GenerateVehicleAccessory() {
		final String[] NAMES = { "Safety", "Interior and Comforts","Other" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("VEHICLEACCESSORY"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
		
		
	}

	private void GenerateTelephoneInfo() {

		final String[] NAMES = { "anytime", "evenings", "daytime", "weekends","home", "work", "mobile" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("TELEADDITIONALINFO"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}

	}

	private void GenerateCurrencies() {
		final String[] NAMES = { "CAD", "EUR", "GBP", "GHS","JPY", "USD", "CNY" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier(ClassifierType.CURRENCY));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
		
	}
	
	private void GeneratePriceInfo() {

		final String[] NAMES = { "Inc VAT", "Inc warranty",
				"Most cards accepted", "Negotiable",
				"No canvassers or agencies", "No offers", "No time wasters",
				"On the road", "Optional warranty", "Or Euro equivalent",
				"Plus VAT", "Price reduced" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("PRICEINFO"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
	}

	private void GenerateFuelType() {

		final String[] NAMES = { "Petrol", "Diesel", "Hybrid", "Electric",
				"LPG", "Bi-Fuel", "Other" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("FUELTYPE"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}

	}

	private void GenerateBodyType() {

		final String[] NAMES = { "4x4", "Convertible","Cummuter", "Coupe", "Hardtop",
				"Hatchback","Salon", "Sedan", "Station Wagon", "SUV", "Other" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("BODYTYPE"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
	}

	private void GenerateGearboxType() {
		final String[] NAMES = { "Manual", "Automatic", "Semi-Automatic" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("GEARBOXTYPE"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
	}

	private void GenerateColorType() {

		final String[] NAMES = { "Metallic", "Non-metallic", "Other" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("COLORTYPE"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
	}

	private void GenerateColor() {

		final String[] NAMES = { "Beige", "Gray", "Light gray", "Silver",
				"Yellow", "Golden", "Black", "Other", "Orange", "Brown", "Red",
				"Light red", "Dark red", "Green", "Light green", "Dark green",
				"Blue", "Light blue", "Dark blue", "Turquoise", "White",
				"Copper", "Purple" };

		for (int i = 0; i < NAMES.length; i++) {
			ClassifierItem object = new ClassifierItem();
			object.setClassifier(classifierFacade.findClassifier("COLOR"));
			object.setName(NAMES[i]);
			classifierFacade.createClassifierItem(object);
		}
	}


	private Classifier createClassifier(String type, String name) {
		Classifier object = new Classifier();
		object.setName(name);
		object.setType(type);
		return object;
	}


	
}
